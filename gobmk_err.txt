Num Loops: 806
####
####
####
0 | 2
1 | 0
3 | 0
7 | 0
10 | 0
12 | 0
14 | 0
16 | 0
18 | 0
20 | 0
23 | 0
25 | 0
newF:
 
define internal void @for.cond105_for.cond12_merged(i32* %m_m, i32* %n_m, i32* %i_m, i32* %j_m, %struct.pattern** %pat.addr_m, [84 x [84 x i8]]* %work_space_m, i1 %ctrl) {
merged_header:
  br label %for.cond105.0.2

for.cond105.0.2_pr:                               ; preds = %for.cond105.0.2
  %0 = load i32* %i_m, align 4
  br label %for.cond105.1.0

for.cond105.1.0:                                  ; preds = %for.cond105.0.2, %for.cond105.0.2_pr
  %1 = load i32* %m_m, align 4
  %2 = load %struct.pattern** %pat.addr_m, align 8
  %maxi106 = getelementptr inbounds %struct.pattern* %2, i32 0, i32 6
  %3 = load i32* %maxi106, align 4
  br i1 %ctrl, label %for.cond105.3.0, label %for.cond105.2.1

for.cond105.2.1:                                  ; preds = %for.cond105.1.0
  %4 = load i32* %m_m, align 4
  br label %for.cond105.3.0

for.cond105.3.0:                                  ; preds = %for.cond105.2.1, %for.cond105.1.0
  %phi_obscure = phi i32 [ %1, %for.cond105.1.0 ], [ %0, %for.cond105.0.2_pr ]
  %add107 = add nsw i32 %1, %3
  %add108 = add nsw i32 %add107, 1
  %cmp109 = icmp ne i32 %phi_obscure, %add108
  br i1 %cmp109, label %A, label %_merged_exit

for.body110.0.2:                                  ; preds = %A
  store i32 0, i32* %j_m, align 4
  br label %for.body110.2.0

for.body110.1.1:                                  ; preds = %A
  %5 = load %struct.pattern** %pat.addr_m, align 8
  %minj = getelementptr inbounds %struct.pattern* %5, i32 0, i32 5
  %6 = load i32* %minj, align 4
  %7 = load i32* %n_m, align 4
  %add17 = add nsw i32 %6, %7
  store i32 %add17, i32* %j_m, align 4
  br label %for.body110.2.0

for.body110.2.0:                                  ; preds = %for.body110.0.2, %for.body110.1.1
  br i1 %ctrl, label %for.cond111.0.2, label %for.cond111.1.0

for.cond111.0.2:                                  ; preds = %for.body110.2.0, %for.inc120.0.0
  %8 = load i32* %j_m, align 4
  br label %for.cond111.1.0

for.cond111.1.0:                                  ; preds = %for.cond111.0.2, %for.body110.2.0
  %9 = load i32* %n_m, align 4
  %10 = load %struct.pattern** %pat.addr_m, align 8
  %minj112 = getelementptr inbounds %struct.pattern* %10, i32 0, i32 5
  %11 = load i32* %minj112, align 4
  br i1 %ctrl, label %for.cond111.3.0, label %for.cond111.2.1

for.cond111.2.1:                                  ; preds = %for.cond111.1.0
  %12 = load i32* %n_m, align 4
  br label %for.cond111.3.0

for.cond111.3.0:                                  ; preds = %for.cond111.2.1, %for.cond111.1.0
  %add113 = add nsw i32 %9, %11
  br i1 %ctrl, label %for.cond111.5.0, label %for.cond111.4.1

for.cond111.4.1:                                  ; preds = %for.cond111.3.0
  %add20 = add nsw i32 %add113, 1
  br label %for.cond111.5.0

for.cond111.5.0:                                  ; preds = %for.cond111.4.1, %for.cond111.3.0
  %phi_obscure1 = phi i32 [ %9, %for.cond111.1.0 ], [ %8, %for.cond111.0.2 ]
  %cmp114 = icmp ne i32 %phi_obscure1, %add113
  br i1 %cmp114, label %for.body115.0.0, label %for.end122.0.0

for.end122.0.0:                                   ; preds = %for.cond111.5.0
  br label %for.inc123.0.0

for.inc123.0.0:                                   ; preds = %for.end122.0.0
  %13 = load i32* %i_m, align 4
  %inc124 = add nsw i32 %13, 1
  store i32 %inc124, i32* %i_m, align 4
  br label %for.cond105.0.2

for.body115.0.0:                                  ; preds = %for.cond111.5.0
  %14 = load i32* %j_m, align 4
  %idxprom116 = sext i32 %14 to i64
  %15 = load i32* %i_m, align 4
  %idxprom117 = sext i32 %15 to i64
  %arrayidx118 = getelementptr inbounds [84 x [84 x i8]]* %work_space_m, i32 0, i64 %idxprom117
  %arrayidx119 = getelementptr inbounds [84 x i8]* %arrayidx118, i32 0, i64 %idxprom116
  br i1 %ctrl, label %for.body115.1.2, label %for.body115.2.1

for.body115.1.2:                                  ; preds = %for.body115.0.0
  store i8 124, i8* %arrayidx119, align 1
  br label %for.body115.3.0

for.body115.2.1:                                  ; preds = %for.body115.0.0
  store i8 63, i8* %arrayidx119, align 1
  br label %for.body115.3.0

for.body115.3.0:                                  ; preds = %for.body115.2.1, %for.body115.1.2
  br label %for.inc120.0.0

for.inc120.0.0:                                   ; preds = %for.body115.3.0
  %16 = load i32* %j_m, align 4
  %inc121 = add nsw i32 %16, 1
  store i32 %inc121, i32* %j_m, align 4
  br label %for.cond111.0.2

for.cond105.0.2:                                  ; preds = %merged_header, %for.inc123.0.0
  br i1 %ctrl, label %for.cond105.0.2_pr, label %for.cond105.1.0

A:                                                ; preds = %for.cond105.3.0
  br i1 %ctrl, label %for.body110.0.2, label %for.body110.1.1

_merged_exit:                                     ; preds = %for.cond105.3.0
  ret void
}

####
0 | 0
2 | 0
4 | 0
6 | 0
8 | 0
11 | 0
13 | 0
15 | 0
17 | 0
19 | 0
21 | 0
newF:
 
define internal void @for.cond288_for.cond253_merged(i32* %pos_m, i32* %esize311_m, i32* %msize312_m, %struct.eye_data** %w_eye.addr_m, i1 %ctrl) {
merged_header:
  br label %for.cond288.0.0

for.cond288.0.0:                                  ; preds = %merged_header, %for.inc320.0.0
  %0 = load i32* %pos_m, align 4
  %cmp289 = icmp slt i32 %0, 400
  br i1 %cmp289, label %for.body291.0.0, label %_merged_exit

for.body291.0.0:                                  ; preds = %for.cond288.0.0
  %1 = load i32* %pos_m, align 4
  %idxprom292 = sext i32 %1 to i64
  %arrayidx293 = getelementptr inbounds [421 x i8]* @board, i32 0, i64 %idxprom292
  %2 = load i8* %arrayidx293, align 1
  %conv294 = zext i8 %2 to i32
  %cmp295 = icmp ne i32 %conv294, 3
  br i1 %cmp295, label %if.end298.0.0, label %if.then297.0.0

if.then297.0.0:                                   ; preds = %for.body291.0.0
  br label %for.inc320.0.0

if.end298.0.0:                                    ; preds = %for.body291.0.0
  %3 = load i32* %pos_m, align 4
  %idxprom299 = sext i32 %3 to i64
  %4 = load %struct.eye_data** %w_eye.addr_m, align 8
  %arrayidx300 = getelementptr inbounds %struct.eye_data* %4, i64 %idxprom299
  %origin301 = getelementptr inbounds %struct.eye_data* %arrayidx300, i32 0, i32 3
  %5 = load i32* %origin301, align 4
  %cmp302 = icmp eq i32 %5, 0
  br i1 %cmp302, label %land.lhs.true304.0.0, label %if.end298.if.end319_crit_edge.0.0

land.lhs.true304.0.0:                             ; preds = %if.end298.0.0
  %6 = load i32* %pos_m, align 4
  %idxprom305 = sext i32 %6 to i64
  %7 = load %struct.eye_data** %w_eye.addr_m, align 8
  %arrayidx306 = getelementptr inbounds %struct.eye_data* %7, i64 %idxprom305
  %color307 = getelementptr inbounds %struct.eye_data* %arrayidx306, i32 0, i32 0
  %8 = load i32* %color307, align 4
  br i1 %ctrl, label %land.lhs.true304.1.2, label %land.lhs.true304.2.1

land.lhs.true304.1.2:                             ; preds = %land.lhs.true304.0.0
  %cmp308 = icmp eq i32 %8, 4
  br label %land.lhs.true304.3.0

land.lhs.true304.2.1:                             ; preds = %land.lhs.true304.0.0
  %cmp272 = icmp eq i32 %8, 5
  br label %land.lhs.true304.3.0

land.lhs.true304.3.0:                             ; preds = %land.lhs.true304.2.1, %land.lhs.true304.1.2
  %phi = phi i1 [ %cmp308, %land.lhs.true304.1.2 ], [ %cmp272, %land.lhs.true304.2.1 ]
  br i1 %phi, label %if.then310.0.0, label %land.lhs.true304.if.end319_crit_edge.0.0

if.then310.0.0:                                   ; preds = %land.lhs.true304.3.0
  store i32 0, i32* %esize311_m, align 4
  store i32 0, i32* %msize312_m, align 4
  %9 = load i32* %pos_m, align 4
  %10 = load i32* %pos_m, align 4
  %11 = load %struct.eye_data** %w_eye.addr_m, align 8
  call void @originate_eye(i32 %9, i32 %10, i32* %esize311_m, i32* %msize312_m, %struct.eye_data* %11)
  %12 = load i32* %esize311_m, align 4
  %13 = load i32* %pos_m, align 4
  %idxprom313 = sext i32 %13 to i64
  %14 = load %struct.eye_data** %w_eye.addr_m, align 8
  %arrayidx314 = getelementptr inbounds %struct.eye_data* %14, i64 %idxprom313
  %esize315 = getelementptr inbounds %struct.eye_data* %arrayidx314, i32 0, i32 1
  store i32 %12, i32* %esize315, align 4
  %15 = load i32* %msize312_m, align 4
  %16 = load i32* %pos_m, align 4
  %idxprom316 = sext i32 %16 to i64
  %17 = load %struct.eye_data** %w_eye.addr_m, align 8
  %arrayidx317 = getelementptr inbounds %struct.eye_data* %17, i64 %idxprom316
  %msize318 = getelementptr inbounds %struct.eye_data* %arrayidx317, i32 0, i32 2
  store i32 %15, i32* %msize318, align 4
  br label %if.end319.0.0

if.end319.0.0:                                    ; preds = %if.then310.0.0, %if.end298.if.end319_crit_edge.0.0, %land.lhs.true304.if.end319_crit_edge.0.0
  br label %for.inc320.0.0

for.inc320.0.0:                                   ; preds = %if.then297.0.0, %if.end319.0.0
  %18 = load i32* %pos_m, align 4
  %inc321 = add nsw i32 %18, 1
  store i32 %inc321, i32* %pos_m, align 4
  br label %for.cond288.0.0

if.end298.if.end319_crit_edge.0.0:                ; preds = %if.end298.0.0
  br label %if.end319.0.0

land.lhs.true304.if.end319_crit_edge.0.0:         ; preds = %land.lhs.true304.3.0
  br label %if.end319.0.0

_merged_exit:                                     ; preds = %for.cond288.0.0
  ret void
}

####
0 | 0
2 | 0
4 | 0
6 | 0
8 | 0
10 | 0
12 | 0
14 | 0
16 | 0
18 | 0
20 | 0
22 | 0
24 | 0
newF:
 
define internal void @for.cond_for.cond_merged(i32* %m_m, i32* %n_m, i32* %first_m, i1 %ctrl) {
merged_header:
  br label %for.cond.0.0

for.cond.0.0:                                     ; preds = %merged_header, %for.inc33.0.0
  %0 = load i32* %m_m, align 4
  %1 = load i32* @board_size, align 4
  %cmp21 = icmp slt i32 %0, %1
  br i1 %cmp21, label %for.body.0.0, label %_merged_exit

for.body.0.0:                                     ; preds = %for.cond.0.0
  store i32 0, i32* %n_m, align 4
  br label %for.cond22.0.0

for.cond22.0.0:                                   ; preds = %for.body.0.0, %for.inc.0.0
  %2 = load i32* %n_m, align 4
  %3 = load i32* @board_size, align 4
  %cmp23 = icmp slt i32 %2, %3
  br i1 %cmp23, label %for.body24.0.0, label %for.end.0.0

for.end.0.0:                                      ; preds = %for.cond22.0.0
  br label %for.inc33.0.0

for.inc33.0.0:                                    ; preds = %for.end.0.0
  %4 = load i32* %m_m, align 4
  %inc34 = add nsw i32 %4, 1
  store i32 %inc34, i32* %m_m, align 4
  br label %for.cond.0.0

for.body24.0.0:                                   ; preds = %for.cond22.0.0
  %5 = load i32* %m_m, align 4
  %mul = mul nsw i32 %5, 20
  %add = add nsw i32 21, %mul
  %6 = load i32* %n_m, align 4
  %add25 = add nsw i32 %add, %6
  %idxprom = sext i32 %add25 to i64
  %arrayidx = getelementptr inbounds [421 x i8]* @board, i32 0, i64 %idxprom
  %7 = load i8* %arrayidx, align 1
  %conv = zext i8 %7 to i32
  %cmp26 = icmp ne i32 %conv, 0
  br i1 %cmp26, label %if.then28.0.0, label %for.body24.if.end32_crit_edge.0.0

if.then28.0.0:                                    ; preds = %for.body24.0.0
  %8 = load i32* %first_m, align 4
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.else30.0.0, label %if.then29.0.0

if.then29.0.0:                                    ; preds = %if.then28.0.0
  call void (i8*, ...)* @gtp_printf(i8* getelementptr inbounds ([2 x i8]* @.str165536, i32 0, i32 0))
  br label %if.end31.0.0

if.else30.0.0:                                    ; preds = %if.then28.0.0
  store i32 0, i32* %first_m, align 4
  br label %if.end31.0.0

if.end31.0.0:                                     ; preds = %if.then29.0.0, %if.else30.0.0
  %9 = load i32* %m_m, align 4
  %10 = load i32* %n_m, align 4
  call void (i8*, ...)* @gtp_mprintf(i8* getelementptr inbounds ([3 x i8]* @.str166537, i32 0, i32 0), i32 %9, i32 %10)
  br label %if.end32.0.0

if.end32.0.0:                                     ; preds = %if.end31.0.0, %for.body24.if.end32_crit_edge.0.0
  br label %for.inc.0.0

for.inc.0.0:                                      ; preds = %if.end32.0.0
  %11 = load i32* %n_m, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %n_m, align 4
  br label %for.cond22.0.0

for.body24.if.end32_crit_edge.0.0:                ; preds = %for.body24.0.0
  br label %if.end32.0.0

_merged_exit:                                     ; preds = %for.cond.0.0
  ret void
}

####
0 | 0
2 | 0
4 | 0
6 | 0
8 | 0
10 | 0
12 | 0
14 | 0
16 | 0
18 | 0
20 | 0
22 | 0
24 | 0
newF:
 
define internal void @for.cond205_for.cond155_merged(i32* %m_m, i32* %n_m, i32* %i_m, i32* %j_m, [84 x [84 x i8]]* %work_space_m, i1 %ctrl) {
merged_header:
  br label %for.cond205.0.0

for.cond205.0.0:                                  ; preds = %merged_header, %for.inc232.0.0
  %0 = load i32* %i_m, align 4
  %cmp206 = icmp ne i32 %0, 64
  br i1 %cmp206, label %for.body208.0.0, label %_merged_exit

for.body208.0.0:                                  ; preds = %for.cond205.0.0
  store i32 20, i32* %j_m, align 4
  br label %for.cond209.0.0

for.cond209.0.0:                                  ; preds = %for.body208.0.0, %for.inc228.0.0
  %1 = load i32* %j_m, align 4
  %cmp210 = icmp ne i32 %1, 64
  br i1 %cmp210, label %for.body212.0.0, label %for.end230.0.0

for.end230.0.0:                                   ; preds = %for.cond209.0.0
  %2 = load %struct._IO_FILE** @stderr, align 8
  %call231 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([2 x i8]* @.str114315, i32 0, i32 0))
  br label %for.inc232.0.0

for.inc232.0.0:                                   ; preds = %for.end230.0.0
  %3 = load i32* %i_m, align 4
  %inc233 = add nsw i32 %3, 1
  store i32 %inc233, i32* %i_m, align 4
  br label %for.cond205.0.0

for.body212.0.0:                                  ; preds = %for.cond209.0.0
  %4 = load i32* %i_m, align 4
  %5 = load i32* %m_m, align 4
  %cmp213 = icmp eq i32 %4, %5
  br i1 %cmp213, label %land.lhs.true215.0.0, label %for.body212.if.else220_crit_edge.0.0

land.lhs.true215.0.0:                             ; preds = %for.body212.0.0
  %6 = load i32* %j_m, align 4
  %7 = load i32* %n_m, align 4
  %cmp216 = icmp eq i32 %6, %7
  br i1 %cmp216, label %if.then218.0.0, label %land.lhs.true215.if.else220_crit_edge.0.0

if.else220.0.0:                                   ; preds = %for.body212.if.else220_crit_edge.0.0, %land.lhs.true215.if.else220_crit_edge.0.0
  %8 = load %struct._IO_FILE** @stderr, align 8
  %9 = load i32* %j_m, align 4
  %idxprom221 = sext i32 %9 to i64
  %10 = load i32* %i_m, align 4
  %idxprom222 = sext i32 %10 to i64
  %arrayidx223 = getelementptr inbounds [84 x [84 x i8]]* %work_space_m, i32 0, i64 %idxprom222
  %arrayidx224 = getelementptr inbounds [84 x i8]* %arrayidx223, i32 0, i64 %idxprom221
  %11 = load i8* %arrayidx224, align 1
  %conv225 = sext i8 %11 to i32
  %call226 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([3 x i8]* @.str434347, i32 0, i32 0), i32 %conv225)
  br label %if.end227.0.0

if.then218.0.0:                                   ; preds = %land.lhs.true215.0.0
  %12 = load %struct._IO_FILE** @stderr, align 8
  %call219 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([2 x i8]* @.str424346, i32 0, i32 0))
  br label %if.end227.0.0

if.end227.0.0:                                    ; preds = %if.then218.0.0, %if.else220.0.0
  br label %for.inc228.0.0

for.inc228.0.0:                                   ; preds = %if.end227.0.0
  %13 = load i32* %j_m, align 4
  %inc229 = add nsw i32 %13, 1
  store i32 %inc229, i32* %j_m, align 4
  br label %for.cond209.0.0

for.body212.if.else220_crit_edge.0.0:             ; preds = %for.body212.0.0
  br label %if.else220.0.0

land.lhs.true215.if.else220_crit_edge.0.0:        ; preds = %land.lhs.true215.0.0
  br label %if.else220.0.0

_merged_exit:                                     ; preds = %for.cond205.0.0
  ret void
}

####
0 | 0
2 | 0
4 | 0
6 | 0
8 | 0
10 | 0
12 | 0
14 | 0
16 | 0
18 | 0
20 | 0
22 | 0
24 | 0
26 | 0
28 | 0
30 | 0
32 | 0
newF:
 
define internal void @for.cond_for.cond_merged1(i32* %adj_m, i32* %r_m, i32* %hpos_m, i32* %u_m, [160 x i32]* %adjs_m, %struct.reading_moves* %moves_m, i1 %ctrl) {
merged_header:
  br label %for.cond.0.0

for.cond.0.0:                                     ; preds = %merged_header, %for.inc57.0.0
  %0 = load i32* %r_m, align 4
  %1 = load i32* %adj_m, align 4
  %cmp16 = icmp slt i32 %0, %1
  br i1 %cmp16, label %for.body.0.0, label %_merged_exit

for.body.0.0:                                     ; preds = %for.cond.0.0
  %2 = load i32* %r_m, align 4
  %idxprom18 = sext i32 %2 to i64
  %arrayidx19 = getelementptr inbounds [160 x i32]* %adjs_m, i32 0, i64 %idxprom18
  %3 = load i32* %arrayidx19, align 4
  call void @break_chain_moves(i32 %3, %struct.reading_moves* %moves_m)
  %4 = load i32* %r_m, align 4
  %idxprom20 = sext i32 %4 to i64
  %arrayidx21 = getelementptr inbounds [160 x i32]* %adjs_m, i32 0, i64 %idxprom20
  %5 = load i32* %arrayidx21, align 4
  %call22 = call i32 @findlib(i32 %5, i32 1, i32* %hpos_m)
  br label %do.body23.0.0

do.body23.0.0:                                    ; preds = %for.body.0.0
  store i32 0, i32* %u_m, align 4
  br label %for.cond24.0.0

for.cond24.0.0:                                   ; preds = %do.body23.0.0, %for.inc.0.0
  %6 = load i32* %u_m, align 4
  %num25 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 2
  %7 = load i32* %num25, align 4
  %cmp26 = icmp slt i32 %6, %7
  br i1 %cmp26, label %for.body28.0.0, label %for.end.loopexit.0.0

for.end.loopexit.0.0:                             ; preds = %for.cond24.0.0
  br label %for.end.0.0

for.body28.0.0:                                   ; preds = %for.cond24.0.0
  %8 = load i32* %u_m, align 4
  %idxprom29 = sext i32 %8 to i64
  %pos = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [50 x i32]* %pos, i32 0, i64 %idxprom29
  %9 = load i32* %arrayidx30, align 4
  %10 = load i32* %hpos_m, align 4
  %cmp31 = icmp eq i32 %9, %10
  br i1 %cmp31, label %if.then33.0.0, label %if.end36.0.0

if.end36.0.0:                                     ; preds = %for.body28.0.0
  br label %for.inc.0.0

for.inc.0.0:                                      ; preds = %if.end36.0.0
  %11 = load i32* %u_m, align 4
  %inc37 = add nsw i32 %11, 1
  store i32 %inc37, i32* %u_m, align 4
  br label %for.cond24.0.0

if.then33.0.0:                                    ; preds = %for.body28.0.0
  %12 = load i32* %u_m, align 4
  %idxprom34 = sext i32 %12 to i64
  %score = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 1
  %arrayidx35 = getelementptr inbounds [50 x i32]* %score, i32 0, i64 %idxprom34
  %13 = load i32* %arrayidx35, align 4
  %add = add nsw i32 %13, 0
  store i32 %add, i32* %arrayidx35, align 4
  br label %for.end.0.0

for.end.0.0:                                      ; preds = %if.then33.0.0, %for.end.loopexit.0.0
  %14 = load i32* %u_m, align 4
  %num38 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 2
  %15 = load i32* %num38, align 4
  %cmp39 = icmp eq i32 %14, %15
  br i1 %cmp39, label %land.lhs.true.0.0, label %for.end.if.end55_crit_edge.0.0

land.lhs.true.0.0:                                ; preds = %for.end.0.0
  %num41 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 2
  %16 = load i32* %num41, align 4
  %cmp42 = icmp slt i32 %16, 50
  br i1 %cmp42, label %if.then44.0.0, label %land.lhs.true.if.end55_crit_edge.0.0

if.then44.0.0:                                    ; preds = %land.lhs.true.0.0
  %17 = load i32* %hpos_m, align 4
  %num45 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 2
  %18 = load i32* %num45, align 4
  %idxprom46 = sext i32 %18 to i64
  %pos47 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 0
  %arrayidx48 = getelementptr inbounds [50 x i32]* %pos47, i32 0, i64 %idxprom46
  store i32 %17, i32* %arrayidx48, align 4
  %num49 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 2
  %19 = load i32* %num49, align 4
  %idxprom50 = sext i32 %19 to i64
  %score51 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 1
  %arrayidx52 = getelementptr inbounds [50 x i32]* %score51, i32 0, i64 %idxprom50
  store i32 0, i32* %arrayidx52, align 4
  %num53 = getelementptr inbounds %struct.reading_moves* %moves_m, i32 0, i32 2
  %20 = load i32* %num53, align 4
  %inc54 = add nsw i32 %20, 1
  store i32 %inc54, i32* %num53, align 4
  br label %if.end55.0.0

if.end55.0.0:                                     ; preds = %if.then44.0.0, %for.end.if.end55_crit_edge.0.0, %land.lhs.true.if.end55_crit_edge.0.0
  br label %do.end56.0.0

do.end56.0.0:                                     ; preds = %if.end55.0.0
  br label %for.inc57.0.0

for.inc57.0.0:                                    ; preds = %do.end56.0.0
  %21 = load i32* %r_m, align 4
  %inc58 = add nsw i32 %21, 1
  store i32 %inc58, i32* %r_m, align 4
  br label %for.cond.0.0

for.end.if.end55_crit_edge.0.0:                   ; preds = %for.end.0.0
  br label %if.end55.0.0

land.lhs.true.if.end55_crit_edge.0.0:             ; preds = %land.lhs.true.0.0
  br label %if.end55.0.0

_merged_exit:                                     ; preds = %for.cond.0.0
  ret void
}

####
####
####
high number of function args: 18
Num Loops: 806
num merged pairs: 5
PHI node entries do not match predecessors!
  %phi_obscure = phi i32 [ %1, %for.cond105.1.0 ], [ %0, %for.cond105.0.2_pr ]
label %for.cond105.0.2_pr
label %for.cond105.2.1
PHI node entries do not match predecessors!
  %phi_obscure1 = phi i32 [ %9, %for.cond111.1.0 ], [ %8, %for.cond111.0.2 ]
label %for.cond111.0.2
label %for.cond111.3.0
LLVM ERROR: Broken function found, compilation aborted!
