int f()
{
  int count = 0;
  for(int i = 10; i < 50; i++){
    count += i*i;
  }
  return count;
}

int g()
{
  int count = 0;
  for(int i = 8; i < 48; i++){
    count += (i+2)*(i+2);
  }
  return count;
}

int main()
{
  int count1 = f();
  int count2 = g();

  return 0;
}
  
