	.text
	.file	"source files/loops1_mod.bc"
	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %for.end
	pushq	%rbp
.Ltmp0:
	.cfi_def_cfa_offset 16
.Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp2:
	.cfi_def_cfa_register %rbp
	movl	$0, -4(%rbp)
	movq	.Lmain.A+32(%rip), %rax
	movq	%rax, -16(%rbp)
	movaps	.Lmain.A+16(%rip), %xmm0
	movaps	%xmm0, -32(%rbp)
	movaps	.Lmain.A(%rip), %xmm0
	movaps	%xmm0, -48(%rbp)
	movq	.Lmain.B+16(%rip), %rax
	movq	%rax, -64(%rbp)
	movaps	.Lmain.B(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	.Lmain.C(%rip), %eax
	movl	%eax, -84(%rbp)
	movl	$0, -88(%rbp)
	movl	$123, -92(%rbp)
	movl	$246, -92(%rbp)
	movl	$0, -96(%rbp)
	jmp	.LBB0_1
	.align	16, 0x90
.LBB0_2:                                # %for.inc19
                                        #   in Loop: Header=BB0_1 Depth=1
	movslq	-96(%rbp), %rax
	movl	-80(%rbp,%rax,4), %ecx
	addl	%ecx, %ecx
	movl	%ecx, -80(%rbp,%rax,4)
	movslq	-96(%rbp), %rax
	movl	-80(%rbp,%rax,4), %eax
	addl	%eax, -84(%rbp)
	incl	-96(%rbp)
.LBB0_1:                                # %for.cond7
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$5, -96(%rbp)
	jle	.LBB0_2
# BB#3:                                 # %for.end21
	xorl	%eax, %eax
	popq	%rbp
	retq
.Ltmp3:
	.size	main, .Ltmp3-main
	.cfi_endproc

	.type	.Lmain.A,@object        # @main.A
	.section	.rodata,"a",@progbits
	.align	16
.Lmain.A:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.size	.Lmain.A, 40

	.type	.Lmain.B,@object        # @main.B
	.align	16
.Lmain.B:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.size	.Lmain.B, 24

	.type	.Lmain.C,@object        # @main.C
	.section	.rodata.cst4,"aM",@progbits,4
	.align	4
.Lmain.C:
	.long	1                       # 0x1
	.size	.Lmain.C, 4


	.ident	"clang version 3.5 (trunk 201963)"
	.section	".note.GNU-stack","",@progbits
