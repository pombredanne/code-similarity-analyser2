int main()
{
	// This program has 2 loops that have the same body 
	// but are executed a different number of times
	int A[10] = {1,2,3,4,5,6,7,8,9,10};
	int B[6] = {1,2,3,4,5,6};
    int C[1] = {1};
	for(int i = 0; i < 10; ++i){
		A[i] *= A[i];
        C[0] += A[i];
	}
    int x = 123;
    x += x;
	for(int j = 0; j < 6; ++j){
      // was * also
		B[j] += B[j];
        C[0] += B[j];
	}
    return 0;
}
