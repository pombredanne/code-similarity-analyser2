#include "stdafx.h"
#include "insertion_sort.h"
#define TESTING 0
void insertion_sort(std::vector<int>& A)
{
  int n = A.size();
  int key , j;
  for (int i = 1; i < n; i++){
    key = A.at(i);
    j = i - 1;
    while (j > -1 && A.at(j) > key){ // why ? [] unchecked access
      A.at(j+1) = A.at(j);
      j--;
    }
    A.at(j+1) = key;
  }
}
// iterators in header file due to inlining

// very slow, ignore
void insertion_sort_swaps(std::vector<int>::iterator begin, std::vector<int>::iterator end)
{
  for(auto i = begin + 1; i < end; i++){
    auto j = i - 1;
    while (j >= begin && *j > *(j+1)){
      auto tmp = *j;
      *j = *(j+1);
      *(j+1) = tmp;
      j--;
    }
  }
}

// 23 10 37 28 1
#if TESTING
int main()
{
  int size;
  // mini example sorting
#if 1
  std::vector<int> a1;
  
  a1.push_back(23);
  a1.push_back(10);
  a1.push_back(37);
  a1.push_back(67);
  a1.push_back(28);
  a1.push_back(1);
  a1.push_back(-3);


  // insertion_sort(a1);
  // for(auto v: a1){
  //   std::cout<<v<<" ";
  // }
  // std::cout<<"\n";
  
  insertion_sort_swaps(a1.begin(),a1.end());
  for(auto v: a1){
    std::cout<<v<<" ";
  }
  std::cout<<"\n";
#endif
  // serious testing
#if 1
  std::vector<int> b;
  std::vector<int> c;

  size = 90000;
  for(int i = 0; i < size; i++){
    auto rand = random();
    b.push_back(rand);
    c.push_back(rand);
  }
  insertion_sort(b.begin(),b.end());
  std::sort(c.begin(),c.end());
  
  std::cout<<"Does our insertion sort sort properly? "<<(b==c)<<"\n";
#endif
  // benchmarking
#if 0
  std::vector<int> d;
  std::vector<int> e;
  
  size = 300000;
  for(int i = 0; i < size; i++){
    auto rand = random();
    d.push_back(rand);
    //e.push_back(rand);
  }
  insertion_sort(d.begin(),d.end());
  //insertion_sort_swaps(d);
  //insertion_sort(b.begin(),b.end());
  //std::sort(c.begin(),c.end());
  
  //std::cout<<"Does our insertion sort sort properly? "<<(b==c)<<"\n";
#endif

  return 0;
}
#endif
