#ifndef H_STDAFX_H
#define H_STDAFX_H

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#include <map>
#include <utility>
#include <cassert>
#include <iterator>
#include <set>
#include <functional>
#include <array> 
#include <thread>
#include <chrono>
#include <cmath>
#include <random>
#include <valarray>
#include <limits>
#include <csignal>
#include <unordered_map>
#include <map>

#endif
