	.text
	.file	"source files/small_loop_m.bc"
	.globl	f
	.align	16, 0x90
	.type	f,@function
f:                                      # @f
	.cfi_startproc
# BB#0:                                 # %entry
	pushq	%rbp
.Ltmp0:
	.cfi_def_cfa_offset 16
.Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp2:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	imull	%edi, %edi
	movl	%edi, %eax
	popq	%rbp
	retq
.Ltmp3:
	.size	f, .Ltmp3-f
	.cfi_endproc

	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %entry
	pushq	%rbp
.Ltmp4:
	.cfi_def_cfa_offset 16
.Ltmp5:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp6:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	$0, -4(%rbp)
	movl	$1, -8(%rbp)
	movl	$3, -12(%rbp)
	movl	-8(%rbp), %edi
	callq	f
	movl	%eax, -8(%rbp)
	movl	$1, -16(%rbp)
	leaq	-16(%rbp), %rdx
	leaq	-8(%rbp), %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	callq	for.cond2_for.cond_merged
	movl	$1, -20(%rbp)
	leaq	-12(%rbp), %rdi
	leaq	-20(%rbp), %rsi
	leaq	-8(%rbp), %rcx
	xorl	%edx, %edx
	movl	$1, %r8d
	callq	for.cond2_for.cond_merged
	movl	-8(%rbp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	-12(%rbp), %esi
	movl	$.L.str1, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbp
	retq
.Ltmp7:
	.size	main, .Ltmp7-main
	.cfi_endproc

	.globl	for.cond2_for.cond_merged
	.align	16, 0x90
	.type	for.cond2_for.cond_merged,@function
for.cond2_for.cond_merged:              # @for.cond2_for.cond_merged
	.cfi_startproc
# BB#0:                                 # %for.cond2_for.cond_merged_header
	pushq	%rbp
.Ltmp8:
	.cfi_def_cfa_offset 16
.Ltmp9:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp10:
	.cfi_def_cfa_register %rbp
	testb	$1, %r8b
	je	.LBB2_3
	jmp	.LBB2_1
	.align	16, 0x90
.LBB2_2:                                # %for.inc7
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	(%rdi), %eax
	addl	(%rsi), %eax
	movl	%eax, (%rdi)
	imull	(%rcx), %eax
	movl	%eax, (%rdi)
	incl	(%rsi)
.LBB2_1:                                # %for.cond2
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$199, (%rsi)
	jle	.LBB2_2
	jmp	.LBB2_5
	.align	16, 0x90
.LBB2_4:                                # %for.inc
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	(%rcx), %eax
	addl	(%rdx), %eax
	movl	%eax, (%rcx)
	imull	%eax, %eax
	movl	%eax, (%rcx)
	incl	(%rdx)
.LBB2_3:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$99, (%rdx)
	jle	.LBB2_4
.LBB2_5:                                # %for.cond2_for.cond_merged_exit
	popq	%rbp
	retq
.Ltmp11:
	.size	for.cond2_for.cond_merged, .Ltmp11-for.cond2_for.cond_merged
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d "
	.size	.L.str, 4

	.type	.L.str1,@object         # @.str1
.L.str1:
	.asciz	"%d \n"
	.size	.L.str1, 5


	.ident	"clang version 3.5 (trunk 201963)"
	.section	".note.GNU-stack","",@progbits
