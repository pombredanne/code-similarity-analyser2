/*
  A call graph (also known as a call multigraph) is a directed graph
  that represents calling relationships between subroutines in a
  computer program. Specifically, each node represents a procedure and
  each edge (f, g) indicates that procedure f calls procedure g. Thus,
  a cycle in the graph indicates recursive procedure calls.
*/
int g(int n);
int f(int n)
{
  if(n  > 20) return g(n);
  else return n*n;
}

int g(int n)
{
  return n+f(n);
}

int h(int n)
{
  return g(f(n));
}

int main()
{
  h(4);
  g(h(3));
  return 0;
}

