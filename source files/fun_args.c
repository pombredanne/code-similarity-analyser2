int f13(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j, int k, int l, int m){
  return a*b*c*d*e*f*g*h*i*j*k*l*m;
}

int f12(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j, int k, int l){
  return a*b*c*d*e*f*g*h*i*j*k*l;
}

int f11(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j, int k){
  return a*b*c*d*e*f*g*h*i*j*k;
}

int f10(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j){
  return a*b*c*d*e*f*g*h*i*j;
}

int f9(int a, int b, int c, int d, int e, int f, int g, int h, int i){
  return a*b*c*d*e*f*g*h*i;
}

int f8(int a, int b, int c, int d, int e, int f, int g, int h){
  return a*b*c*d*e*f*g*h;
}

int f7(int a, int b, int c, int d, int e, int f, int g){
  return a*b*c*d*e*f*g;
}

int f6(int a, int b, int c, int d, int e, int f){
  return a*b*c*d*e*f;
}

int f5(int a, int b, int c, int d, int e){
  return a*b*c*d*e;
}

int f4(int a, int b, int c, int d){
  return a*b*c*d;
}

int f3(int a, int b, int c){
  return a*b*c;
}

int f2(int a, int b){
  return a*b;
}

int f1(int a){
  return a;
}


int main()
{
  int x = 3;
  int a1 = f1(x);
  int a2 = f2(x,x);
  int a3 = f3(x,x,x);
  int a4 = f4(x,x,x,x);
  int a5 = f5(x,x,x,x,x);
  int a6 = f6(x,x,x,x,x,x);
  int a7 = f7(x,x,x,x,x,x,x);
  int a8 = f8(x,x,x,x,x,x,x,x);
  int a9 = f9(x,x,x,x,x,x,x,x,x);
  int a10 = f10(x,x,x,x,x,x,x,x,x,x);
  int a11 = f11(x,x,x,x,x,x,x,x,x,x,x);
  int a12 = f12(x,x,x,x,x,x,x,x,x,x,x,x);
  int a13 = f13(x,x,x,x,x,x,x,x,x,x,x,x,x);

  return 0;
}


