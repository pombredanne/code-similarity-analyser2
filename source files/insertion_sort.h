#include "stdafx.h"

#ifndef H_INSERTION_SORT_H
#define H_INSERTION_SORT_H
void insertion_sort(std::vector<int>& A);

inline void insertion_sort(std::vector<int>::iterator begin, std::vector<int>::iterator end)
{
  for(auto i = begin + 1; i < end; i++){
    auto j = i - 1;
    auto key = *i;
    while (j >= begin && *j > key){
      *(j+1) = *j;
      j--;
    }
    *(j+1) = key;
  }
}

#endif
