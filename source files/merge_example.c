#include <stdio.h>
#include <math.h>
int main(){
  int a = 1;
  int b = 2;
  int ac1 = 0; int ac2 = 1; int ac3 = 2;
  int bc1 = 0; int bc2 = 1; int bc3 = 2;
  for(int i = 0; i < 7; ++i){
    ac1 += a*i;
    if (i%2 == 0)
      ac2 *= a*i;
    ac3 += a;
  }
  for(int j = 0; j < 8; ++j){
    bc1 += b*j;
    if (j%2 == 0)
      bc2 *= b*j;
    bc3 += b;
  }
  printf("%d %d %d, %d %d %d\n",ac1,ac2,ac3,bc1,bc2,bc3);
  return 0;
}
