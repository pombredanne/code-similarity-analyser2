/**
 * Loop analysing program Written by Bilyan Borisov
 */
#include "llvm/Analysis/CFGPrinter.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopInfoImpl.h"
#include "llvm/Analysis/LoopIterator.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/DebugInfo.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/SimplifyIndVar.h"
#include "llvm/Transforms/Utils/LoopUtils.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <map>
#include <utility>
#include <cmath>
#include <random>
#include <stdint.h>
using namespace llvm;

namespace {
  static int my_min(int x, int y){
    if (x < y) return x;
    else return y;
  }
  typedef std::string opcode;
  typedef struct {
    std::vector<std::string> str_repr;
    std::map<opcode,unsigned> op_counts;
    // debug info
    DebugLoc dbLoc;
    unsigned order;
    Function* fun;
  } loop_info;
  static const std::vector<opcode> ops = {
    "ret",
    "br",
    "switch",
    "indirectbr",
    "invoke",
    "resume",
    "unreachable",
    "add",
    "fadd",
    "sub",
    "fsub",
    "mul",
    "fmul",
    "udiv",
    "sdiv",
    "fdiv",
    "urem",
    "srem",
    "frem",
    "and",
    "or",
    "xor",
    "alloca",
    "load",
    "store",
    "cmpxchg",
    "atomicrmw",
    "fence",
    "getelementptr",
    "trunc",
    "zext",
    "sext",
    "fptrunc",
    "fpext",
    "fptoui",
    "fptosi",
    "uitofp",
    "sitofp",
    "inttoptr",
    "ptrtoint",
    "bitcast",
    "addrspacecast",
    "icmp",
    "fcmp",
    "phi",
    "select",
    "call",
    "shl",
    "lshr",
    "ashr",
    "va_arg",
    "extractelement",
    "insertelement",
    "shufflevector",
    "extractvalue",
    "insertvalue",
    "landingpad",
  };
  static std::map<opcode, int> op2dim;
  struct CodeSimilarityAnalyser : public ModulePass{
    static char ID;
    CodeSimilarityAnalyser() : ModulePass(ID){
      errs() << "Init called\n";
      loops_infos = std::vector<loop_info>();
      for (int i = 0; i < 57; ++i){
        op2dim[ops.at(i)] = i;
      }
    }

    std::vector<loop_info> loops_infos;

    void getAnalysisUsage(AnalysisUsage &AU) const {
      AU.setPreservesAll();
      AU.addRequired<LoopInfo>();
      AU.addPreserved<LoopInfo>();
      AU.addRequiredID(LoopSimplifyID);
      AU.addPreservedID(LoopSimplifyID);
    }
    virtual bool runOnModule(Module& Mod){
      /*
        implementation using LSH
        nice O(n lg(n)) solution
      */
      double nipbb = 0.0;
      int numbb = 0;
      // first gather all loop info
      for(Module::iterator f = Mod.begin(), fend = Mod.end(); f != fend; ++f){
        unsigned order = 0;
        if (!(*f).isDeclaration()){
          LoopInfo& LI = getAnalysis<LoopInfo>(*f);
          
          for(LoopInfo::iterator l = LI.begin(), lend = LI.end(); l != lend; ++l){
            std::vector<std::string> str_repr;
            std::map<opcode, unsigned> op_counts;
            for(Loop::block_iterator i = (*l)->block_begin(), iend = (*l)->block_end(); i != iend; ++i){              
              // number of instructions per basic block
              nipbb += (*i)->size();
              ++numbb;
              for(BasicBlock::iterator j = (*i)->begin(), jend = (*i)->end(); j != jend; ++j){
                // get opcode of instruction
                opcode op = j->getOpcodeName();
                // get Type of instruction
                std::string type_str_;
                llvm::raw_string_ostream rso(type_str_);
                j->getType()->print(rso);
                std::string type_str = rso.str();
                // get number of operands
                std::string num_op = std::to_string(j->getNumOperands());
                // add all operand types as well
                std::string op_types("");
                for (unsigned z = 0, num_ops = j->getNumOperands(); z != num_ops; ++z){           
                  std::string cur_op_str_;
                  llvm::raw_string_ostream rso(cur_op_str_);                 
                  j->getOperand(z)->getType()->print(rso);
                  op_types += rso.str();
                }
                
                // create string and add all info in predefined order
                std::string str("");
                str += op + type_str + num_op + op_types;
                // push string to representation
                //errs () << str << "\n";
                str_repr.push_back(str);
                // finally update opcodes for locality sensitive hashing
                ++op_counts[op];
              }
            }
            loop_info li{str_repr, op_counts, (*l)->getStartLoc(), order++,f};
            loops_infos.push_back(li);
          }
        }
      }
      errs () << "av num of inst per basic block: " << nipbb / (double) numbb << "\n";
      /* 
         compute parameters of LSH
         N = number of loops
         D = number of dimensions, for us it is the distinct number
         K = number of hyperplanes, O (log N)
         LLVM IR opcodes
         L = number of times we repeat computation
      */

      int N = loops_infos.size();
      int K = my_min(std::round(std::log2(N)),32); // 32 bit hash max
      int D = 57; // depends on LLVM release
      int L = 10; 
      
      // setup random generation
      std::random_device rd;
      std::mt19937_64 e(rd());
      std::normal_distribution<> normal_dist(0, 1);
      
      // initialise L hash tables
      std::vector<std::map<uint32_t,std::vector<loop_info>>> hMaps(L);
      // populate all hashMaps
      errs() << "Place loops in buckets...\n";
      for (int l = 0; l < L; ++l){
        errs() <<"L=" << l << "\n";
        // hyperplanes - we store them as normal vectors to planes
        std::vector<std::vector<double>> hyperplanes(K);
        /* 
           generate hyperplanes - https://math.stackexchange.com/questions/444700/uniform-distribution-on-the-surface-of-unit-sphere?rq=1
           generate D numbers from a standart normal N(0,1), normalise the vector
           result is uniform on the unit D-sphere
        */
        // create hyperplanes
        for (int i = 0; i < K; ++i){
          std::vector<double> w(D);
          double sum_sq = 0.0;
          for (int j = 0; j < D; ++j){
            double s = normal_dist(e); 
            w.at(j) = s;
            sum_sq += s*s;
          }
          // normalise w
          for(auto& el: w){
            el /= std::sqrt(sum_sq);
          }
          hyperplanes.at(i) = w;
        }
        std::map<uint32_t,std::vector<loop_info>> hMap;
        for(auto& li: loops_infos){
          uint32_t hash = 0;
          uint32_t mask = 1;
          for(int k = 0; k < K; ++k){
            double dotp = dot(hyperplanes.at(k), li.op_counts);
            if (dotp > 0){
              hash |= mask;
            }
            mask <<= 1; 
          }
          if (hMap.find(hash) == hMap.end()){
            std::vector<loop_info> bucket;
            bucket.push_back(li);
            hMap[hash] = bucket;
          }
          else hMap[hash].push_back(li);
        }
        hMaps.push_back(hMap);
      }
      // do (N/2^k)^2/2 comparisons in every bucket
      errs() << "Do N^2 comparisons per bucket\n";
      for (int l = 0; l < L; ++l){
        errs() << "L="<< l << "\n";
        for(auto& p : hMaps.at(L)){
          std::vector<loop_info> loops = p.second;
          int count = 0;
          double sum = 0.0;
          for(auto l = loops.begin(); l < loops.end(); ++l){
            for(auto p = loops.begin(); p < loops.end(); ++p){
              if (l > p){
                double sim = analyse_similarity_lcs(l->str_repr, p->str_repr);
                if (sim > 0.9){
                  l->dbLoc.print(Mod.getContext(), errs());
                  errs() << " ";
                  p->dbLoc.print(Mod.getContext(), errs());
                  errs() << "\n";
                }
                sum += sim;
                ++count;
                // do k-way merge
              }
            }
          }
        }
      }
      
      errs() << "K: " << K << "\n";
      return false;
    }
    // computes dot product of normal w and opcode -> count map
    double dot(std::vector<double> w, std::map<opcode, unsigned> m){
      double sum = 0.0;
      for(auto& e: m){
        sum += w[op2dim[e.first]] * e.second;
      }
      return sum;
    }

    double analyse_similarity_lcs(std::vector<std::string> str_repr1,std::vector<std::string> str_repr2){
      // LCS
      if (str_repr1.size() == 0 || str_repr2.size() == 0) return 0;
      auto n = str_repr1.size();
      auto m = str_repr2.size();
      std::vector<std::vector<int>> C(m+1);
      for(size_t i = 0; i <= m; ++i){
        C.at(i) = std::vector<int>(n+1);
      }
      // all elements are allready 0
      // instead of these 2 loops we just
      // pass pointers to the loops 
      // do the LCS on a basic block level
      // if the two loops are structurally the same
      // then we do LCS on the basic blocks
      // and get the average similarity per loop?
      // better than nothing...
      // can reuse enumerate && isEqual
      // to insert the extra control flow
      // we need to get the places where we have a diff
      for (size_t i = 1; i <= m; ++i){
        for (size_t j = 1; j <= n; ++j){
          // instead of encoding as string - pass instructions?
          // first compare loops CFGS - if structurally similar
          // then to go on to do lcs on the instructions
          // if they aren't phis or control flow
          if(str_repr2.at(i-1).compare(str_repr1.at(j-1)) == 0){
            C.at(i).at(j) = C.at(i-1).at(j-1) + 1;             
          }
          else{
            C.at(i).at(j) = std::max(C.at(i).at(j-1),C.at(i-1).at(j));
          }
        }
      } 
      double nom = (double) 2*C.at(m).at(n);
      double denom = (double) str_repr1.size() + str_repr2.size();
      return nom/denom;
    }
    double analyse_similarity_lcss(size_t n, size_t m, std::vector<std::string> str_repr1,std::vector<std::string> str_repr2){
      if (n == 0 || m == 0 || str_repr1.size() == 0 || str_repr2.size() == 0) return 0;
      std::vector<std::vector<int>> C(m+1);
      for(size_t i = 0; i <= m; ++i){
        C.at(i) = std::vector<int>(n+1);
      }
      // all elements are allready 0
      int z = 0;
      for (size_t i = 1; i <= m; ++i){
        for (size_t j = 1; j <= n; ++j){
          if(str_repr2.at(i-1).compare(str_repr1.at(j-1)) == 0){
            C.at(i).at(j) = C.at(i-1).at(j-1) + 1;
          }
          z = std::max(z,C.at(i).at(j));
        }
      } 
      double nom = (double) 2*z;
      double denom = (double) str_repr1.size() + str_repr2.size();
      return nom/denom;
    }
    void printLoop(std::vector<std::string> v){
      for(auto x: v){
        errs() << x;
      }
    }
  };

  class LocalitySensitiveHashing{
    
  };
}

char CodeSimilarityAnalyser::ID = 0;
static RegisterPass<CodeSimilarityAnalyser> X("codesimilarityanalyser", "Code Similarity Analyser Pass", false, false);
//asd
