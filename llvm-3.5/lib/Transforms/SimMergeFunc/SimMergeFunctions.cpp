//===- MergeFunctions.cpp - Merge identical functions ---------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This pass looks for equivalent functions that are mergable and folds them.
//
// A hash is computed from the function, based on its type and number of
// basic blocks.
//
// Once all hashes are computed, we perform an expensive equality comparison
// on each function pair. This takes n^2/2 comparisons per bucket, so it's
// important that the hash function be high quality. The equality comparison
// iterates through each instruction in each basic block.
//
// When a match is found the functions are folded. If both functions are
// overridable, we move the functionality into a new internal function and
// leave two overridable thunks to it.
//
//===----------------------------------------------------------------------===//
//
// Future work:
//
// * virtual functions.
//
// Many functions have their address taken by the virtual function table for
// the object they belong to. However, as long as it's only used for a lookup
// and call, this is irrelevant, and we'd like to fold such functions.
//
// * switch from n^2 pair-wise comparisons to an n-way comparison for each
// bucket.
//
// * be smarter about bitcasts.
//
// In order to fold functions, we will sometimes add either bitcast instructions
// or bitcast constant expressions. Unfortunately, this can confound further
// analysis since the two functions differ where one has a bitcast and the
// other doesn't. We should learn to look through bitcasts.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "simmergefunc" 

#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/FoldingSet.h"
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/Statistic.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopInfoImpl.h"
#include "llvm/Analysis/LoopIterator.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/DominanceFrontier.h"

#include "llvm/IR/CallSite.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InlineAsm.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/ValueHandle.h"

#include "llvm/Pass.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/Format.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/Transforms/IPO.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#include "llvm/Transforms/Utils/SimplifyIndVar.h"
#include "llvm/Transforms/Utils/ValueMapper.h"

#include <list>
#include <vector>

using namespace llvm;

static bool compareTypes(Type*, Type*);

STATISTIC(NumFunctionsMerged, "Number of functions merged");
STATISTIC(NumThunksWritten, "Number of thunks generated");
STATISTIC(NumAliasesWritten, "Number of aliases generated");
STATISTIC(NumDoubleWeak, "Number of new functions created");

// avg sim, inst thresh, num fun args, num BBs. 
// (TD,*l,*p,0.80,0,6,5); - ok for bzip2 + make all phis different

static cl::opt<unsigned> MergeMinInsts("mergefunc-min-insts",
  cl::Hidden, cl::init(10),
  cl::desc("Min instructions required to even consider single block fns"));

static cl::opt<unsigned> MergeDifferingMinInsts("mergefunc-diff-min-insts",
  cl::Hidden, cl::init(15),
  cl::desc("Min instructions required to try merging differing functions"));

static cl::opt<unsigned> MergeMaxDiffering("mergefunc-max-diff",
  cl::Hidden, cl::init(86),
  cl::desc("Maximum number of differing instructions allowed"));

static cl::opt<unsigned> MergeMinSimilarity("mergefunc-min-similarity",
  cl::Hidden, cl::init(70),
  cl::desc("Minimum percentage of similar instructions required"));

// my statics
static cl::opt<double> MinSim("min-sim",
                                       cl::Hidden, cl::init(0.8),
                                       cl::desc("Min similarity required to merge loops"));
static cl::opt<unsigned> MinInstructions("min-insts",
                                       cl::Hidden, cl::init(10),
                                       cl::desc("Min num of instructions required to merge loops"));
static cl::opt<unsigned> MaxInstructions("max-insts",
                                       cl::Hidden, cl::init(300),
                                       cl::desc("Max num of instructions required to merge loops"));
static cl::opt<unsigned> MinNumCommonValues("min-num-common-values",
                                       cl::Hidden, cl::init(0),
                                       cl::desc("Minimum number of common values - this will be the number of parameters of the merged fun"));
static cl::opt<unsigned> MaxNumCommonValues("max-num-common-values",
                                       cl::Hidden, cl::init(6),
                                       cl::desc("Maximum number of common values - this will be the number of parameters of the merged fun"));
static cl::opt<unsigned> MinNumBBs("min-num-bbs",
                                       cl::Hidden, cl::init(0),
                                       cl::desc("Minimum number of basic block "));
static cl::opt<unsigned> MaxNumBBs("max-num-bbs",
                                       cl::Hidden, cl::init(20),
                                       cl::desc("Maximum number of basic block "));
static cl::opt<unsigned> SimilarityMetric("sim-metric",
                                       cl::Hidden, cl::init(0),
                                       cl::desc("Similarity metric to pick. 0 (default) is for instructuion weighted avg similarity, 1 is for average similarity "));
static cl::opt<unsigned> UseHeap("use-heap",
                                       cl::Hidden, cl::init(1),
                                       cl::desc("Should we use a heap to select which pairs to merge - default is yes (1), otherwise merge pairs as they are found "));

static void cloneLoop(Loop* L, std::vector<BasicBlock*>& result, ValueToValueMapTy& vmap);
static void deleteClonedLoop(std::vector<BasicBlock*> clonedBBs);
static bool isLikelyProfitableToMerge(unsigned comb_insts, unsigned num_fun_args, unsigned fun_insts, unsigned slack);
static void getDefs(Loop* L, std::vector<Instruction*>& defs);

static unsigned module_insts(Module& M){
  unsigned n = 0;
  for(auto fi = M.begin(), fe = M.end(); fi != fe; ++fi){
    if (fi->isDeclaration())
      continue;
    for(auto bi = fi->begin(), be = fi->end(); bi != be; ++bi){
      for(auto ii = bi->begin(), ie = bi->end(); ii != ie; ++ii){
        ++n;
      }
    }
  }
  return n;
}
static unsigned module_cfg(Module& M){
  unsigned n = 0;
  for(auto fi = M.begin(), fe = M.end(); fi != fe; ++fi){
    if (fi->isDeclaration())
      continue;
    for(auto bi = fi->begin(), be = fi->end(); bi != be; ++bi){
      ++n;
    }
  }
  return n;
}
static unsigned loop_insts(Loop* l){
  unsigned n = 0;
  if (l->getLoopDepth() != 1)
    return n;
  for(auto bi = l->block_begin(), be = l->block_end(); bi != be; ++bi){
    for(auto ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
      ++n;
    }
  }
  return n;
}
static const char *MERGED_SUFFIX = ".mrg";

/// Returns the type id for a type to be hashed. We turn pointer types into
/// integers here because the actual compare logic below considers pointers and
/// integers of the same size as equal.
static Type::TypeID getTypeIDForHash(Type *Ty) {
  if (Ty->isPointerTy())
    return Type::IntegerTyID;
  return Ty->getTypeID();
}

/// Creates a hash-code for the function which is the same for any two
/// functions that will compare equal, without looking at the instructions
/// inside the function.
static unsigned profileFunction(const Function *F) {
  FunctionType *FTy = F->getFunctionType();

  FoldingSetNodeID ID;
  ID.AddInteger(F->size());
  ID.AddInteger(F->getCallingConv());
  ID.AddBoolean(F->hasGC());
  ID.AddBoolean(FTy->isVarArg());
  ID.AddInteger(getTypeIDForHash(FTy->getReturnType()));
  for (unsigned i = 0, e = FTy->getNumParams(); i != e; ++i)
    ID.AddInteger(getTypeIDForHash(FTy->getParamType(i)));
  if (F->size())
    ID.AddInteger(F->front().size());
  return ID.ComputeHash();
}

/// Create a cast instruction if needed to cast V to type DstType. We treat
/// pointer and integer types of the same bitwidth as equivalent, so this can be
/// used to cast them to each other where needed. The function returns the Value
/// itself if no cast is needed, or a new CastInst instance inserted before
/// InsertBefore. The integer type equivalent to pointers must be passed as
/// IntPtrType (get it from DataLayout). This is guaranteed to generate no-op
/// casts, otherwise it will assert.
static Value *createCastIfNeeded(Value *V, Type *DstType,
                                 Instruction *InsertBefore, Type *IntPtrType) {
  if (V->getType() == DstType)
    return V;

  CastInst *Result;
  Type *OrigType = V->getType();

  if (OrigType->isPointerTy() &&
      (DstType->isIntegerTy() || DstType->isPointerTy())) {
    Result = CastInst::CreatePointerCast(V, DstType, "", InsertBefore);
  } else if (OrigType->isIntegerTy() && DstType->isPointerTy() &&
             OrigType == IntPtrType) {
    // Int -> Ptr
    Result = CastInst::Create(CastInst::IntToPtr, V, DstType, "", InsertBefore);
  } else {
    llvm_unreachable("Can only cast int -> ptr or ptr -> (ptr or int)");
  }

  assert(cast<CastInst>(Result)->isNoopCast(IntPtrType) &&
         "Cast is not a no-op cast. Potential loss of precision");

  return Result;
}

/// Replace Inst1 by a switch statement that executes Inst1 or one of Inst2s
/// depending on the value of SwitchVal. If a value in Inst2s is NULL, it
/// defaults to executing Inst1. Returns set of terminator instructions of newly
/// created switch blocks in Ret.
///
/// For instance, the transformation may look as follows:
///         ...Head...
///           Inst1           with all of Insts2s without parents
///         ...Tail...
///  into
///         ...Head...
///           Switch
///         /     |       \            ...
///    (default) (1)       (2)
///      Inst1   Inst2s[0] Inst2s[1]
///     Ret[0]   Ret[1]    Ret[2]
///        \      |       /
///         ...Tail...
///
static void SplitBlockAndInsertSwitch(
                                      Value *SwitchVal, Instruction *Inst1,
                                      SmallVectorImpl<Instruction *> &Inst2s,
                                      SmallVectorImpl<TerminatorInst *> &Ret,
                                      Type *IntPtrTy) {
  // Split block
  BasicBlock *Head = Inst1->getParent();
  BasicBlock *Tail = Head->splitBasicBlock(Inst1);

  // Create default block
  LLVMContext &C = Head->getContext();
  BasicBlock *DefaultBlock = BasicBlock::Create(C, "", Head->getParent(), Tail);

  // Insert switch instruction at end of Head
  TerminatorInst *HeadOldTerm = Head->getTerminator();
  SwitchInst *Switch = SwitchInst::Create(SwitchVal, DefaultBlock,
                                          Inst2s.size());
  ReplaceInstWithInst(HeadOldTerm, Switch);

  // Move instructions into the blocks
  if (Inst1->isTerminator()) {
    Inst1->removeFromParent();
    DefaultBlock->getInstList().push_back(Inst1);
    Ret.push_back(cast<TerminatorInst>(Inst1));
  } else {
    TerminatorInst *DefaultTerm = BranchInst::Create(Tail, DefaultBlock);
    Inst1->moveBefore(DefaultTerm);
    Ret.push_back(DefaultTerm);
  }

  for (unsigned InstPos = 0, InstNum = Inst2s.size(); InstPos < InstNum;
       ++InstPos) {
    Instruction *Inst2 = Inst2s[InstPos];
    if (!Inst2) {
      Ret.push_back(NULL);
      continue;
    }

    BasicBlock *CaseBlock = BasicBlock::Create(C, "", Head->getParent(), Tail);

    if (Inst2->isTerminator()) {
      assert(Inst1->isTerminator() &&
             "Inst1 and Inst2 must both be terminators or non-terminators!");
      CaseBlock->getInstList().push_back(Inst2);
      Ret.push_back(cast<TerminatorInst>(Inst2));
    } else {
      TerminatorInst *CaseTerm = BranchInst::Create(Tail, CaseBlock);
      Inst2->insertBefore(CaseTerm);
      Ret.push_back(CaseTerm);
    }

    Switch->addCase(ConstantInt::get(cast<IntegerType>(SwitchVal->getType()),
                                     InstPos+1),
                    CaseBlock);
  }

  // If Inst1 (and Inst2s) are TerminatorInst's, Tail will be empty and can be
  // deleted now. We also need to update PHI nodes to add the additional
  // incoming blocks from the SwitchInst.
  bool HavePhi = false;
  if (Inst1->isTerminator()) {
    for (succ_iterator I = succ_begin(DefaultBlock), E = succ_end(DefaultBlock);
         I != E; ++I) {
      BasicBlock *Successor = *I;
      PHINode *Phi;

      for (BasicBlock::iterator II = Successor->begin();
           (Phi = dyn_cast<PHINode>(II)); ++II)
        for (unsigned ValId = 0, ValEnd = Phi->getNumIncomingValues();
             ValId != ValEnd; ++ValId)
          if (Phi->getIncomingBlock(ValId) == Tail) {
            Phi->setIncomingBlock(ValId, DefaultBlock);

            bool Inst1UsedInPhi = false;
            if (Phi->getIncomingValue(ValId) == Inst1) {
              Inst1UsedInPhi = true;
              HavePhi = true;
            }

            SmallVectorImpl<TerminatorInst *>::iterator
              SwitchI = Ret.begin(), SwitchE = Ret.end();
            for (++SwitchI; SwitchI != SwitchE; ++SwitchI) {
              if (!*SwitchI)
                continue;
              if (Inst1UsedInPhi)
                Phi->addIncoming(*SwitchI, (*SwitchI)->getParent());
              else
                Phi->addIncoming(Phi->getIncomingValue(ValId),
                                 (*SwitchI)->getParent());
            }
          }
    }

    Tail->eraseFromParent();
  }

  // If the instructions have uses, we need to insert a PHI node
  if (!Inst1->use_empty() && !HavePhi) {
    // We treat all pointer types as equal, so we may need to insert
    // a bitcast to ensure that all incoming values of the PHI node have the
    // same type
    Type *F1IType = Inst1->getType();
    BasicBlock *TailBB = Ret[0]->getSuccessor(0);
    PHINode *Phi = PHINode::Create(F1IType, Inst2s.size(), "",
                                   &TailBB->front());
    Inst1->replaceAllUsesWith(Phi);

    Phi->addIncoming(Inst1, Inst1->getParent());
    for (unsigned FnI = 0, FnE = Inst2s.size(); FnI != FnE; ++FnI) {
      Instruction *F2InstInNewF = Inst2s[FnI];
      if (!F2InstInNewF)
        continue;

      if (F2InstInNewF->getType() != F1IType) {
        assert(!F2InstInNewF->isTerminator() &&
               "Cannot cast result of terminator instruction");

        F2InstInNewF = cast<Instruction>(
                                         createCastIfNeeded(F2InstInNewF,
                                                            F1IType,
                                                            Ret[FnI+1],
                                                            IntPtrTy));
      }

      Phi->addIncoming(F2InstInNewF, F2InstInNewF->getParent());
    }
  }
}

/// Insert function NewF into module, placing it immediately after the
/// existing function PredF. If PredF does not exist, insert at the end.
static void insertFunctionAfter(Function *NewF, Function *PredF) {
  Module *M = PredF->getParent();
  Module::FunctionListType &FList = M->getFunctionList();

  for (Module::FunctionListType::iterator I = FList.begin(), E = FList.end();
       I != E; ++I) {
    if (PredF == I) {
      FList.insertAfter(I, NewF);
      return;
    }
  }

  // Couldn't find PredF, insert at end
  FList.push_back(NewF);
}

namespace {

  /// ComparableFunction - A struct that pairs together functions with a
  /// DataLayout so that we can keep them together as elements in the DenseSet.
  class ComparableFunction {
  public:
    ComparableFunction(Function *Func)
      : Func(Func), IsNew(true) {}

    Function *getFunc() const { return Func; }
    bool isNew() const { return IsNew; }

    // Drops AssertingVH reference to the function. Outside of debug mode, this
    // does nothing.
    void release() {
      assert(Func &&
             "Attempted to release function twice, or release empty/tombstone!");
      Func = NULL;
    }

    void markCompared() {
      IsNew = false;
    }
  private:

    AssertingVH<Function> Func;
    bool IsNew;

  };

}

namespace {

  /// FunctionComparator - Compares two functions to determine whether or not
  /// they will generate machine code with the same behaviour. DataLayout is
  /// used if available. The comparator always fails conservatively (erring on the
  /// side of claiming that two functions are different).
  class FunctionComparator {
  public:
    FunctionComparator(const DataLayout *TD, Function *F1, Function *F2)
      : isDifferent(false), isNotMergeable(false),
        BasicBlockCount(0), InstructionCount(0), DifferingInstructionsCount(0),
        F1(F1), F2(F2), SimilarityMetric(0), TD(TD) {}

    /// Test whether the two functions have equivalent behaviour. Returns true if
    /// they are equal or can be merged, false if not.
    bool compare();

    /// Indicate whether the two functions are an exact match after comparison
    bool isExactMatch();

    /// Indicate whether the two functions candidates for merging after comparison
    bool isMergeCandidate();

    /// Get a similarity metric between the two functions. Higher means more
    /// similar.
    unsigned getSimilarityMetric() {
      if (!SimilarityMetric)
        SimilarityMetric = (unsigned)(((float)InstructionCount -
                                       DifferingInstructionsCount)/InstructionCount*10000);
      return SimilarityMetric;
    }

    Function *getF1() { return F1; }
    Function *getF2() { return F2; }
    ValueToValueMapTy &getF1toF2Map() { return id_map; }
    ValueToValueMapTy &getF2toF1Map() { return seen_values; }
    const DataLayout *getDataLayout() { return TD; }

    /// Assign or look up previously assigned numbers for the two values, and
    /// return whether the numbers are equal. Numbers are assigned in the order
    /// visited. If NoSelfRef is set, F1 and F2 are not assigned to each other
    /// (treated as 'equal').
    bool enumerate(const Value *V1, const Value *V2, bool NoSelfRef=false);

    /// Compare two Types, treating all pointer types as equal.
    bool isEquivalentType(Type *Ty1, Type *Ty2) const;

    /// Instructions that differ between the two functions (F1's -> F2's inst).
    DenseMap<const Instruction *, const Instruction *> DifferingInstructions;

    /// Instructions that reference F1/F2 itself (recursive calls etc.)
    /// These may need special treatment when merging differing functions.
    DenseMap<const Instruction *, const Instruction *> SelfRefInstructions;

    bool isDifferent;
    bool isNotMergeable;

    // Comparison statistics
    unsigned BasicBlockCount;
    unsigned InstructionCount;
    unsigned DifferingInstructionsCount;

  private:
    /// Test whether two basic blocks have equivalent behaviour. Returns true if
    /// they are equal or can be merged, false if not. PHINodes are not compared
    /// in this function, but added to the PHIsFound list for delayed processing.
    bool compare(const BasicBlock *BB1, const BasicBlock *BB2,
                 std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound);

    /// Compare pairs of PHI nodes. Returns true if all pairs are equal or can
    /// be merged, false if not.
    bool comparePHIs(
                     const std::list<std::pair<const PHINode*,const PHINode*> > &PHIs);

    /// Compare two Instructions for equivalence, similar to
    /// Instruction::isSameOperationAs but with modifications to the type
    /// comparison.
    bool isEquivalentOperation(const Instruction *I1,
                               const Instruction *I2) const;

    /// Compare two GEPs for equivalent pointer arithmetic.
    bool isEquivalentGEP(const GEPOperator *GEP1, const GEPOperator *GEP2);
    bool isEquivalentGEP(const GetElementPtrInst *GEP1,
                         const GetElementPtrInst *GEP2) {
      return isEquivalentGEP(cast<GEPOperator>(GEP1), cast<GEPOperator>(GEP2));
    }

    // The two functions undergoing comparison.
    Function *F1, *F2;

    unsigned SimilarityMetric;

    const DataLayout *TD;
    ValueToValueMapTy id_map;
    ValueToValueMapTy seen_values;
  };
  class FunctionPtr {
    AssertingVH<Function> F;
    const DataLayout *DL;

  public:
    FunctionPtr(Function *F, const DataLayout *DL) : F(F), DL(DL) {}
    Function *getFunc() const { return F; }
    void release() { F = 0; }
    // TODO: comment out and see if it compiles
    bool operator<(const FunctionPtr &RHS) const {
      return (FunctionComparator(DL, F, RHS.getFunc()).compare());
    }
  };
}

// Any two pointers in the same address space are equivalent, intptr_t and
// pointers are equivalent. Otherwise, standard type equivalence rules apply.
bool FunctionComparator::isEquivalentType(Type *Ty1, Type *Ty2) const {

  PointerType *PTy1 = dyn_cast<PointerType>(Ty1);
  PointerType *PTy2 = dyn_cast<PointerType>(Ty2);

  if (TD) {
    if (PTy1 && PTy1->getAddressSpace() == 0) Ty1 = TD->getIntPtrType(Ty1);
    if (PTy2 && PTy2->getAddressSpace() == 0) Ty2 = TD->getIntPtrType(Ty2);
  }

  if (Ty1 == Ty2)
    return true;

  if (Ty1->getTypeID() != Ty2->getTypeID())
    return false;

  switch (Ty1->getTypeID()) {
  default:
    llvm_unreachable("Unknown type!");
    // Fall through in Release mode.
  case Type::IntegerTyID:
  case Type::VectorTyID:
    // Ty1 == Ty2 would have returned true earlier.
    return false;

  case Type::VoidTyID:
  case Type::FloatTyID:
  case Type::DoubleTyID:
  case Type::X86_FP80TyID:
  case Type::FP128TyID:
  case Type::PPC_FP128TyID:
  case Type::LabelTyID:
  case Type::MetadataTyID:
    return true;

  case Type::PointerTyID: {
    assert(PTy1 && PTy2 && "Both types must be pointers here.");
    return PTy1->getAddressSpace() == PTy2->getAddressSpace();
  }

  case Type::StructTyID: {
    StructType *STy1 = cast<StructType>(Ty1);
    StructType *STy2 = cast<StructType>(Ty2);
    if (STy1->getNumElements() != STy2->getNumElements())
      return false;

    if (STy1->isPacked() != STy2->isPacked())
      return false;

    for (unsigned i = 0, e = STy1->getNumElements(); i != e; ++i) {
      if (!isEquivalentType(STy1->getElementType(i), STy2->getElementType(i)))
        return false;
    }
    return true;
  }

  case Type::FunctionTyID: {
    FunctionType *FTy1 = cast<FunctionType>(Ty1);
    FunctionType *FTy2 = cast<FunctionType>(Ty2);
    if (FTy1->getNumParams() != FTy2->getNumParams() ||
        FTy1->isVarArg() != FTy2->isVarArg())
      return false;

    if (!isEquivalentType(FTy1->getReturnType(), FTy2->getReturnType()))
      return false;

    for (unsigned i = 0, e = FTy1->getNumParams(); i != e; ++i) {
      if (!isEquivalentType(FTy1->getParamType(i), FTy2->getParamType(i)))
        return false;
    }
    return true;
  }

  case Type::ArrayTyID: {
    ArrayType *ATy1 = cast<ArrayType>(Ty1);
    ArrayType *ATy2 = cast<ArrayType>(Ty2);
    return ATy1->getNumElements() == ATy2->getNumElements() &&
      isEquivalentType(ATy1->getElementType(), ATy2->getElementType());
  }
  }
}

// Determine whether the two operations are the same except that pointer-to-A
// and pointer-to-B are equivalent. This should be kept in sync with
// Instruction::isSameOperationAs.
bool FunctionComparator::isEquivalentOperation(const Instruction *I1,
                                               const Instruction *I2) const {
  // Differences from Instruction::isSameOperationAs:
  //  * replace type comparison with calls to isEquivalentType.
  //  * we test for I->hasSameSubclassOptionalData (nuw/nsw/tail) at the top
  //  * because of the above, we don't test for the tail bit on calls later on
  
  // printing type
  // https://stackoverflow.com/questions/8721115/string-representation-of-llvmtype-structure
  std::string type_str;
  llvm::raw_string_ostream rso(type_str);
  I1->getType()->print(rso);
  DEBUG(errs() << "bibo e pi4, " << rso.str() << "\n");
  //
  if (I1->getOpcode() != I2->getOpcode() ||
      I1->getNumOperands() != I2->getNumOperands() ||
      !isEquivalentType(I1->getType(), I2->getType()) ||
      !I1->hasSameSubclassOptionalData(I2))
    return false;

  // We have two instructions of identical opcode and #operands.  Check to see
  // if all operands are the same type
  for (unsigned i = 0, e = I1->getNumOperands(); i != e; ++i)
    if (!isEquivalentType(I1->getOperand(i)->getType(),
                          I2->getOperand(i)->getType()))
      return false;

  // Check special state that is a part of some instructions.
  if (const LoadInst *LI = dyn_cast<LoadInst>(I1))
    return LI->isVolatile() == cast<LoadInst>(I2)->isVolatile() &&
      LI->getAlignment() == cast<LoadInst>(I2)->getAlignment() &&
      LI->getOrdering() == cast<LoadInst>(I2)->getOrdering() &&
      LI->getSynchScope() == cast<LoadInst>(I2)->getSynchScope();
  if (const StoreInst *SI = dyn_cast<StoreInst>(I1))
    return SI->isVolatile() == cast<StoreInst>(I2)->isVolatile() &&
      SI->getAlignment() == cast<StoreInst>(I2)->getAlignment() &&
      SI->getOrdering() == cast<StoreInst>(I2)->getOrdering() &&
      SI->getSynchScope() == cast<StoreInst>(I2)->getSynchScope();
  if (const AllocaInst *AI = dyn_cast<AllocaInst>(I1))
    return AI->getArraySize() == cast<AllocaInst>(I2)->getArraySize() &&
      AI->getAllocatedType() == cast<AllocaInst>(I2)->getAllocatedType();
  if (const CmpInst *CI = dyn_cast<CmpInst>(I1))
    return CI->getPredicate() == cast<CmpInst>(I2)->getPredicate();
  if (const CallInst *CI = dyn_cast<CallInst>(I1))
    return CI->getCallingConv() == cast<CallInst>(I2)->getCallingConv() &&
      CI->getAttributes() == cast<CallInst>(I2)->getAttributes();
  if (const InvokeInst *CI = dyn_cast<InvokeInst>(I1))
    return CI->getCallingConv() == cast<InvokeInst>(I2)->getCallingConv() &&
      CI->getAttributes() == cast<InvokeInst>(I2)->getAttributes();
  if (const InsertValueInst *IVI = dyn_cast<InsertValueInst>(I1))
    return IVI->getIndices() == cast<InsertValueInst>(I2)->getIndices();
  if (const ExtractValueInst *EVI = dyn_cast<ExtractValueInst>(I1))
    return EVI->getIndices() == cast<ExtractValueInst>(I2)->getIndices();
  if (const FenceInst *FI = dyn_cast<FenceInst>(I1))
    return FI->getOrdering() == cast<FenceInst>(I2)->getOrdering() &&
      FI->getSynchScope() == cast<FenceInst>(I2)->getSynchScope();
  // TODO: ask tobias if this is okay
  // if (const AtomicCmpXchgInst *CXI = dyn_cast<AtomicCmpXchgInst>(I1))
  //   return CXI->isVolatile() == cast<AtomicCmpXchgInst>(I2)->isVolatile() &&
  //          CXI->getOrdering() == cast<AtomicCmpXchgInst>(I2)->getOrdering() &&
  //          CXI->getSynchScope() == cast<AtomicCmpXchgInst>(I2)->getSynchScope();
  if (const AtomicRMWInst *RMWI = dyn_cast<AtomicRMWInst>(I1))
    return RMWI->getOperation() == cast<AtomicRMWInst>(I2)->getOperation() &&
      RMWI->isVolatile() == cast<AtomicRMWInst>(I2)->isVolatile() &&
      RMWI->getOrdering() == cast<AtomicRMWInst>(I2)->getOrdering() &&
      RMWI->getSynchScope() == cast<AtomicRMWInst>(I2)->getSynchScope();
  // TODO : should it be >?
  return true;
}

// Determine whether two GEP operations perform the same underlying arithmetic.
bool FunctionComparator::isEquivalentGEP(const GEPOperator *GEP1,
                                         const GEPOperator *GEP2) {
  unsigned AS = GEP1->getPointerAddressSpace();
  if (AS != GEP2->getPointerAddressSpace())
    return false;

  if (TD) {
    // When we have target data, we can reduce the GEP down to the value in bytes
    // added to the address.
    unsigned BitWidth = TD ? TD->getPointerSizeInBits(AS) : 1;
    APInt Offset1(BitWidth, 0), Offset2(BitWidth, 0);
    if (GEP1->accumulateConstantOffset(*TD, Offset1) &&
        GEP2->accumulateConstantOffset(*TD, Offset2)) {
      return Offset1 == Offset2;
    }
  }

  if (GEP1->getPointerOperand()->getType() !=
      GEP2->getPointerOperand()->getType())
    return false;

  if (GEP1->getNumOperands() != GEP2->getNumOperands())
    return false;

  for (unsigned i = 0, e = GEP1->getNumOperands(); i != e; ++i) {
    if (!enumerate(GEP1->getOperand(i), GEP2->getOperand(i)))
      return false;
  }

  return true;
}

// Compare two values used by the two functions under pair-wise comparison. If
// this is the first time the values are seen, they're added to the mapping so
// that we will detect mismatches on next use.
bool FunctionComparator::enumerate(const Value *V1, const Value *V2,
                                   bool NoSelfRef/*=false*/) {
  // Check for function @f1 referring to itself and function @f2 referring to
  // itself, or referring to each other, or both referring to either of them.
  // They're all equivalent if the two functions are otherwise equivalent.
  if (!NoSelfRef && V1 == F1 && V2 == F2)
    return true;
  if (!NoSelfRef && V1 == F2 && V2 == F1)
    return true;

  if (const Constant *C1 = dyn_cast<Constant>(V1)) {
    if (V1 == V2) return true;
    const Constant *C2 = dyn_cast<Constant>(V2);
    if (!C2) return false;
    // TODO: constant expressions with GEP or references to F1 or F2.
    if (C1->isNullValue() && C2->isNullValue() &&
        isEquivalentType(C1->getType(), C2->getType()))
      return true;
    // Try bitcasting C2 to C1's type. If the bitcast is legal and returns C1
    // then they must have equal bit patterns. Aggregate types cannot be
    // bitcast.
    if (C1->getType()->isAggregateType() || C2->getType()->isAggregateType())
      return false;
    return C1->getType()->canLosslesslyBitCastTo(C2->getType()) &&
      C1 == ConstantExpr::getBitCast(const_cast<Constant*>(C2), C1->getType());
  }

  if (isa<InlineAsm>(V1) || isa<InlineAsm>(V2))
    return V1 == V2;

  // Check that V1 maps to V2. If we find a value that V1 maps to then we simply
  // check whether it's equal to V2. When there is no mapping then we need to
  // ensure that V2 isn't already equivalent to something else. For this
  // purpose, we track the V2 values in a set.

  ValueToValueMapTy::iterator I = id_map.find(V1);
  if (I != id_map.end()){
    DEBUG(errs() << "LOOOL " <<V1->getName() << " " << V2->getName() << "\n");
    return V2 == I->second;
  }
  // FIXME: Const casts!!!
  if (!seen_values.insert(std::make_pair(V2, const_cast<Value *>(V1))).second)
    return false;
  id_map[V1] = const_cast<Value *>(V2);
  return true;
}

/// Test whether two basic blocks have equivalent behaviour. Returns true if the
/// blocks can be merged, false if they cannot. Differing instructions are
/// recorded in DifferingInstructions.
bool FunctionComparator::compare(const BasicBlock *BB1, const BasicBlock *BB2,
                                 std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound) {
  BasicBlock::const_iterator F1I, F1E, F2I, F2E;

  for (F1I = BB1->begin(), F1E = BB1->end(),
         F2I = BB2->begin(), F2E = BB2->end();
       F1I != F1E && F2I != F2E; ++F1I, ++F2I) {
    // Skip debug information
    const CallInst *DbgCall;
    while (F1I != F1E && (DbgCall = dyn_cast<CallInst>(F1I)) &&
           DbgCall->getCalledFunction() &&
           DbgCall->getCalledFunction()->hasName() &&
           DbgCall->getCalledFunction()->getName().startswith("llvm.dbg."))
      ++F1I;

    while (F2I != F2E && (DbgCall = dyn_cast<CallInst>(F2I)) &&
           DbgCall->getCalledFunction() &&
           DbgCall->getCalledFunction()->hasName() &&
           DbgCall->getCalledFunction()->getName().startswith("llvm.dbg."))
      ++F2I;

    if (F1I == F1E || F2I == F2E)
      break;

    // Ok, we're dealing with real instructions. Check a few cases that will
    // prevent merging first.

    // Cannot merge insts that differ in whether they have uses
    if (F1I->use_empty() != F2I->use_empty()) {
      // TODO: Could implement merging for this case (would need to introduce a
      // dummy value in the PHI node etc.)
      return false;
    }

    // Cannot merge insts whose types are non-equivalent
    if (!isEquivalentType(F1I->getType(), F2I->getType())) {
      return false;
    }

    // TODO:  Currently cannot merge InvokeInsts with differing result types
    //        that have uses. We cannot push up a bitcast into their block after
    //        them because they are terminators. Would need to insert an
    //        additional BB.
    if (isa<InvokeInst>(F1I) && !F1I->use_empty() &&
        F1I->getType() != F2I->getType())
      return false;

    if (!enumerate(F1I, F2I))
      goto differing_instructions;

    if (const GetElementPtrInst *GEP1 = dyn_cast<GetElementPtrInst>(F1I)) {
      const GetElementPtrInst *GEP2 = dyn_cast<GetElementPtrInst>(F2I);
      if (!GEP2)
        goto differing_instructions;

      if (!enumerate(GEP1->getPointerOperand(), GEP2->getPointerOperand()))
        goto differing_instructions;

      if (!isEquivalentGEP(GEP1, GEP2))
        goto differing_instructions;
    } else if (const PHINode *Phi1 = dyn_cast<PHINode>(F1I)) {
      const PHINode *Phi2 = dyn_cast<PHINode>(F2I);
      // We can't currently merge a PHI and non-PHI instruction
      if (!Phi2)
        return false;

      // We can't currently merge PHI nodes with different numbers of incoming
      // values
      if (F1I->getNumOperands() != F2I->getNumOperands())
        return false;

      // We need to treat PHI nodes specially. Their incoming values may be in a
      // different order even if they are equivalent. We can't compare them
      // until we've seen the incoming blocks and know which values are
      // equivalent. Therefore postpone PHINode comparison until the end.
      PHIsFound->push_back(std::make_pair(Phi1, Phi2));
    } else {
      if (!isEquivalentOperation(F1I, F2I))
        goto differing_instructions;

      assert(F1I->getNumOperands() == F2I->getNumOperands());
      for (unsigned i = 0, e = F1I->getNumOperands(); i != e; ++i) {
        Value *OpF1 = F1I->getOperand(i);
        Value *OpF2 = F2I->getOperand(i);

        if (!enumerate(OpF1, OpF2))
          goto differing_instructions;

        if (OpF1->getValueID() != OpF2->getValueID() ||
            !isEquivalentType(OpF1->getType(), OpF2->getType()))
          goto differing_instructions;

        if ((OpF1 == F1 && OpF2 == F2) || (OpF1 == F2 && OpF2 == F1))
          SelfRefInstructions[F1I] = F2I;
      }
    }
    continue;

  differing_instructions:
    // Cannot merge functions with differing landing pad instructions yet. They
    // would need special treatment which involves updating the corresponding
    // invoke instructions.
    if (isa<LandingPadInst>(F1I))
      return false;

    DifferingInstructions[F1I] = F2I;
  }

  // We cannot currently merge basic blocks with different instruction counts
  return F1I == F1E && F2I == F2E;
}

bool FunctionComparator::comparePHIs(
                                     const std::list<std::pair<const PHINode*,const PHINode*> > &PHIs) {
  if (PHIs.empty())
    return true;

  for (std::list<std::pair<const PHINode*,const PHINode*> >::const_iterator
         I = PHIs.begin(), E = PHIs.end(); I != E; ++I) {
    const PHINode *Phi1 = I->first, *Phi2 = I->second;

    for (unsigned ValId = 0, ValNum = Phi1->getNumIncomingValues();
         ValId < ValNum; ++ValId) {
      Value *Phi1Val = Phi1->getIncomingValue(ValId);

      // Get corresponding Phi2Val
      Value *BBinPhi2Val = getF1toF2Map()[Phi1->getIncomingBlock(ValId)];

      if (!BBinPhi2Val)
        return false; // Currently can't handle differing predecessor blocks

      BasicBlock *BBinPhi2 = cast<BasicBlock>(BBinPhi2Val);
      Value *Phi2Val = Phi2->getIncomingValueForBlock(BBinPhi2);

      // Enumerate the values. If the PHI node references the function itself (a
      // very rare case), we mark it as different (NoSelfRef). This is only
      // necessary for outline merging, not equiv merging. TODO: Make equal
      // merging possible with such PHI nodes.
      if (!enumerate(Phi1Val, Phi2Val,/*NoSelfRef=*/true)) {
        DifferingInstructions[Phi1] = Phi2;
        break;
      }
    }
  }

  return true;
}

// Test whether the two functions have equivalent behaviour.
bool FunctionComparator::compare() {
  // We need to recheck everything, but check the things that weren't included
  // in the hash first.

  if (F1->getAttributes() != F2->getAttributes())
    goto not_mergeable;

  if (F1->hasGC() != F2->hasGC())
    goto not_mergeable;

  if (F1->hasGC() && F1->getGC() != F2->getGC())
    goto not_mergeable;

  if (F1->hasSection() != F2->hasSection())
    goto not_mergeable;

  if (F1->hasSection() && F1->getSection() != F2->getSection())
    goto not_mergeable;

  if (F1->isVarArg() != F2->isVarArg())
    goto not_mergeable;

  if (F1->size() != F2->size())
    goto not_mergeable;

  // TODO: if it's internal and only used in direct calls, we could handle
  // this case too.
  if (F1->getCallingConv() != F2->getCallingConv())
    goto not_mergeable;

  if (!isEquivalentType(F1->getFunctionType(), F2->getFunctionType()))
    goto not_mergeable;

  assert(F1->arg_size() == F2->arg_size() &&
         "Identically typed functions have different numbers of args!");

  // Visit the arguments so that they get enumerated in the order they're
  // passed in.
  for (Function::const_arg_iterator f1i = F1->arg_begin(),
         f2i = F2->arg_begin(), f1e = F1->arg_end(); f1i != f1e; ++f1i, ++f2i) {
    if (!enumerate(f1i, f2i))
      llvm_unreachable("Arguments repeat!");
  }

  // We do a CFG-ordered walk since the actual ordering of the blocks in the
  // linked list is immaterial. Our walk starts at the entry block for both
  // functions, then takes each block from each terminator in order. As an
  // artifact, this also means that unreachable blocks are ignored.
  // NOTE : this is the structured CFB BB walk
  {
    SmallVector<const BasicBlock *, 8> F1BBs, F2BBs;
    SmallSet<const BasicBlock *, 128> VisitedBBs; // in terms of F1.
    std::list<std::pair<const PHINode*,const PHINode*> > PHIsFound;

    F1BBs.push_back(&F1->getEntryBlock());
    F2BBs.push_back(&F2->getEntryBlock());

    VisitedBBs.insert(F1BBs[0]);
    while (!F1BBs.empty()) {
      const BasicBlock *F1BB = F1BBs.pop_back_val();
      const BasicBlock *F2BB = F2BBs.pop_back_val();

      // Check for control flow divergence
      if (!enumerate(F1BB, F2BB))
        goto not_mergeable;

      const TerminatorInst *F1TI = F1BB->getTerminator();
      const TerminatorInst *F2TI = F2BB->getTerminator();

      // TODO: Implement merging of blocks with different numbers of instructions.
      if (F1TI->getNumSuccessors() != F2TI->getNumSuccessors() ||
          F1BB->size() != F2BB->size())
        goto not_mergeable;

      // The actual instruction-by-instruction comparison
      if (!compare(F1BB, F2BB, &PHIsFound))
        goto not_mergeable;

      // FIXME: Count this in compare(F1BB,F2BB) so it doesn't include debug
      // instructions.
      InstructionCount += std::max(F1BB->size(), F2BB->size());

      assert(F1TI->getNumSuccessors() == F2TI->getNumSuccessors());
      for (unsigned i = 0, e = F1TI->getNumSuccessors(); i != e; ++i) {
        if (!VisitedBBs.insert(F1TI->getSuccessor(i)))
          continue;

        F1BBs.push_back(F1TI->getSuccessor(i));
        F2BBs.push_back(F2TI->getSuccessor(i));
      }
    }

    BasicBlockCount = VisitedBBs.size();

    // After we've seen all values and BBs, compare the PHI nodes
    if (!comparePHIs(PHIsFound))
      goto not_mergeable;
  }

  if (DifferingInstructions.size()) {
    // Currently we can't merge vararg functions with differing instructions.
    // TODO: Explore whether this is feasible; the difficult bit is the
    // additional argument we need to add.
    if (F1->isVarArg())
      goto not_mergeable;

    isDifferent = true;
    DifferingInstructionsCount += DifferingInstructions.size();
    // debug only output TODO
    DEBUG(dbgs() << "Similar fns: " << F1->getName() << " and " << F2->getName()
          << " bbs=" << BasicBlockCount << " insts=" << InstructionCount
          << " different=" << DifferingInstructionsCount << " metric="
          << getSimilarityMetric() << '\n');
  }

  return true;

 not_mergeable:
  // Fail: cannot merge the two functions
  isNotMergeable = true;
  return false;
}

bool FunctionComparator::isExactMatch() {
  return (!isNotMergeable && !isDifferent);
}

bool FunctionComparator::isMergeCandidate() {
  if (isNotMergeable)
    return false;

  if (!isDifferent)
    return true;

  // Heuristic when to attempt merging
  if (InstructionCount > MergeDifferingMinInsts &&
      DifferingInstructionsCount <= MergeMaxDiffering &&
      getSimilarityMetric() > MergeMinSimilarity*100) {
    return true;
  } else {
    DEBUG(dbgs() << "Candidate excluded by parameters.\n");
  }

  return false;
}
/// LoopComparator - compares two loops to see if they are the same

/// . DataLayout is
/// used if available. The comparator always fails conservatively (erring on the
/// side of claiming that two functions are different).
class LoopComparator {
public:
  LoopComparator(const DataLayout *TD, Loop *L1, Loop *L2, double threshold, unsigned inst_threshold, unsigned max_fun_args, unsigned max_num_blocks, unsigned sim_metric, unsigned use_heap, bool analysis)
    :  isDifferent(false), isNotMergeable(false), 
       BasicBlockCount(0),  InstructionCount(0), DifferingInstructionsCount(0), lcsL(), lcsP(), bb_lcs2lcs(), bb_lcs2lcs_rev(),
       diffBlocks(), cfg_map(), annots_map(), cfg_for_merge(), annots_for_merge(), loop_to_cfg_map(), cfg_kind_map(), extra_cf_blocks(),
       C_map(), bb_lcs(), extras(), extra_ls(), extra_ps(),
    L1(L1), L2(L2), SimilarityMetric(0),  TD(TD), threshold(threshold), inst_threshold(inst_threshold), max_fun_args(max_fun_args), max_num_blocks(max_num_blocks), sim_metric(sim_metric), use_heap(use_heap), analysis(analysis)  {}
  
  /// Test whether the two loops have equivalent behaviour. Returns true if
  /// they are equal or can be merged, false if not.
  bool compare(Module& M);

  /// Indicate whether the two functions are an exact match after comparison
  bool isExactMatch();

  /// Indicate whether the two functions candidates for merging after comparison
  bool isMergeCandidate();

  /// Get a similarity metric between the two functions. Higher means more
  /// similar.
  unsigned getSimilarityMetric() {
    if (!SimilarityMetric)
      SimilarityMetric = (unsigned)(((float)InstructionCount -
                                     DifferingInstructionsCount)/InstructionCount*10000);
    return SimilarityMetric;
  }

  Loop *getL1() { return L1; }
  Loop *getL2() { return L2; }
  ValueToValueMapTy &getL1toL2Map() { return id_map; }
  ValueToValueMapTy &getL2toL1Map() { return seen_values; }
  const DataLayout *getDataLayout() { return TD; }
  double getSim() { 
    if (!sim_metric) 
      return wavg_sim;
    else
      return avg_sim;
  }
  double getThresh() { return threshold;}
  unsigned getInstThresh() { return inst_threshold;}
  unsigned getMaxFunArgs() { return max_fun_args; }
  unsigned getMaxNumBlocks() { return max_num_blocks; }
  unsigned getUseHeap() { return use_heap;}
  /// Assign or look up previously assigned numbers for the two values, and
  /// return whether the numbers are equal. Numbers are assigned in the order
  /// visited. If NoSelfRef is set, F1 and F2 are not assigned to each other
  /// (treated as 'equal').
  bool enumerate(const Value *V1, const Value *V2, bool NoSelfRef=false);

  /// Compare two Types, treating all pointer types as equal.
  bool isEquivalentType(Type *Ty1, Type *Ty2) const;

  /// Instructions that differ between the two functions (F1's -> F2's inst).
  DenseMap<const Instruction *, const Instruction *> DifferingInstructions;

  /// Instructions that reference F1/F2 itself (recursive calls etc.)
  /// These may need special treatment when merging differing functions.
  DenseMap<const Instruction *, const Instruction *> SelfRefInstructions;

  bool isDifferent;
  bool isNotMergeable;
  
  // Comparison statistics
  unsigned BasicBlockCount;
  unsigned InstructionCount;
  unsigned DifferingInstructionsCount;
  // lcsss - we need one longest common subsequence per basic block
  std::map<const BasicBlock*, std::set<const Instruction*>> lcsL;
  std::map<const BasicBlock*, std::set<const Instruction*>> lcsP;
  // map of corresponding basic blocks
  DenseMap<BasicBlock*, BasicBlock*> bbmap;
  // lcs map of instruction to instruction per BasicBlock
  std::map<const BasicBlock*, std::map<const Instruction*, const Instruction*>> bb_lcs2lcs;
  // reverse map
  std::map<const BasicBlock*, std::map<const Instruction*, const Instruction*>> bb_lcs2lcs_rev;
  // diffblock
  std::map<const BasicBlock*, std::deque<const Instruction*>> diffBlocks;
  // cfg and annots maps
  std::map<BasicBlock*, std::vector<BasicBlock*> > cfg_map;
  std::map<BasicBlock*, std::vector<unsigned> > annots_map;
  // flattened out version with all basic blokcs
  std::vector<BasicBlock*> cfg_for_merge;
  std::vector<unsigned> annots_for_merge;
  // prepare cfg for merge
  void prepareCFGsForMerge(Value* ctrl, BasicBlock* exit);
  void createBBlcsAndExtras();
  // map from bbs of loop to cfg basic blocks
  std::map<BasicBlock*, BasicBlock*> loop_to_cfg_map;
  std::map<BasicBlock*, unsigned> cfg_kind_map;
  // create storage space for extra blocks
  std::vector<BasicBlock*> extra_cf_blocks;
  // map of C matrix to basic blocks
  std::map<BasicBlock*, std::vector<std::vector<int> > > C_map;
  // bc_lcs
  std::vector<BasicBlock*> bb_lcs;
  std::vector<BasicBlock*> extras;
  std::vector<BasicBlock*> extra_ls;
  std::vector<BasicBlock*> extra_ps;
  // first block
  BasicBlock* entry;
  unsigned comb_insts = 0;
  bool canBeMerged(){ return analysis && can_be_merged;}
private:
  double avg_sim = 0.0;
  double wavg_sim = 0.0;
  double threshold;
  unsigned inst_threshold;
  unsigned max_fun_args;
  unsigned max_num_blocks;
  unsigned sim_metric;
  unsigned use_heap;
  bool analysis;
  bool can_be_merged = false;
  unsigned merged_len = 2; // function entry jump and return functions
  unsigned extr_cf = 2; // function entry and exit blocks
  void set_lcsss(std::deque<const Instruction*>& , std::deque<const Instruction*>&, const BasicBlock*, const BasicBlock*);
  /// Test whether two basic blocks have equivalent behaviour. Returns true if
  /// they are equal or can be merged, false if not. PHINodes are not compared
  /// in this function, but added to the PHIsFound list for delayed processing.
  bool compare(const BasicBlock *BB1, const BasicBlock *BB2,
               std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound);

  /// Compare pairs of PHI nodes. Returns true if all pairs are equal or can
  /// be merged, false if not.
  bool comparePHIs(
                   const std::list<std::pair<const PHINode*,const PHINode*> > &PHIs);

  /// Compare two Instructions for equivalence, similar to
  /// Instruction::isSameOperationAs but with modifications to the type
  /// comparison.
  bool isEquivalentOperation(const Instruction *I1,
                             const Instruction *I2) const;

  /// Compare two GEPs for equivalent pointer arithmetic.
  bool isEquivalentGEP(const GEPOperator *GEP1, const GEPOperator *GEP2);
  bool isEquivalentGEP(const GetElementPtrInst *GEP1,
                       const GetElementPtrInst *GEP2) {
    return isEquivalentGEP(cast<GEPOperator>(GEP1), cast<GEPOperator>(GEP2));
  }
  /// do similarity comparison of basic blocks with 
  /// longest common sequence
  std::pair<double, bool> compareSim(const BasicBlock *BB1, const BasicBlock *BB2,  std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound);

  /// check if two instructions can't be merged, returns true if they
  /// can't. also populates a list of phis
  bool not_mergeable(const Instruction *I1, const Instruction *I2, std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound);

  /// given a C matrix from the LCS run, and the two basic block, this gives back
  /// a vector of the longest common subsequence of equivalent instructions
  void backtrack(const std::vector<std::vector<int>>& C, std::vector<const Instruction*> BB1, std::vector<const Instruction*> BB2, int i, int j, std::vector<const Instruction*>& result1, std::vector<const Instruction*>& result2);
  void createDiffBlock(const std::vector<std::vector<int>>& C, std::vector<const Instruction*> BB1, std::vector<const Instruction*> BB2, int i, int j, std::deque<const Instruction*>& diffBlock, std::deque<const Instruction*>& lcs1, std::deque<const Instruction*>& lcs2);

  void createDiffBlockDummy(const std::vector<std::vector<int>>& C, std::vector<const Instruction*> BB1, std::vector<const Instruction*> BB2, int i, int j, std::deque<unsigned>& diffBlock);
  
void createCFGfromDiffBlock(
                            BasicBlock* BB1,
                            std::string parName,
                            std::deque<const Instruction*>& diffBlock,
                            std::deque<unsigned>& annotations,
                            std::vector<BasicBlock*>& cfg,
                            std::vector<unsigned>& bb_annotations);
  /// check if two instructions are differing but mergeable
  bool differing_insts(const Instruction *I1, const Instruction *I2);
  
  // The two functions undergoing comparison.
  Loop *L1, *L2;

  unsigned SimilarityMetric;

  const DataLayout *TD;
  ValueToValueMapTy id_map;
  ValueToValueMapTy seen_values;
};
// class LoopPtr {
//   AssertingVH<Loop> L;
//   const DataLayout *DL;

// public:
//   LoopPtr(Loop *L, const DataLayout *DL) : L(L), DL(DL) {}
//   Loop *getLoop() const { return L; }
//   void release() { L = 0; }
//   // TODO: comment out and see if it compiles
//   bool operator<(const LoopPtr &RHS) const {
//     return (LoopComparator(DL, L, RHS.getLoop()).compare());
//   }
// };


// Any two pointers in the same address space are equivalent, intptr_t and
// pointers are equivalent. Otherwise, standard type equivalence rules apply.
bool LoopComparator::isEquivalentType(Type *Ty1, Type *Ty2) const {

  return Ty1 == Ty2;
    
  PointerType *PTy1 = dyn_cast<PointerType>(Ty1);
  PointerType *PTy2 = dyn_cast<PointerType>(Ty2);
  // remove for now
  if (TD) {
    if (PTy1 && PTy1->getAddressSpace() == 0) Ty1 = TD->getIntPtrType(Ty1);
    if (PTy2 && PTy2->getAddressSpace() == 0) Ty2 = TD->getIntPtrType(Ty2);
  }

  if (Ty1 == Ty2)
    return true;

  if (Ty1->getTypeID() != Ty2->getTypeID())
    return false;

  switch (Ty1->getTypeID()) {
  default:
    llvm_unreachable("Unknown type!");
    // Fall through in Release mode.
  case Type::IntegerTyID:
  case Type::VectorTyID:
    // Ty1 == Ty2 would have returned true earlier.
    return false;

  case Type::VoidTyID:
  case Type::FloatTyID:
  case Type::DoubleTyID:
  case Type::X86_FP80TyID:
  case Type::FP128TyID:
  case Type::PPC_FP128TyID:
  case Type::LabelTyID:
  case Type::MetadataTyID:
    return true;

  case Type::PointerTyID: {
    assert(PTy1 && PTy2 && "Both types must be pointers here.");
    return PTy1->getAddressSpace() == PTy2->getAddressSpace();
  }

  case Type::StructTyID: {
    StructType *STy1 = cast<StructType>(Ty1);
    StructType *STy2 = cast<StructType>(Ty2);
    if (STy1->getNumElements() != STy2->getNumElements())
      return false;

    if (STy1->isPacked() != STy2->isPacked())
      return false;

    for (unsigned i = 0, e = STy1->getNumElements(); i != e; ++i) {
      if (!isEquivalentType(STy1->getElementType(i), STy2->getElementType(i)))
        return false;
    }
    return true;
  }

  case Type::FunctionTyID: {
    FunctionType *FTy1 = cast<FunctionType>(Ty1);
    FunctionType *FTy2 = cast<FunctionType>(Ty2);
    if (FTy1->getNumParams() != FTy2->getNumParams() ||
        FTy1->isVarArg() != FTy2->isVarArg())
      return false;

    if (!isEquivalentType(FTy1->getReturnType(), FTy2->getReturnType()))
      return false;

    for (unsigned i = 0, e = FTy1->getNumParams(); i != e; ++i) {
      if (!isEquivalentType(FTy1->getParamType(i), FTy2->getParamType(i)))
        return false;
    }
    return true;
  }

  case Type::ArrayTyID: {
    ArrayType *ATy1 = cast<ArrayType>(Ty1);
    ArrayType *ATy2 = cast<ArrayType>(Ty2);
    return ATy1->getNumElements() == ATy2->getNumElements() &&
      isEquivalentType(ATy1->getElementType(), ATy2->getElementType());
  }
  }
}

// Determine whether the two operations are the same except that pointer-to-A
// and pointer-to-B are equivalent. This should be kept in sync with
// Instruction::isSameOperationAs.
bool LoopComparator::isEquivalentOperation(const Instruction *I1,
                                           const Instruction *I2) const {
  // Differences from Instruction::isSameOperationAs:
  //  * replace type comparison with calls to isEquivalentType.
  //  * we test for I->hasSameSubclassOptionalData (nuw/nsw/tail) at the top
  //  * because of the above, we don't test for the tail bit on calls later on
  
  // printing type
  // https://stackoverflow.com/questions/8721115/string-representation-of-llvmtype-structure
  // std::string type_str;
  // llvm::raw_string_ostream rso(type_str);
  // I1->getType()->print(rso);
  // DEBUG(errs() << "bibo e pi4, " << rso.str() << "\n");
  //
  if (I1->getOpcode() != I2->getOpcode() ||
      I1->getNumOperands() != I2->getNumOperands() ||
      !isEquivalentType(I1->getType(), I2->getType()) ||
      !I1->hasSameSubclassOptionalData(I2))
    return false;

  // We have two instructions of identical opcode and #operands.  Check to see
  // if all operands are the same type
  for (unsigned i = 0, e = I1->getNumOperands(); i != e; ++i)
    if (!isEquivalentType(I1->getOperand(i)->getType(),
                          I2->getOperand(i)->getType()))
      return false;

  // Check special state that is a part of some instructions.
  if (const LoadInst *LI = dyn_cast<LoadInst>(I1))
    return LI->isVolatile() == cast<LoadInst>(I2)->isVolatile() &&
      LI->getAlignment() == cast<LoadInst>(I2)->getAlignment() &&
      LI->getOrdering() == cast<LoadInst>(I2)->getOrdering() &&
      LI->getSynchScope() == cast<LoadInst>(I2)->getSynchScope();
  if (const StoreInst *SI = dyn_cast<StoreInst>(I1)){
    // DEBUG(errs() << *I1 << " -- " << *I2 << "\n");
    // DEBUG(errs() << (SI->isVolatile() == cast<StoreInst>(I2)->isVolatile()) << "\n");
    // DEBUG(errs() << (SI->getAlignment() == cast<StoreInst>(I2)->getAlignment()) << "\n");
    // DEBUG(errs() << (SI->getOrdering() == cast<StoreInst>(I2)->getOrdering()) << "\n");
    // DEBUG(errs() << (SI->getSynchScope() == cast<StoreInst>(I2)->getSynchScope()) << "\n");
    return SI->isVolatile() == cast<StoreInst>(I2)->isVolatile() &&
      SI->getAlignment() == cast<StoreInst>(I2)->getAlignment() &&
      SI->getOrdering() == cast<StoreInst>(I2)->getOrdering() &&
      SI->getSynchScope() == cast<StoreInst>(I2)->getSynchScope();
  }
  if (const AllocaInst *AI = dyn_cast<AllocaInst>(I1))
    return AI->getArraySize() == cast<AllocaInst>(I2)->getArraySize() &&
      AI->getAllocatedType() == cast<AllocaInst>(I2)->getAllocatedType();
  if (const CmpInst *CI = dyn_cast<CmpInst>(I1))
    return CI->getPredicate() == cast<CmpInst>(I2)->getPredicate();
  if (const CallInst *CI = dyn_cast<CallInst>(I1))
    return CI->getCallingConv() == cast<CallInst>(I2)->getCallingConv() &&
      CI->getAttributes() == cast<CallInst>(I2)->getAttributes();
  if (const InvokeInst *CI = dyn_cast<InvokeInst>(I1))
    return CI->getCallingConv() == cast<InvokeInst>(I2)->getCallingConv() &&
      CI->getAttributes() == cast<InvokeInst>(I2)->getAttributes();
  if (const InsertValueInst *IVI = dyn_cast<InsertValueInst>(I1))
    return IVI->getIndices() == cast<InsertValueInst>(I2)->getIndices();
  if (const ExtractValueInst *EVI = dyn_cast<ExtractValueInst>(I1))
    return EVI->getIndices() == cast<ExtractValueInst>(I2)->getIndices();
  if (const FenceInst *FI = dyn_cast<FenceInst>(I1))
    return FI->getOrdering() == cast<FenceInst>(I2)->getOrdering() &&
      FI->getSynchScope() == cast<FenceInst>(I2)->getSynchScope();
  // TODO: ask tobias if this is okay
  // if (const AtomicCmpXchgInst *CXI = dyn_cast<AtomicCmpXchgInst>(I1))
  //   return CXI->isVolatile() == cast<AtomicCmpXchgInst>(I2)->isVolatile() &&
  //          CXI->getOrdering() == cast<AtomicCmpXchgInst>(I2)->getOrdering() &&
  //          CXI->getSynchScope() == cast<AtomicCmpXchgInst>(I2)->getSynchScope();
  if (const AtomicRMWInst *RMWI = dyn_cast<AtomicRMWInst>(I1))
    return RMWI->getOperation() == cast<AtomicRMWInst>(I2)->getOperation() &&
      RMWI->isVolatile() == cast<AtomicRMWInst>(I2)->isVolatile() &&
      RMWI->getOrdering() == cast<AtomicRMWInst>(I2)->getOrdering() &&
      RMWI->getSynchScope() == cast<AtomicRMWInst>(I2)->getSynchScope();
  // TODO : should it be >?
  return true;
}

// Determine whether two GEP operations perform the same underlying arithmetic.
bool LoopComparator::isEquivalentGEP(const GEPOperator *GEP1,
                                     const GEPOperator *GEP2) {
  unsigned AS = GEP1->getPointerAddressSpace();
  if (AS != GEP2->getPointerAddressSpace())
    return false;
  // remove for now
  // if (TD) {
  //   // When we have target data, we can reduce the GEP down to the value in bytes
  //   // added to the address.
  //   unsigned BitWidth = TD ? TD->getPointerSizeInBits(AS) : 1;
  //   APInt Offset1(BitWidth, 0), Offset2(BitWidth, 0);
  //   if (GEP1->accumulateConstantOffset(*TD, Offset1) &&
  //       GEP2->accumulateConstantOffset(*TD, Offset2)) {
  //     return Offset1 == Offset2;
  //   }
  // }

  if (GEP1->getPointerOperand()->getType() !=
      GEP2->getPointerOperand()->getType())
    return false;

  if (GEP1->getNumOperands() != GEP2->getNumOperands())
    return false;

  for (unsigned i = 0, e = GEP1->getNumOperands(); i != e; ++i) {
    if (!enumerate(GEP1->getOperand(i), GEP2->getOperand(i)))
      return false;
  }

  return true;
}

// Compare two values used by the two functions under pair-wise comparison. If
// this is the first time the values are seen, they're added to the mapping so
// that we will detect mismatches on next use.
bool LoopComparator::enumerate(const Value *V1, const Value *V2,
                               bool NoSelfRef/*=false*/) {
  // Check for function @f1 referring to itself and function @f2 referring to
  // itself, or referring to each other, or both referring to either of them.
  // They're all equivalent if the two functions are otherwise equivalent.
  if (!NoSelfRef)
    return true;

  if (const Constant *C1 = dyn_cast<Constant>(V1)) {
    if (V1 == V2) return true;
    const Constant *C2 = dyn_cast<Constant>(V2);
    if (!C2) return false;
    // TODO: constant expressions with GEP or references to F1 or F2.
    if (C1->isNullValue() && C2->isNullValue() &&
        isEquivalentType(C1->getType(), C2->getType()))
      return true;
    // Try bitcasting C2 to C1's type. If the bitcast is legal and returns C1
    // then they must have equal bit patterns. Aggregate types cannot be
    // bitcast.
    // above statement true according to llvm programmer's manual
    if (C1->getType()->isAggregateType() || C2->getType()->isAggregateType())
      return false;
    return C1->getType()->canLosslesslyBitCastTo(C2->getType()) &&
      C1 == ConstantExpr::getBitCast(const_cast<Constant*>(C2), C1->getType());
  }

  if (isa<InlineAsm>(V1) || isa<InlineAsm>(V2))
    return V1 == V2;

  // Check that V1 maps to V2. If we find a value that V1 maps to then we simply
  // check whether it's equal to V2. When there is no mapping then we need to
  // ensure that V2 isn't already equivalent to something else. For this
  // purpose, we track the V2 values in a set.

  ValueToValueMapTy::iterator I = id_map.find(V1);
  if (I != id_map.end()){
    DEBUG(errs() << "LOOOL " << *V1 << " " << *V2 << "\n");
    return V2 == I->second;
  }
  // FIXME: Const casts!!!
  if (!seen_values.insert(std::make_pair(V2, const_cast<Value *>(V1))).second)
    return false;
  id_map[V1] = const_cast<Value *>(V2);
  return true;
}
// sets the longest common subsequences
void LoopComparator::set_lcsss(std::deque<const Instruction*>& lcs1,  std::deque<const Instruction*>& lcs2, const BasicBlock* b1, const BasicBlock* b2){
  
  std::set<const Instruction*> lcs1_set;
  std::set<const Instruction*> lcs2_set;
  
  for(auto& x: lcs1){
    lcs1_set.insert(x);
  }
  for(auto& y: lcs2){
    lcs2_set.insert(y);
  }
  lcsL[b1] = lcs1_set;
  lcsP[b2] = lcs2_set;
  // map is l
  std::map<const Instruction*, const Instruction*> lcs2lcs;
  // map for p, reverse of l
  std::map<const Instruction*, const Instruction*> lcs2lcs_rev;
  assert(lcs1.size() == lcs2.size() && "lcss need to be same length!");
  // need to make it bidirectional!
  for(unsigned i = 0; i != lcs1.size(); ++i){
    lcs2lcs[lcs1.at(i)] = lcs2.at(i);
    lcs2lcs_rev[lcs2.at(i)] = lcs1.at(i);
  }
  // set in correspondng bb map
  bb_lcs2lcs[b1] = lcs2lcs;
  // we index by l's blocks so have b1 here
  bb_lcs2lcs_rev[b1] = lcs2lcs_rev;
}
// todo: ask about coupling between compare and equivalent Op
std::pair<double, bool> LoopComparator::compareSim(const BasicBlock *BB1, const BasicBlock *BB2,
                                  std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound) {
  // change names
  // BasicBlock* bb1 = const_cast<BasicBlock*>(BB1);
  // BasicBlock* bb2 = const_cast<BasicBlock*>(BB2);
  // bb1->setName(bb1->getName() + "_l");
  // bb2->setName(bb2->getName() + "_p");
  // setup DP matrix
  auto n = BB1->size();
  auto m = BB2->size();
  std::vector<std::vector<int>> C(m+1);
  for(size_t i = 0; i <= m; ++i){
    C.at(i) = std::vector<int>(n+1);
  }
  
  // start comparison
  int ii = 1;
  int jj = 1;
  for(auto i = BB1->begin(), iend = BB1->end(); i != iend; ++i){
    ii = 1;
    for(auto j = BB2->begin(), jend = BB2->end(); j != jend; ++j){
      //DEBUG(errs() << *i << "\n" << *j <<"\n");
      // if (not_mergeable(i,j,PHIsFound)) return 0.0;
      if (differing_insts(i,j)){
        // cant merge landing differing landing pad instructions
        // if (isa<LandingPadInst>(i))
        //   return 0.0;
        DifferingInstructions[i] = j;
        // the LCS code below
        C.at(ii).at(jj) = std::max(C.at(ii).at(jj-1),C.at(ii-1).at(jj));
      }
      else{ // instructions are the same
        C.at(ii).at(jj) = C.at(ii-1).at(jj-1) + 1;
      }
      ++ii;
    }
    ++jj;
  }
  // for(auto& row: C){
  //   for(auto& v: row){
  //     DEBUG(errs() << v << " ");
  //   }
  //   DEBUG(errs() << "\n");
  // }
  // obtain the LCS with backtrack
  // obtain diff block

  C_map[const_cast<BasicBlock*>(BB1)] = C;

  double nom = (double) 2*C.at(m).at(n);
  double denom = (double) n + m;
  double sim = nom/denom;
  //DEBUG(errs() << (double)nom/denom << "\n");
  if (sim != 1.0){
    // DEBUG(errs() << nom/denom << "\n");
    // DEBUG(errs() << "BB1: "<< *BB1 <<"\n");
    // DEBUG(errs() << "BB2: "<< *BB2 <<"\n");
    // DEBUG(errs() << "LCS1: \n");
    // for(auto i = lcs1.rbegin(); i != lcs1.rend(); ++i){
    //   DEBUG(errs() << **i << "\n");
    // }
    // DEBUG(errs() << "LCS2: \n");
    // for(auto i = lcs2.rbegin(); i != lcs2.rend(); ++i){
    //   DEBUG(errs() << **i << "\n");
    // }

  }
  // check that the LCS can be merged
  std::vector<const Instruction*> vbb1;
  std::vector<const Instruction*> vbb2;
  
  for(auto i = BB1->begin(), iend = BB1->end(); i != iend; ++i){
    vbb1.push_back(i);
  }
  for(auto j = BB2->begin(), jend = BB2->end(); j != jend; ++j){
    vbb2.push_back(j);
  }
    
  // get lcs
  std::deque<const Instruction*> lcs1;
  std::deque<const Instruction*> lcs2;
  std::deque<const Instruction*> dum;
  std::deque<unsigned> diffBlockDummy;
  
  createDiffBlock(C, vbb1, vbb2, n, m, dum, lcs1, lcs2);
  createDiffBlockDummy(C,vbb1,vbb2,n,m,diffBlockDummy); 
  
  for(int i = 0; i != lcs1.size(); ++i){
    if (not_mergeable(lcs1.at(i), lcs2.at(i), PHIsFound)) return std::make_pair(0.0,true);
  }
  // length of dum is the length of the lcs. keep adding it
  // well as any extra control flow
  merged_len += dum.size();
  for(unsigned ii = 0, ie = diffBlockDummy.size()-1; ii != ie; ++ii){
    unsigned c = diffBlockDummy.at(ii);
    unsigned c_n = diffBlockDummy.at(ii+1);
    if ((c == 0 && c_n != 0) || (c == 1 && c_n == 2) || (c == 2 && c_n == 1)){
      ++extr_cf;
      ++merged_len;
    }
  }
  return std::make_pair(sim,false);
}
void LoopComparator::prepareCFGsForMerge(Value* ctrl, BasicBlock* exit){
  // get all the information needed
  for(Loop::block_iterator bi = L1->block_begin(), be = L1->block_end(); bi != be; ++bi){
    // gather pointers to instructions in two ectors
    std::vector<const Instruction*> vbb1;
    std::vector<const Instruction*> vbb2;

    for(auto i = (*bi)->begin(), iend = (*bi)->end(); i != iend; ++i){
      vbb1.push_back(i);
    }
    for(auto j = bbmap[*bi]->begin(), jend = bbmap[*bi]->end(); j != jend; ++j){
      vbb2.push_back(j);
    }
    
    // get lcs
    std::deque<const Instruction*> lcs1;
    std::deque<const Instruction*> lcs2;
    int n = (*bi)->size();
    int m = bbmap[*bi]->size();
    std::vector<std::vector<int > > C = C_map[*bi];
    // backtrack(C, vbb1, vbb2, n, m, lcs1, lcs2);

    std::deque<const Instruction*> diffBlock;
    std::deque<unsigned> diffBlockDummy;
    std::vector<BasicBlock*> bbs;
    std::vector<unsigned> bb_anots;

    createDiffBlock(C,vbb1,vbb2,n,m,diffBlock, lcs1,lcs2);
    createDiffBlockDummy(C,vbb1,vbb2,n,m,diffBlockDummy);

    // create bb_lcs2lcs maps
    DEBUG(errs() << "lcs1\n");
    for(auto& l : lcs1){
      DEBUG(errs() << *l << "\n");
    }
    DEBUG(errs() << "lcs2\n");
    for(auto& l : lcs2){
      DEBUG(errs() << *l << "\n");
    }
    set_lcsss(lcs1,lcs2,*bi,bbmap[*bi]);

    DEBUG(errs() << "diffblock\n");
    for(auto& d: diffBlock){
      DEBUG(errs() << *d << "\n");
    }
    DEBUG(errs() << "diffdum\n");
    for(auto& d: diffBlockDummy){
      DEBUG(errs() << d << "\n");
    }
    // this will update bb_lcs2lcs map
    createCFGfromDiffBlock(const_cast<BasicBlock*>(*bi),(*bi)->getName(),diffBlock,diffBlockDummy, bbs, bb_anots);
    diffBlocks[*bi] = diffBlock;
    cfg_map[const_cast<BasicBlock*>(*bi)] = bbs;
    annots_map[const_cast<BasicBlock*>(*bi)] = bb_anots;

    
  }
  
  // create the cfg from the gathered information

  // used to help add null blocks where we see a 3
  int j = 0;

  for(Loop::block_iterator bi = L1->block_begin(), be = L1->block_end(); bi != be; ++bi){
    std::vector<unsigned> annots = annots_map[*bi];
    for(auto a: annots){
      annots_for_merge.push_back(a);
    }

    std::vector<BasicBlock*> vbbs = cfg_map[*bi];

    for(std::vector<BasicBlock*>::iterator bbi = vbbs.begin(), bbe = vbbs.end(); bbi != bbe; ++bbi){
      cfg_for_merge.push_back(*bbi);
      if (annots_for_merge.at(j+1) == 3){
        cfg_for_merge.push_back(nullptr);
        ++j;
      }
      ++j;
    }
  }
  // update all terminator instructions
  for(Loop::block_iterator bi = L1->block_begin(), be = L1->block_end(); bi != be; ++bi){
    // experimental
    (*bi)->replaceAllUsesWith(loop_to_cfg_map[*bi]);
  }
  // need to update branch out at some point - will do that in a later function
  // update initial branch? maybe at end, not sure
  unsigned i = 0;
  unsigned ie = annots_for_merge.size();
  assert(cfg_for_merge.size() == ie && "sies must agree!\n");

  // init entry block
  entry = nullptr;
  // flag for first entry
  bool fst_entry = true;
  bool exit_not_set = true;
  while (i != ie){
    errs() << i << " | " << annots_for_merge.at(i) 
           // << annots_for_merge.at(i+1) 
           // << annots_for_merge.at(i+2)
           // << annots_for_merge.at(i+3)
           << "\n";
    if (annots_for_merge.at(i) == 3){
      ++i;
      continue;
    }
    if (annots_for_merge.at(i) == 0){
      if (i+1 < ie && annots_for_merge.at(i+1) == 3){
        if (cfg_for_merge.at(i)->getTerminator()->getNumOperands() == 3){
          if (exit_not_set){
            cfg_for_merge.at(i)->getTerminator()->setOperand(1,exit);
            exit_not_set = false;
          }

          if (i+2 == ie)
            break;
          if (annots_for_merge.at(i+2) == 0){
            i += 2;
            continue;
          }

          BasicBlock* A = BasicBlock::Create(getGlobalContext(),"A");
          extra_cf_blocks.push_back(A);
          cfg_for_merge.at(i)->getTerminator()->setOperand(2,A);

          if (annots_for_merge.at(i+2) == 1 && annots_for_merge.at(i+3) == 0){
            BranchInst* br_ctrl = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2), ctrl, A);
            BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2));
            i += 3;
          }
          else if (annots_for_merge.at(i+2) == 2 && annots_for_merge.at(i+3) == 0){
            BranchInst* br_ctrl = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+3), ctrl, A);
            BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2));            
            i += 3;
          }
          else if (annots_for_merge.at(i+2) == 2 && annots_for_merge.at(i+3) == 1 && annots_for_merge.at(i+4) == 0){
            BranchInst* br_ctrl = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+3), ctrl, A);
            BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+4), cfg_for_merge.at(i+3));
            BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+4), cfg_for_merge.at(i+2));            
            i += 4;
          }
        }
        else{
          if (i+2 == ie)
            break;
          
          if (annots_for_merge.at(i+2) == 0 ){//&& annots_for_merge.at(i+3) == 3){
            i += 2;
            continue;
          }

          BranchInst* br0_old = cast<BranchInst>(cfg_for_merge.at(i)->getTerminator());
          if (annots_for_merge.at(i+2) == 1 && annots_for_merge.at(i+3) == 0){
            BranchInst* br0 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2), ctrl);
            ReplaceInstWithInst(br0_old, br0);
            BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2));
            i += 3;
          }
          else if (annots_for_merge.at(i+2) == 2 && annots_for_merge.at(i+3) == 0){
            BranchInst* br0 = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+3), ctrl);
            ReplaceInstWithInst(br0_old, br0);            
            BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2));
            i += 3;
          }
          else if (annots_for_merge.at(i+2) == 2 && annots_for_merge.at(i+3) == 1 && annots_for_merge.at(i+4) == 0){
            BranchInst* br0 = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+3), ctrl);
            ReplaceInstWithInst(br0_old, br0);     
            BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+4), cfg_for_merge.at(i+3));
            BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+4), cfg_for_merge.at(i+2));
            i += 4;
          }
        }
        // update i
        continue;
      }
      if (annots_for_merge.at(i+1) == 1 && annots_for_merge.at(i+2) == 0){
        BranchInst* br = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+1), ctrl, cfg_for_merge.at(i)); 
        BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+1)); 
        i += 2;
      }
      else if (annots_for_merge.at(i+1) == 2 && annots_for_merge.at(i+2) == 0){
        // ctrl true is 2 , false is 1
        BranchInst* br = BranchInst::Create(cfg_for_merge.at(i+1), cfg_for_merge.at(i+2), ctrl, cfg_for_merge.at(i)); 
        BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+1)); 
        i += 2;
      }
      else if (annots_for_merge.at(i+1) == 1 && annots_for_merge.at(i+2) == 2 && annots_for_merge.at(i+3) == 0){
        // swap - we rely on the same ordering for the 1 and 2 blocks
        llvm_unreachable("impossible??swap1\n");
        i += 3;
      }
      else if (annots_for_merge.at(i+1) == 2 && annots_for_merge.at(i+2) == 1 && annots_for_merge.at(i+3) == 0){
        BranchInst* br = BranchInst::Create(cfg_for_merge.at(i+1), cfg_for_merge.at(i+2), ctrl, cfg_for_merge.at(i)); 
        BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+1));
        BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+3), cfg_for_merge.at(i+2));
        
        i += 3;
      }
      else{
        DEBUG(errs() << annots_for_merge.at(i) << " " << annots_for_merge.at(i+1) << "\n");
        llvm_unreachable("why?\n");
      }
    }
    else if (annots_for_merge.at(i) == 1){
      if (annots_for_merge.at(i+1) == 0){
        BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+1), cfg_for_merge.at(i)); 

        // need to modify branch instruction of ctrl
        if (!fst_entry)
          break;
        BasicBlock* bl1_pred = cfg_for_merge.at(i)->splitBasicBlock(cfg_for_merge.at(i)->begin(),cfg_for_merge.at(i)->getName() + "_pr");
        extra_cf_blocks.push_back(cfg_for_merge.at(i));
        BranchInst* br1_1 = BranchInst::Create(cfg_for_merge.at(i+1), bl1_pred, ctrl);
        BranchInst* br_pred = cast<BranchInst>(cfg_for_merge.at(i)->getTerminator());
        ReplaceInstWithInst(br_pred, br1_1); 
        entry = cfg_for_merge.at(i);
        cfg_for_merge.at(i) = bl1_pred;
        fst_entry = false;
        cfg_kind_map[bl1_pred] = 1;

        i += 1;
      }
      else if (annots_for_merge.at(i+1) == 2 && annots_for_merge.at(i+2) == 0){
        // swap - we rely on the same ordering for the 1 and 2 blocks
        //std::swap(cfg_for_merge.begin() + i, cfg_for_merge.begin() + i + 1);
        llvm_unreachable("impossible??swap1\n");
      }
    }
    else if (annots_for_merge.at(i) == 2){
      if (annots_for_merge.at(i+1) == 0){
        BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+1), cfg_for_merge.at(i)); 
        
        // need to modify branch instruction of entry
        if (!fst_entry)
          break;
        BasicBlock* bl2_pred = cfg_for_merge.at(i)->splitBasicBlock(cfg_for_merge.at(i)->begin(), cfg_for_merge.at(i)->getName() + "_pr");
        extra_cf_blocks.push_back(cfg_for_merge.at(i));
        BranchInst* br2_2 = BranchInst::Create(bl2_pred, cfg_for_merge.at(i+1), ctrl);
        BranchInst* br_pred = cast<BranchInst>(cfg_for_merge.at(i)->getTerminator());
        ReplaceInstWithInst(br_pred, br2_2);
        entry = cfg_for_merge.at(i);
        cfg_for_merge.at(i) = bl2_pred;
        fst_entry = false;
        cfg_kind_map[bl2_pred] = 2;

        i += 1;
      }
      else if (annots_for_merge.at(i+1) == 1 && annots_for_merge.at(i+2) == 0){
        BranchInst* br1 = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i)); 
        BranchInst* br2 = BranchInst::Create(cfg_for_merge.at(i+2), cfg_for_merge.at(i+1)); 

        // need to modify branch instruction of entry
        if (!fst_entry)
          break;
        BasicBlock* bl2_pred = cfg_for_merge.at(i)->splitBasicBlock(cfg_for_merge.at(i)->begin(), cfg_for_merge.at(i)->getName() + "_pr");
        extra_cf_blocks.push_back(cfg_for_merge.at(i));
        BranchInst* br2_2 = BranchInst::Create(bl2_pred, cfg_for_merge.at(i+1), ctrl);
        BranchInst* br_pred = cast<BranchInst>(cfg_for_merge.at(i)->getTerminator());
        ReplaceInstWithInst(br_pred, br2_2);
        entry = cfg_for_merge.at(i);
        cfg_for_merge.at(i) = bl2_pred;
        fst_entry = false;
        cfg_kind_map[bl2_pred] = 2;

        i += 2;
      }
    }
  }
  // setup entry block
  if (!entry)
    entry = cfg_for_merge.at(0);
  // create the bb_lcs and extras
  createBBlcsAndExtras();
}
void LoopComparator::backtrack(
                               const std::vector<std::vector<int>>& C, 
                               std::vector<const Instruction*> BB1, 
                               std::vector<const Instruction*> BB2, 
                               int i, 
                               int j,
                               std::vector<const Instruction*>& result1,
                               std::vector<const Instruction*>& result2)
{
  if (i == 0 || j == 0){
    return ;
  }
  else if(!differing_insts(BB1.at(i-1), BB2.at(j-1))){
    result1.push_back(BB1.at(i-1));
    result2.push_back(BB2.at(j-1));
    return backtrack(C, BB1, BB2, i-1, j-1, result1, result2);
  }
  else{
    if (C.at(j-1).at(i) > C.at(j).at(i-1)){
      return backtrack(C, BB1, BB2, i, j-1, result1, result2);
    }
    else{
      return backtrack(C, BB1, BB2, i-1, j, result1, result2);
    }
  }
}
// create diff block
void LoopComparator::createDiffBlock(
                               const std::vector<std::vector<int>>& C, 
                               std::vector<const Instruction*> BB1, 
                               std::vector<const Instruction*> BB2, 
                               int i, 
                               int j,
                               std::deque<const Instruction*>& diffBlock,
                               std::deque<const Instruction*>& lcs1,
                               std::deque<const Instruction*>& lcs2)
{
  if (i > 0 && j > 0 && !differing_insts(BB1.at(i-1), BB2.at(j-1))){
    lcs1.push_front(BB1.at(i-1));
    lcs2.push_front(BB2.at(j-1));
    diffBlock.push_front(BB1.at(i-1));
    createDiffBlock(C,BB1,BB2,i-1,j-1,diffBlock,lcs1,lcs2);
  }
  else if(j > 0 && ( i == 0 || C.at(j-1).at(i) >= C.at(j).at(i-1))){
    diffBlock.push_front(BB2.at(j-1));
    return createDiffBlock(C, BB1, BB2, i, j-1, diffBlock,lcs1,lcs2);
  }
  else if(i > 0 && ( j == 0 || C.at(j-1).at(i) < C.at(j).at(i-1))){
    diffBlock.push_front(BB1.at(i-1));
    return createDiffBlock(C, BB1, BB2, i-1, j, diffBlock,lcs1,lcs2);
  }
  else
    return;
}
void LoopComparator::createCFGfromDiffBlock(
                                            BasicBlock* BB1,
                                            std::string parName,
                                            std::deque<const Instruction*>& diffBlock,
                                            std::deque<unsigned>& annotations,
                                            std::vector<BasicBlock*>& cfg,
                                            std::vector<unsigned>& bb_annotations)
{
  assert(diffBlock.size() == annotations.size() && diffBlock.size() != 0 && "bbs must have at least 1 instruction and annotations must be equal..");
  int name = 0;
  int i = 1;
  int e = diffBlock.size();

  BasicBlock* bb = BasicBlock::Create(getGlobalContext(),parName + "." + std::to_string(name++) + "." + std::to_string(annotations.at(0)));

  loop_to_cfg_map[BB1] = bb;

  Instruction* inst = const_cast<Instruction*>(diffBlock.at(0));
  inst->removeFromParent();
  bb->getInstList().push_back(inst);

  cfg.push_back(bb);
  bb_annotations.push_back(annotations.at(0));

  cfg_kind_map[bb] = annotations.at(0);
  
  bb_lcs2lcs[bb] = bb_lcs2lcs[BB1];
  bb_lcs2lcs_rev[bb] = bb_lcs2lcs_rev[BB1];
  while(i != e){
    Instruction* inst = const_cast<Instruction*>(diffBlock.at(i));
    inst->removeFromParent();

    if (annotations.at(i) == annotations.at(i-1)){
      bb->getInstList().push_back(inst);
    }
    else{
      bb = BasicBlock::Create(getGlobalContext(),parName + "." + std::to_string(name++) + "." + std::to_string(annotations.at(i)));
      bb->getInstList().push_back(inst);
      cfg.push_back(bb);

      bb_annotations.push_back(annotations.at(i));
      cfg_kind_map[bb] = annotations.at(i);

      bb_lcs2lcs[bb] = bb_lcs2lcs[BB1];
      bb_lcs2lcs_rev[bb] = bb_lcs2lcs_rev[BB1];      
    }
    ++i;
  }
  // push boundary between basic blocks
  bb_annotations.push_back(3);
}

void LoopComparator::createDiffBlockDummy(
                               const std::vector<std::vector<int>>& C, 
                               std::vector<const Instruction*> BB1, 
                               std::vector<const Instruction*> BB2, 
                               int i, 
                               int j,
                               std::deque<unsigned>& diffBlock)
{
  if (i > 0 && j > 0 && !differing_insts(BB1.at(i-1), BB2.at(j-1))){
    diffBlock.push_front(0);
    createDiffBlockDummy(C,BB1,BB2,i-1,j-1,diffBlock);
  }
  else if(j > 0 && ( i == 0 || C.at(j-1).at(i) >= C.at(j).at(i-1))){
    diffBlock.push_front(1);
    return createDiffBlockDummy(C, BB1, BB2, i, j-1, diffBlock);
  }
  else if(i > 0 && ( j == 0 || C.at(j-1).at(i) < C.at(j).at(i-1))){
    diffBlock.push_front(2);
    return createDiffBlockDummy(C, BB1, BB2, i-1, j, diffBlock);
  }
  else
    return;
}

void LoopComparator::createBBlcsAndExtras(){
  
  for(unsigned i = 0, ie = annots_for_merge.size(); i != ie; ++i){
    switch(annots_for_merge.at(i)){
    case 0:
      bb_lcs.push_back(cfg_for_merge.at(i));
      break;
    case 1:
      extra_ps.push_back(cfg_for_merge.at(i));
      extras.push_back(cfg_for_merge.at(i));
      break;
    case 2:
      extra_ls.push_back(cfg_for_merge.at(i));
      extras.push_back(cfg_for_merge.at(i));
      break;
    case 3:
      continue;
    }
  }
}
// return false if instructions not mergeable
// populate phi nodes everytime it gets called
bool LoopComparator::not_mergeable(const Instruction *I1, const Instruction *I2, std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound){
  if (I1->use_empty() != I2->use_empty()){
    // can't merge instructions such that one has an empty use list
    // and the other doesnt
    // http://llvm.org/docs/doxygen/html/classllvm_1_1Value.html
    DEBUG(errs() <<"use_empty: \n");
    return true;
  }
  if (!isEquivalentType(I1->getType(), I2->getType())){
    // can't merge instructions with different types
    DEBUG(errs() <<"not eq type\n");
    return true;
  }
  if (isa<InvokeInst>(I1) && !I1->use_empty() && I1->getType() != I2->getType()){
    //dunno why
    DEBUG(errs() << "is a invoke inst\n");
    return true;
  }
  if (const PHINode *Phi1 = dyn_cast<PHINode>(I1)) {
    const PHINode *Phi2 = dyn_cast<PHINode>(I2);
    // We can't currently merge a PHI and non-PHI instruction
    if (!Phi2){
      DEBUG(errs() << "cant' merge phi with non phi\n");
      return true;
    }
    
    // We can't currently merge PHI nodes with different numbers of incoming
    // values
    if (I1->getNumOperands() != I2->getNumOperands()){
      DEBUG(errs() << "can't merge phi nodes wit diff ops\n");
      return true;
    }
    // TODO - somehow look at this
    // We need to treat PHI nodes specially. Their incoming values may be in a
    // different order even if they are equivalent. We can't compare them
    // until we've seen the incoming blocks and know which values are
    // equivalent. Therefore postpone PHINode comparison until the end.
    PHIsFound->push_back(std::make_pair(Phi1, Phi2));
  } 
  //DEBUG(errs() << "actually mergeable!\n");
  return false;
}
bool LoopComparator::differing_insts(const Instruction *I1, const Instruction *I2){
  // if(isa<PHINode>(I1) && isa<PHINode>(I2))
  //   return true;

  if (PointerType* i1t = dyn_cast<PointerType>(I1->getType())){
    PointerType* i2t = dyn_cast<PointerType>(I2->getType());
    if(i2t)
      if (i1t != i2t || i1t->getAddressSpace() != i2t->getAddressSpace())
        return true;
  }
  //DEBUG(errs() << "differing\n");
  if (!enumerate(I1, I2))
    return true;
  
  if (const GetElementPtrInst *GEP1 = dyn_cast<GetElementPtrInst>(I1)) {
    const GetElementPtrInst *GEP2 = dyn_cast<GetElementPtrInst>(I2);
    if (!GEP2)
      return true;
    
    if (!enumerate(GEP1->getPointerOperand(), GEP2->getPointerOperand()))
      return true;
    
    if (!isEquivalentGEP(GEP1, GEP2)){
      return true;
    }
  }
  else {
    if (!isEquivalentOperation(I1, I2))
      return true;
    
    assert(I1->getNumOperands() == I2->getNumOperands());
    for (unsigned i = 0, e = I1->getNumOperands(); i != e; ++i) {
      Value *OpF1 = I1->getOperand(i);
      Value *OpF2 = I2->getOperand(i);
      // if constants, check that they are equal
      if (isa<Constant>(OpF1) && !isa<Constant>(OpF2))
        return true;
      if (!isa<Constant>(OpF1) && isa<Constant>(OpF2))
        return true;
      if (isa<Constant>(OpF1) && isa<Constant>(OpF2))
        if(OpF1 != OpF2)
          return true;
      if (!enumerate(OpF1, OpF2))
        return true;
      // TODO: why?
      // too late to ask tobias
      // this definitely checks the types
      // this checks if all operands are of the same type
      // 
      if (OpF1->getValueID() != OpF2->getValueID() ||
          !isEquivalentType(OpF1->getType(), OpF2->getType()))
        return true;
      // TODO: implement samo map that maps the operands of
      // non-differing instructions
    }
  }
  return false;
}


/// Test whether two basic blocks have equivalent behaviour. Returns true if the
/// blocks can be merged, false if they cannot. Differing instructions are
/// recorded in DifferingInstructions.
bool LoopComparator::compare(const BasicBlock *BB1, const BasicBlock *BB2,
                             std::list<std::pair<const PHINode*,const PHINode*> > *PHIsFound) {
  return false;
}

bool LoopComparator::comparePHIs(
                                 const std::list<std::pair<const PHINode*,const PHINode*> > &PHIs) {
  if (PHIs.empty())
    return true;

  for (auto I = PHIs.begin(), E = PHIs.end(); I != E; ++I) {
    const PHINode *Phi1 = I->first, *Phi2 = I->second;
    for (unsigned ValId = 0, ValNum = Phi1->getNumIncomingValues(); ValId != ValNum; ++ValId) {
      Value *Phi1Val = Phi1->getIncomingValue(ValId);

      // Get corresponding Phi2Val
      Value *BBinPhi2Val = getL1toL2Map()[Phi1->getIncomingBlock(ValId)];

      if (!BBinPhi2Val)
        return false; // Currently can't handle differing predecessor blocks

      BasicBlock *BBinPhi2 = cast<BasicBlock>(BBinPhi2Val);
      Value *Phi2Val = Phi2->getIncomingValueForBlock(BBinPhi2);

      // Enumerate the values. If the PHI node references the function itself (a
      // very rare case), we mark it as different (NoSelfRef). This is only
      // necessary for outline merging, not equiv merging. TODO: Make equal
      // merging possible with such PHI nodes.
      if (!enumerate(Phi1Val, Phi2Val,/*NoSelfRef=*/true)) {
        DifferingInstructions[Phi1] = Phi2;
        break;
      }
    }
  }

  return true;
}

// Test whether the two loops have equivalent behaviour.
bool LoopComparator::compare(Module& M) {
  // We need to recheck everything, but check the things that weren't included
  // in the hash first.

  // We do a CFG-ordered walk since the actual ordering of the blocks in the
  // linked list is immaterial. Our walk starts at the entry block for both
  // functions, then takes each block from each terminator in order. As an
  // artifact, this also means that unreachable blocks are ignored.
  // NOTE : this is the structured CFB BB walk
  double total_sim = 0.0;
  int numBB = L1->getNumBlocks();  
  if (numBB > getMaxNumBlocks() || numBB < MinNumBBs)
    return false;

  std::vector<double> sims(numBB);
  
  double N_inst = 0.0;
  double M_inst = 0.0;

  {
    SmallVector< BasicBlock *, 8> L1BBs, L2BBs;
    SmallSet< BasicBlock *, 128> VisitedBBs; // in terms of L1.
    std::list<std::pair<const PHINode*,const PHINode*> > PHIsFound;
    // update 
    L1BBs.push_back(L1->getHeader());
    L2BBs.push_back(L2->getHeader());
    
    DEBUG(errs() << L1->getHeader()->getParent()->getName().data() << "\n");
    if (std::strcmp(L1->getHeader()->getParent()->getName().data(), "get_f") == 0)
      int x = 42;
    // we need to keep a counter with the number of
    // basic blocks we've processed
    int count = 0;

    // DEBUG(errs() <<  L1->getNumBlocks() << " " <<  L2->getNumBlocks() << "\n");
    VisitedBBs.insert(L1BBs[0]);
    VisitedBBs.insert(L2BBs[0]);
    // similarity vector

    while (count != numBB && !L1BBs.empty() && !L2BBs.empty()) {
      BasicBlock *L1BB = L1BBs.pop_back_val();
      BasicBlock *L2BB = L2BBs.pop_back_val();
      // populate BB map
      bbmap[L1BB] = L2BB;
      
      // DEBUG(errs() << *L1BB << "\n");
      // DEBUG(errs() << *L2BB << "\n");
      
      // Check for control flow divergence
      if (!enumerate(L1BB, L2BB))
        goto not_mergeable;

      const TerminatorInst *L1TI = L1BB->getTerminator();
      const TerminatorInst *L2TI = L2BB->getTerminator();

      // TODO: Implement merging of blocks with different numbers of instructions.
      // todo : ask tobias
      // number of successors must be same, but size not necessarily
      if (!isa<ReturnInst>(*L1TI) && !isa<ReturnInst>(*L2TI) && L1TI->getNumSuccessors() != L2TI->getNumSuccessors()){ 
        DEBUG(errs() << "ret: " << !isa<ReturnInst>(*L1TI) << " " <<  !isa<ReturnInst>(*L2TI) << "\n");
        DEBUG(errs() << "sucs: " << L1TI->getNumSuccessors() << " " << L2TI->getNumSuccessors()  << "\n");
        goto not_mergeable;
      }
      // The actual instruction-by-instruction comparison
      // using LCS
      std::pair<double,bool> res_p = compareSim(L1BB, L2BB, &PHIsFound);
      double sim = res_p.first;
      bool not_merg = res_p.second;

      // if at least 1 block is less than 80%
      // NOTE: threshold is 0.7
      //DEBUG(errs() << "sim: " << sim << "\n");
      DEBUG(errs() << "sim: " << sim << "\n");
      if (not_merg){ DEBUG(errs() << "sim reported not mergeable\n");
        goto not_mergeable;
      }

      total_sim += sim;
      sims.at(count) = sim*(L1BB->size() + L2BB->size());
      N_inst += L1BB->size();
      M_inst += L2BB->size();
      // FIXME: Count this in compare(L1BB,L2BB) so it doesn't include debug
      // instructions.
      InstructionCount += std::max(L1BB->size(), L2BB->size());

      // NOTE: doesn't ne
      // if (!isa<ReturnInst>(*L1TI) && !isa<ReturnInst>(*L2TI)){
      //   assert(L1TI->getNumSuccessors() == L2TI->getNumSuccessors());
      //   for (unsigned i = 0, e = L1TI->getNumSuccessors(); i != e; ++i) {
      //     if (!L1->contains(L1TI->getSuccessor(i)) || !L2->contains(L2TI->getSuccessor(i)))
      //       continue;
      //     if (!VisitedBBs.insert(L1TI->getSuccessor(i)))
      //       continue;
          
      //     L1BBs.push_back(L1TI->getSuccessor(i));
      //     L2BBs.push_back(L2TI->getSuccessor(i));
      //   }
      // }

      if (!isa<ReturnInst>(*L1TI)){
        for (unsigned i = 0, e = L1TI->getNumSuccessors(); i != e; ++i) {
          if (!L1->contains(L1TI->getSuccessor(i)))
            continue;
          if (!VisitedBBs.insert(L1TI->getSuccessor(i)))
            continue;          
          L1BBs.push_back(L1TI->getSuccessor(i));
        }
      }
      if (!isa<ReturnInst>(*L2TI)){
        for (unsigned i = 0, e = L2TI->getNumSuccessors(); i != e; ++i) {
          if (!L2->contains(L2TI->getSuccessor(i)))
            continue;
          if (!VisitedBBs.insert(L2TI->getSuccessor(i)))
            continue;
          L2BBs.push_back(L2TI->getSuccessor(i));
        }
      }

      ++count;
    }

    BasicBlockCount = VisitedBBs.size();

    // After we've seen all values and BBs, compare the PHI nodes
    if (!comparePHIs(PHIsFound)){ 
      DEBUG(errs() << "phis dont compare\n");
      goto not_mergeable;
    }
  }
  
  if (DifferingInstructions.size()) {
    // Currently we can't merge vararg functions with differing instructions.
    // TODO: Explore whether this is feasible; the difficult bit is the
    // additional argument we need to add.

    isDifferent = true;
    DifferingInstructionsCount += DifferingInstructions.size();
    // debug only output TODO

  }
  DEBUG(errs() << "avg sim: " << total_sim / (1.0 * L1->getNumBlocks()) << "\n");
  DEBUG(errs() << "weighted avg sim: " << std::accumulate(sims.begin(), sims.end(), 0) /(N_inst + M_inst) << "\n");
  comb_insts = N_inst + M_inst; // TODO - check if true??
  avg_sim = total_sim / (1.0 * L1->getNumBlocks());
  wavg_sim = std::accumulate(sims.begin(), sims.end(), 0) /(N_inst + M_inst);
  if (getSim() < getThresh() || N_inst < getInstThresh() ||  M_inst < getInstThresh() || N_inst > MaxInstructions || M_inst > MaxInstructions)
    goto not_mergeable;
  if (analysis){
    can_be_merged = true;
  }
  DEBUG(errs() << "total sim: " << total_sim << "\n");
  DEBUG(errs() << "Can be merged: \n");
  DEBUG(errs() << "L1:\n");
  for(auto i = L1->block_begin(); i != L1->block_end(); ++i){
    DEBUG(errs() << **i <<"\n");
  }
  DEBUG(errs() << "\nL2:\n");
  for(auto i = L2->block_begin(); i != L2->block_end(); ++i){
    DEBUG(errs() << **i <<"\n");
  }
  
  // output instruction mixes
  {
    // std::map<std::string, unsigned> l1_mix;
    // std::map<std::string, unsigned> l2_mix;
    
    // for(auto bi = L1->block_begin(), be = L1->block_end(); bi != be; ++bi){
    //   for(auto ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
    //     ++l1_mix[ii->getOpcodeName()];
    //   }
    // }

    // for(auto bi = L2->block_begin(), be = L2->block_end(); bi != be; ++bi){
    //   for(auto ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
    //     ++l2_mix[ii->getOpcodeName()];
    //   }
    // }
    // errs() << "mix 1: \n";
    // for(auto p: l1_mix){
    //   errs() << p.first << " -> " << p.second << "\n";
    // }
    // errs() << "mix 2: \n";
    // for(auto p: l2_mix){
    //   errs() << p.first << " -> " << p.second << "\n";
    // }

  }
  errs() << "merged len: " <<merged_len<<"\n";
  errs() << "extra cf: " << extr_cf << "\n";
  errs() << "n and m inst: " << N_inst << " " << M_inst <<"\n";
  errs() << "n and m cfg size: " << 2*L1->getNumBlocks() << "\n";
  if (true && (merged_len > N_inst + M_inst || extr_cf > L1->getNumBlocks()))
    goto not_mergeable;
  return true;
 not_mergeable:
  // Fail: cannot merge the two functions
  DEBUG(errs() << "total sim: " << total_sim << "\n");
  isNotMergeable = true;
  avg_sim = 0.0;
  wavg_sim = 0.0;
  return false;
}

bool LoopComparator::isExactMatch() {
  return (!isNotMergeable && !isDifferent);
}

bool LoopComparator::isMergeCandidate() {
  if (isNotMergeable)
    return false;

  if (!isDifferent)
    return true;

  // Heuristic when to attempt merging
  if (InstructionCount > MergeDifferingMinInsts &&
      DifferingInstructionsCount <= MergeMaxDiffering &&
      getSimilarityMetric() > MergeMinSimilarity*100) {
    return true;
  } else {
    DEBUG(dbgs() << "Candidate excluded by parameters.\n");
  }

  return false;
}

// end of LoopComparator
namespace {

  struct FunctionComparatorOrdering {
    bool operator () (FunctionComparator *LHS, FunctionComparator *RHS) {
      unsigned MetricLHS = LHS->getSimilarityMetric(),
        MetricRHS = RHS->getSimilarityMetric();

      // Fall back to pointer comparison with identical metrics.
      if (MetricLHS == MetricRHS)
        return LHS > RHS;
      return MetricLHS > MetricRHS;
    }
  };

  class MergeRegistry {
  public:
    typedef MapVector<unsigned, std::list<ComparableFunction> > FnCompareMap;
    typedef std::set<FunctionComparator *, FunctionComparatorOrdering>
    FnComparatorSet;
    typedef std::map<Function *, FnComparatorSet> SimilarFnMap;

    ~MergeRegistry();

    /// Defer a function for consideration in the next round.
    void defer(Function *F);

    /// Return true if we have deferred functions that can be enqueued.
    bool haveDeferred() { return !Deferred.empty(); }

    /// Move all the deferred functions into buckets to consider them for merging.
    /// Returns number of functions that have been added.
    unsigned enqueue();

    /// Add a candidate for merging
    void insertCandidate(FunctionComparator *Comp);

    /// Remove a Function from the FnSet and queue it up for a second sweep of
    /// analysis if Reanalyze is set. If it is a candidate for merging, remove it
    /// from consideration.
    void remove(Function *F, bool Reanalyze=true);

    /// Return the similarity metric of the most similar function to F that is
    /// not listed in the Ignore set.
    unsigned getMaxSimilarity(Function *F, const DenseSet<Function *> &Ignore);

    /// The collection of buckets that contain functions that may be similar to
    /// each other (same hash value).
    FnCompareMap FunctionsToCompare;

    std::list<FunctionComparator *> FunctionsToMerge;
    SimilarFnMap SimilarFunctions;

  private:
    typedef std::vector<WeakVH> FnDeferredQueue;

    /// A work queue of functions that may have been modified and should be
    /// analyzed again.
    FnDeferredQueue Deferred;
  };

}  // end anonymous namespace

MergeRegistry::~MergeRegistry() {
  for (std::list<FunctionComparator *>::iterator
         I = FunctionsToMerge.begin(), E = FunctionsToMerge.end();
       I != E; ++I)
    delete *I;
}

static bool isComparisonCandidate(Function *F) {
  // Ignore declarations and tiny functions - no point in merging those
  return (!F->isDeclaration() && !F->hasAvailableExternallyLinkage() &&
          !(F->size() == 1 && F->begin()->size() < MergeMinInsts) &&
          !F->getName().endswith(MERGED_SUFFIX));
}

void MergeRegistry::defer(Function *F) {
  if (isComparisonCandidate(F))
    Deferred.push_back(F);
}

// Move functions from Deferred into buckets. remove() may have been called
// multiple times for the same function, so eliminate duplicates using the
// set. We reverse them because MergeFunctions::insert inserts at the front
// of each bucket.
unsigned MergeRegistry::enqueue() {
  DenseSet<Function *> InsertedFuncs;

  for (std::vector<WeakVH>::reverse_iterator
         DefI = Deferred.rbegin(), DefE = Deferred.rend();
       DefI != DefE; ++DefI) {
    if (!*DefI) continue;
    Function *F = cast<Function>(*DefI);
    if (InsertedFuncs.find(F) != InsertedFuncs.end()) continue;
    if (!isComparisonCandidate(F)) continue;

    unsigned Hash = profileFunction(F);
    FunctionsToCompare[Hash].push_front(F);

    InsertedFuncs.insert(F);
  }

  Deferred.clear();

  return InsertedFuncs.size();
}

static void dumpSimilar(MergeRegistry::SimilarFnMap &Map, raw_ostream &O) {
  O << "**** Similar function set dump:\n";

  for (MergeRegistry::SimilarFnMap::iterator I = Map.begin(), E = Map.end();
       I != E; ++I) {
    Function *F = I->first;
    MergeRegistry::FnComparatorSet &S = I->second;

    O << " - " << F->getName() << ":\n";
    for (MergeRegistry::FnComparatorSet::iterator II = S.begin(), EE = S.end();
         II != EE; ++II) {
      FunctionComparator *C = *II;
      O << "   " << C->getF1()->getName() << " and "
        << C->getF2()->getName() << " metric " << C->getSimilarityMetric()
        << "\n";
    }
  }
}

void MergeRegistry::insertCandidate(FunctionComparator *Comp) {
  FunctionsToMerge.push_back(Comp);
  SimilarFunctions[Comp->getF1()].insert(Comp);
}

static void removeFromBucket(Function *F,
                             std::list<ComparableFunction> &Bucket) {
  for (std::list<ComparableFunction>::iterator
         I = Bucket.begin(), E = Bucket.end(); I != E; ++I) {
    if (I->getFunc() == F) {
      Bucket.erase(I);
      return;
    }
  }
}

void MergeRegistry::remove(Function *F, bool Reanalyze/*=true*/) {

  // There is no need to remove a function that is  not already
  // in a bucket.
  if (!isComparisonCandidate(F))
    return;

  unsigned Hash = profileFunction(F);
  std::list<ComparableFunction> &Bucket = FunctionsToCompare[Hash];

  removeFromBucket(F, Bucket);

  if (Reanalyze)
    defer(F);

  // Check whether we have any existing FunctionComparator objects for this fn.
  // If yes, discard them because F has changed. Retry merging for those
  // functions by adding them to Deferred.
  std::list<FunctionComparator *>::iterator I = FunctionsToMerge.begin();
  while (I != FunctionsToMerge.end()) {
    FunctionComparator *Comp = *I;
    if (Comp->getF1() == F) {
      Function *OtherF = Comp->getF2();
      defer(OtherF);
      removeFromBucket(OtherF, Bucket);
      if (!SimilarFunctions[F].erase(Comp))
        llvm_unreachable("Inconsistent SimilarFunctions set");
      I = FunctionsToMerge.erase(I);
      delete Comp;
    } else if (Comp->getF2() == F) {
      Function *OtherF = Comp->getF1();
      defer(OtherF);
      removeFromBucket(OtherF, Bucket);
      if (!SimilarFunctions[OtherF].erase(Comp))
        llvm_unreachable("Inconsistent SimilarFunctions set");
      I = FunctionsToMerge.erase(I);
      delete Comp;
    } else {
      ++I;
    }
  }
}

unsigned MergeRegistry::getMaxSimilarity(Function *F,
                                         const DenseSet<Function *> &Ignore) {
  FnComparatorSet &Similar = SimilarFunctions[F];

  for (FnComparatorSet::iterator I = Similar.begin(), E = Similar.end();
       I != E; ++I) {
    FunctionComparator *Comp = *I;
    if (Ignore.count(Comp->getF2()))
      continue;

    return Comp->getSimilarityMetric();
  }

  return 0;
}

namespace {

  /// MergeFunctions finds functions which will generate identical machine code,
  /// by considering all pointer types to be equivalent. Once identified,
  /// MergeFunctions will fold them by replacing a call to one to a call to a
  /// bitcast of the other.
  /// Main pass
  class MergeFunctions : public ModulePass {
  public:
    static char ID;
    MergeFunctions()
      : ModulePass(ID), HasGlobalAliases(false) {
      //initializeMergeFunctionsPass(*PassRegistry::getPassRegistry());
    }

    bool runOnModule(Module &M);
    void getAnalysisUsage(AnalysisUsage &AU) const {
      AU.addRequired<LoopInfo>();
      AU.addRequired<DominatorTreeWrapperPass>();
      // I dont think we need this - we really dont...
      //AU.addRequiredID(BreakCriticalEdgesID);
      //AU.addRequiredID(LoopSimplifyID);
      AU.addRequired<DominanceFrontier>();
    }

    struct type_compare {
      bool operator() (const Value* lhs, const Value* rhs) const{
        return lhs->getType() < rhs->getType();
      }
    };
  private:
    class LoopPtr{
    public:
      LoopPtr(Function* F, int pos, MergeFunctions* mf): F(F), pos(pos), mf(mf) {}
      Loop* getLoop(){
        LoopInfo& LI = mf->getAnalysis<LoopInfo>(*F);
        DEBUG(errs() << *(LI.begin()) + pos << "\n");
        return *(LI.begin()) + pos;
      };
      Function* getFunction(){
        return F;
      };
    private:
      Function* F;
      int pos;
      MergeFunctions* mf;
    };
    // this static function makes sure that the loop p's BBs are removed
    // and all dangling instruction refereces are cleared up.
    // only p's header block remains and an unconditional jump 
    // to the next successor is returned. if there is more than one, we can't merge
    // so we return false, otherwise always returns true
    static bool replaceLoopWithEmptyBB(Loop* p);
    bool prepareValuesForCallInst(const std::vector<Value*>&, const std::vector<Value*>&, std::vector<Value*>&);
    static bool compareLoopComparators(LoopComparator* l1, LoopComparator* l2);
  private:
    typedef std::set<FunctionPtr> FnTreeType;
    /// Remove a Function from the FnTree and queue it up for a second sweep of
    /// analysis.
    void remove(Function *F);

    /// A work queue of functions that may have been modified and should be
    /// analyzed again.
    std::vector<WeakVH> Deferred;

    /// Find the functions that use this Value and remove them from FnSet and
    /// queue the functions.
    void removeUsers(Value *V);

    /// Replace all direct calls of Old with calls of New. Will bitcast New if
    /// necessary to make types match.
    void replaceDirectCallers(Function *Old, Function *New);

    /// Process functions in the specified bucket, by either doing equiv merging
    /// marking them for diff merging. Returns false if the bucket needs to be
    /// re-scanned after an equiv merge. Sets Changed if the module was changed by
    /// equiv merge.
    bool mergeBucket(std::list<ComparableFunction> &Fns, bool &Changed);

    /// Exhaustively compare all functions in each bucket and do equiv merging
    /// where possible. Functions that have already been compared will not be
    /// compared again. Returns true if the module was modified.
    bool doExhaustiveCompareMerge();

    /// Merge all the functions marked for diff merging. Returns true if the
    /// module was modified.
    bool doDiffMerge();

    /// Merge two equivalent functions. Upon completion, G may be deleted, or may
    /// be converted into a thunk. In either case, it should never be visited
    /// again.
    void mergeTwoFunctions(Function *F, Function *G);

    /// Merge a set of functions with differences.
    void outlineAndMergeFunctions(SmallVectorImpl<FunctionComparator *> &Fns);

    /// Replace G with a thunk or an alias to F. Deletes G.
    void writeThunkOrAlias(Function *F, Function *G);

    /// Replace G with a simple tail call to bitcast(F). Also replace direct uses
    /// of G with bitcast(F). Deletes G.
    void writeThunk(Function *F, Function *G);

    /// Replace G with a tail call to F with an additional argument.
    ///
    void writeThunkWithChoice(Function *NewF, Function *OldF, int Choice);

    /// Replace G with an alias to F. Deletes G.
    void writeAlias(Function *F, Function *G);
  
    /// The set of all distinct functions. Use the insert() and remove() methods
    /// to modify it.
    FnTreeType FnTree;

    /// DataLayout for more accurate GEP comparisons. May be NULL.
    const DataLayout *TD;

    /// Merge registry. Stores all the information about functions being
    /// considered for merging as well as current candidates for merging.
    MergeRegistry Registry;

    /// Whether or not the target supports global aliases.
    bool HasGlobalAliases;
  };

}  // end anonymous namespace

char MergeFunctions::ID = 0;
static RegisterPass<MergeFunctions> X("simmergefunc", "Similarity Merge functions", false, false);

// INITIALIZE_PASS_BEGIN(MergeFunctions, "simmergefunc", "Similarty Merge Loops", false,
//                       false)
// INITIALIZE_PASS_END(MergeFunctions, "simmergefunc", "Simplify the CFG", false,
//                     false)
 


bool MergeFunctions::runOnModule(Module &M) {
  //bool Changed = false;
  DEBUG(errs() << "miti e pi4ozen\n");
  unsigned nf = M.size();
  DataLayoutPass *DLP = getAnalysisIfAvailable<DataLayoutPass>();
  std::string str("");
  std::string str_cfg("");
  TD = DLP ? &DLP->getDataLayout() : nullptr;
  // gather pointers to all loops
  // probably need to store pointer to containing function
  std::vector<Loop*> loops;
  for (Module::iterator F = M.begin(), FEND = M.end(); F != FEND; ++F){
    if (!(*F).isDeclaration()){
      LoopInfo& LI = getAnalysis<LoopInfo>(*F);
      // if we don't do this check, we end up with a lot more
      // items, dunno why. Implementation of container needs to be
      // changed to make the problem O(n log(n))
      // or just insert, then do std::sort and std::unique
      for(LoopInfo::iterator l = LI.begin(), lend = LI.end(); l != lend; ++l){
        if ((*l)->getLoopDepth() != 1)
          continue;
        if(std::find(loops.begin(), loops.end(), *l) == loops.end())
          loops.push_back(*l);
      }
    }
  }
  // do a simple map with each bucket being number of basic blocks
  errs () << "Num Loops: "<< loops.size() << "\n";
  unsigned total_loop_insts = 0;
  std::map<unsigned long, std::vector<Loop*>> lmap;
  for(auto l: loops){
    if (lmap.find(l->getNumBlocks()) != lmap.end()){
      lmap[l->getNumBlocks()].push_back(l);
    }
    else{
      lmap[l->getNumBlocks()] = std::vector<Loop*>{l};
    }
    
  }
  
  for(auto bucket: lmap){
    //errs() <<"# " << bucket.first << "->" << bucket.second.size() << "\n";
    if (bucket.second.size() > 1){
      for (auto l : bucket.second){
        total_loop_insts += loop_insts(l);
      }
    }
  }
  // get dom tree used for code extraction
  int num_merged_pairs = 0;
  unsigned num_merged_analysis_pairs = 0;
  unsigned total_phis = 0;
  int total_improv = 0;
  int total_cfg_diff = 0;
  
  unsigned mod_insts = module_insts(M);
  unsigned mod_cfg = module_cfg(M);
  // once a pair is merged, the loops become invalid to merge
  // 

  // do actual bucket loop comparison
  for(auto& bucket: lmap){
    DenseSet<Loop*> processed;
    std::deque<LoopComparator*> compared_loops;
    for(auto l = bucket.second.begin(); l != bucket.second.end(); ++l){
      auto p_it = l+1;
      // if this loop has been processed, move on
      if(processed.find(*l) != processed.end())
        break;
      for(auto& p = p_it; p != bucket.second.end(); ++p){
        // if this loop has been processed, move on
        if(processed.find(*p) != processed.end() || processed.find(*l) != processed.end())
          break;
        // we can only deal with top level loops
        if( (*l)->getLoopDepth() >0 && (*p)->getLoopDepth() > 0){
          // cloning instructions will invalidate this before we run 
          // the updateCFG
          std::vector<BasicBlock*> lBBs = (*l)->getBlocks();
          std::vector<BasicBlock*> pBBs = (*p)->getBlocks();
          // get l and p's headers
          BasicBlock* lh = (*l)->getHeader();
          BasicBlock* ph = (*p)->getHeader();
          // get l and p's headers's single predecessors
          // could be preheader - dunno what's the difference
          // precessor is less strict according to the API
          // check diff between preheader and predecessor
          BasicBlock* lp = (*l)->getLoopPreheader();
          BasicBlock* pp = (*p)->getLoopPreheader();    
          // get exit blocks of loops
          BasicBlock* le = (*l)->getExitBlock();
          BasicBlock* pe = (*p)->getExitBlock();
          // we only support loops with 1 predecessor
          // and one exit block
          if(!le || !pe || !lp || !pp){
            if(!le || !lp)
              processed.insert(*l);
            if(!pp || !pe)
              processed.insert(*p);
            // DEBUG(errs() << "we only support loops with 1 predecessor and one exit block!\n");
            // DEBUG(errs() << (**l) << "\n" << **p << "\n");
            continue;
          }
          // avg sim, inst thresh, num fun args, num BBs. 
          // (TD,*l,*p,0.80,0,6,5); - ok for bzip2 + make all phis different - last a
          LoopComparator*  lc = new LoopComparator(TD,*l,*p, MinSim, MinInstructions, MaxNumCommonValues, MaxNumBBs, SimilarityMetric, UseHeap,false);
          if(lc->compare(M)){
            compared_loops.push_front(lc);
          }
          else{
            delete lc;
          }
        }
      }
    }
    //errs() << "# total number of pairs that could possibly be merged: " << compared_loops.size() << "\n";
    if (UseHeap)
      std::make_heap(compared_loops.begin(), compared_loops.end(), compareLoopComparators);
    while(!compared_loops.empty()){
      if (UseHeap)
        std::pop_heap(compared_loops.begin(), compared_loops.end(), compareLoopComparators); // move largest to back
      LoopComparator* lc = compared_loops.back();
      DEBUG(errs() << "lc sim: " << lc->getSim() << "\n");
      compared_loops.pop_back();
      Loop* l = lc->getL1();
      Loop* p = lc->getL2();
      std::vector<Instruction*> defsL;
      std::vector<Instruction*> defsP;
      if (processed.find(l) != processed.end() || processed.find(p) != processed.end())
        continue;
      if(std::strcmp(l->getHeader()->getName().data(),"do.body72") == 0){
        processed.insert(l);
        continue;
      }
      // ok now safe to clone loops
      std::vector<BasicBlock*> clonedL;
      std::vector<BasicBlock*> clonedP;
      ValueToValueMapTy vmapL;
      ValueToValueMapTy vmapP;
      // cloneLoop(l,clonedL,vmapL);
      // cloneLoop(p,clonedP,vmapP);
      // deleteClonedLoop(clonedL);
      // deleteClonedLoop(clonedP);
      // getDefs(l,defsL);
      // getDefs(p,defsP);
      Instruction* il = l->getHeader()->begin();

      
      std::vector<BasicBlock*> lBBs = l->getBlocks();
      std::vector<BasicBlock*> pBBs = p->getBlocks();
      // get l and p's headers
      BasicBlock* lh = l->getHeader();
      BasicBlock* ph = p->getHeader();
      // get l and p's headers's single predecessors
      // could be preheader - dunno what's the difference
      // precessor is less strict according to the API
      // check diff between prehdeader and predecessor
      BasicBlock* lp = l->getLoopPreheader();
      BasicBlock* pp = p->getLoopPreheader();    
      // get exit blocks of loops
      BasicBlock* le = l->getExitBlock();
      BasicBlock* pe = p->getExitBlock();
      // we only support loops with 1 predecessor
      // and one exit block

      DEBUG(errs() << l << ", " << p << "\n");
      // l and p's enclosing functions
      Function* lF = (*l->block_begin())->getParent();
      Function* pF = (*p->block_begin())->getParent();
      if (std::strcmp(lF->getName().data(), "Perl_do_vop") == 0 || std::strcmp(pF->getName().data(), "Perl_do_vop") == 0)
        int x = 42;
      if (std::strcmp(lF->getName().data(), "writeMotionInfo2NAL") == 0 && std::strcmp(pF->getName().data(), "writeMotionInfo2NAL") == 0)
        continue;
      // get p and l's BBs
            
      // we need to gather all values used by both loops
      std::set<Value*> vals;
      // non-const non-BB vals in l and p
      std::set<Value*> vals_l_set;
      std::set<Value*> vals_p_set;
      std::vector<Value*> vals_l_ord;
      std::vector<Value*> vals_p_ord;
      // values that are inside the basic blocks - we don't need to pass them
      // as arguments
      DenseSet<Value*> vals_ins;
      // get values from loop l
      for (Loop::block_iterator bi = l->block_begin(), be = l->block_end(); bi != be; ++bi) {
        //vals_ins.insert(*bi);
        // no need to include BB values in vals_ins - they won't be in vals
        for(BasicBlock::iterator ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
          // filter out constants - they won't be in vals
          if(!isa<Constant>(ii))
            vals_ins.insert(ii);
          for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
            // removing basic blocks from values - they can't be
            // arguments to functions. ALso remove constants - they are embedded
            // in the instructions
            if (!isa<BasicBlock>(ii->getOperand(no)) && !isa<Constant>(ii->getOperand(no))){
              vals.insert(ii->getOperand(no));
            }
            if (!isa<BasicBlock>(ii->getOperand(no))){
              vals_l_set.insert(ii->getOperand(no));
              if (std::find(vals_l_ord.begin(), vals_l_ord.end(), ii->getOperand(no)) == vals_l_ord.end())
                vals_l_ord.push_back(ii->getOperand(no));
            }
          }
        }
      }
      // get values from loop p
      for (Loop::block_iterator bi = p->block_begin(), be = p->block_end(); bi != be; ++bi) {
        //vals_ins.insert(*bi);
        // no need to include BB values in vals_ins - they won't be in vals
        for(BasicBlock::iterator ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
          // filter out constants - they won't be in vals
          if(!isa<Constant>(ii))
            vals_ins.insert(ii);
          for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
            // removing basic blocks from values - they can't be
            // arguments to functions. ALso remove constants - they are embedded
            // in the instructions
            if (!isa<BasicBlock>(ii->getOperand(no)) && !isa<Constant>(ii->getOperand(no))){
              vals.insert(ii->getOperand(no));
            }
            if (!isa<BasicBlock>(ii->getOperand(no))){
              vals_p_set.insert(ii->getOperand(no));
              if (std::find(vals_p_ord.begin(), vals_p_ord.end(), ii->getOperand(no)) == vals_p_ord.end())
                vals_p_ord.push_back(ii->getOperand(no));
            }
          }
        }
      }

      // remove values inside loops from vals
      for(auto vii = vals_ins.begin(), vie = vals_ins.end(); vii != vie; ++vii){
        auto it = vals.find(*vii);
        if(it != vals.end()){
          // remove value since it's comming from inside loop
          vals.erase(it);
        }
      }

      DEBUG(errs() << "values: " << vals.size() << "\n");
      // values only present in l and p. the ones that
      // are missing are represented by nulls
      std::vector<Value*> vals_in_l;
      std::vector<Value*> vals_in_p;
      // for(auto vii = vals.begin(), vie = vals.end(); vii != vie; ++vii){
      //   DEBUG(errs() << **vii << " ");
      //   // check to see if value is in l
      //   auto itl = vals_l_set.find(*vii);
      //   if(itl != vals_l_set.end()){
      //     vals_in_l.push_back(*vii);
      //   }              
      //   else{
      //     vals_in_l.push_back(nullptr);
      //   }
      //   // do the same for p
      //   auto itp = vals_p_set.find(*vii);
      //   if(itp != vals_p_set.end()){
      //     vals_in_p.push_back(*vii);
      //   }              
      //   else{
      //     vals_in_p.push_back(nullptr);
      //   }
      // }
      std::multiset<Value*, MergeFunctions::type_compare> vals_l_set_ord_type;
      std::multiset<Value*, MergeFunctions::type_compare> vals_p_set_ord_type;
      
      // get vals in l and p ordered by type
      errs() << "####\n";
      for (auto vi = vals_l_ord.begin(), ve = vals_l_ord.end(); vi != ve; ++vi){
        if (isa<Instruction>(*vi) && vals_ins.find(*vi) == vals_ins.end())
          vals_l_set_ord_type.insert(*vi);
      }
      
      for (auto vi = vals_p_ord.begin(), ve = vals_p_ord.end(); vi != ve; ++vi){
        if (isa<Instruction>(*vi) && vals_ins.find(*vi) == vals_ins.end())
          vals_p_set_ord_type.insert(*vi);
      }
      
      // get vals_ in l and p ordered by
      for (auto vi = vals_l_set_ord_type.begin(), ve = vals_l_set_ord_type.end(); vi != ve; ++vi){
        vals_in_l.push_back(*vi);
      }

      for (auto vi = vals_p_set_ord_type.begin(), ve = vals_p_set_ord_type.end(); vi != ve; ++vi){
        vals_in_p.push_back(*vi);
      }
      
      // if we have a different number of values go on
      if (vals_in_l.size() != vals_in_p.size())
        continue;
      
      bool term = false;
      for (unsigned ii = 0, ie = vals_in_l.size(); ii != ie; ++ii){
        if (vals_in_l.at(ii)->getType() != vals_in_p.at(ii)->getType()){//!lc->isEquivalentType(vals_in_l.at(ii)->getType(), vals_in_p.at(ii)->getType()))
          term = true;
          break;
        }
      }
      // if there is a type mismatch
      if (term)
        continue;
      if (vals_in_l.size() > lc->getMaxFunArgs() || vals_in_l.size() < MinNumCommonValues){
        errs() << "high number of function args: " << vals_in_l.size() << "\n";
        continue;
      }

      // print vals in l and vals in p
      DEBUG(errs() << "\nvals in l: \n");
      for(auto vall: vals_in_l){
        if(vall)
          DEBUG(errs() << *vall << " ");
        else
          DEBUG(errs() << "NULL;; ");
      }
      DEBUG(errs() << "\nvals in p: \n");
      for(auto valp: vals_in_p){
        if(valp)
          DEBUG(errs() << *valp << ";; "); 
        else
          DEBUG(errs() << "NULL;; ");
      }                       
      DEBUG(errs() << "\n");

      // now we need to replace all uses of values in p with corresponding values in p
      // refuse to merge when function has very high number of input arguments
      // what if the values in in l and p are different?
      
      // add loops to processed set
      processed.insert(l);
      processed.insert(p);

      if (lc->canBeMerged()){
        ++num_merged_analysis_pairs;
        continue;
      }
      // after this point we start to merge 
      

      std::vector<Type*> args;
      std::vector<Value*> vals_vec;
      // putting the values in some sorted order - need to check if
      // DenseSet iterators always iterate in the same order
      for(auto vii = vals_in_l.begin(), vie = vals_in_l.end(); vii != vie; ++vii){
        args.push_back((*vii)->getType());
      }            
      // add extra control parameter
      args.push_back(Type::getInt1Ty(getGlobalContext()));
      DEBUG(errs() << "types: \n");
      for(auto t: args){
        DEBUG(errs() << *t << "\n");
      }
      FunctionType *FT = FunctionType::get(Type::getVoidTy(getGlobalContext()),args,false);
      Function* newF = Function::Create(FT,GlobalValue::LinkageTypes::InternalLinkage,l->getHeader()->getName() +"_" + p->getHeader()->getName() + "_merged",&M);
      // add named arguments for newF
      unsigned Idx = 0;
      // map to restore arguments
      std::map<Value*,Value*> argsL;
      std::map<Value*,Value*> argsP;
      for (Function::arg_iterator AI = newF->arg_begin(); Idx != vals_in_l.size(); ++AI, ++Idx) {
        AI->setName(vals_in_l[Idx]->getName()+"_m");
        // replace values used in l defed outside of l with arguments
        for (Loop::block_iterator bi = l->block_begin(), be = l->block_end(); bi != be; ++bi) {
          for(BasicBlock::iterator ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
            for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              if (ii->getOperand(no) == vals_in_l.at(Idx)){
                argsL[AI] = ii->getOperand(no);
                ii->setOperand(no,AI);
              }
            }
          }
        }
        // replace values used in p defed outside of p with arguments
        for (Loop::block_iterator bi = p->block_begin(), be = p->block_end(); bi != be; ++bi) {
          for(BasicBlock::iterator ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
            for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              if (ii->getOperand(no) == vals_in_p.at(Idx)){
                argsP[AI] = ii->getOperand(no);
                ii->setOperand(no,AI);
              }
            }
          }
        }
      }
      
      // add last argument with appropriate name
      (--(newF->arg_end()))->setName("ctrl");
      // create vectors of basic blocks with everything ready in them
      BasicBlock* exit = BasicBlock::Create(getGlobalContext(),"_merged_exit");
      if (std::strcmp(lF->getName().data(),"pattern_2_string")==0)
        int x = 42;
      lc->prepareCFGsForMerge((--(newF->arg_end())), exit);
      
      for(auto& bb: lc->cfg_for_merge){
        if(bb)
          DEBUG(errs() << *bb << "\n");
      }
      for(auto& a: lc->annots_for_merge){
        DEBUG(errs() << a << "\n");
      }
      for(auto& e: lc->extra_cf_blocks){
        DEBUG(errs() << *e << "\n");
      }
      // create basic block that will be the first entry point to the function 
      BasicBlock* ctrl = BasicBlock::Create(getGlobalContext(),"merged_header", newF);
      // insert both sets of blocks after ctrl block
      // unlink all of l and p's BBs. Put l's basic blocks in 
      // newF
      BasicBlock* prev = ctrl;
      // insert cfg blocks into newF
      for(auto& bb: lc->cfg_for_merge){
        if (bb)
          newF->getBasicBlockList().push_back(bb);
      }
      // insert the extra A blocks
      for(auto& bb: lc->extra_cf_blocks){
        if (bb)
          newF->getBasicBlockList().push_back(bb);
      }
      newF->getBasicBlockList().push_back(exit);
            
      // now put the right branch instruction in ctrl
      // and at the ends of of the loops
      Instruction* br = BranchInst::Create(lc->entry, ctrl);
      // make loop headers point to right exit block
      Instruction* ret = ReturnInst::Create(getGlobalContext(), exit);
      // need to modify predecessors and successors of loop headers
      DEBUG(errs() << "preds:\n");
      DEBUG(errs() << *lp << "\n");
      DEBUG(errs() << *pp << "\n");
      // create call instruction in predecessor, pass values and parameter, and make it
      // branch to the loop successor
      TerminatorInst* lpt = lp->getTerminator();
      // create values for l
      std::vector<Value*> vals_l;
      // need to figure out value - is it 0 or 1?
      vals_in_l.push_back(ConstantInt::get(Type::getInt1Ty(getGlobalContext()),1));
      DEBUG(errs() << "vals that will be passed to l: \n");
      for(auto val: vals_in_l){
        DEBUG(errs() << *val << ";; ");
      }
      DEBUG(errs() << "\n");
      Instruction* lci = CallInst::Create(newF,vals_in_l,"",lpt);
      if(isa<BranchInst>(lpt)){
        // modify branch targets 
        assert(lpt->getNumSuccessors() == 1 && "we can only have 1 successor to loop intro block?");
        lpt->setSuccessor(0,le);
        // what about second successor?
        // well, unsure - can the branch instruction be conditional?
        // at the successor of a loop? maybe, maybe not...
      }
      TerminatorInst* ppt = pp->getTerminator();
      // create values for p
      std::vector<Value*> vals_p;
      
      // need to figure out value - is it 0 or 1?
      vals_in_p.push_back(ConstantInt::get(Type::getInt1Ty(getGlobalContext()),0));
      DEBUG(errs() << "vals that will be passed to p: \n");
      for(auto val: vals_in_p){
        DEBUG(errs() << *val << ";; ");
      }
      DEBUG(errs() << "\n");

      Instruction* pci = CallInst::Create(newF,vals_in_p,"",ppt);
      if(isa<BranchInst>(ppt)){
        // modify branch targets 
        assert(ppt->getNumSuccessors() == 1 && "we can only have 1 successor to loop intro block?");
        ppt->setSuccessor(0,pe);
        // what about second successor?
        // well, unsure - can the branch instruction be conditional?
        // at the successor of a loop? maybe, maybe not...
      }
      // go to all operands of all instructions in newF
      // if operands are the same as vals_vec, replace use with argument
            
      // keep pointers to extraL, extraP, blcs for phis
      std::vector<BasicBlock*> bbs_phi;
            
      // replace all uses of this value with the argument
      //vals_vec[Idx]->replaceAllUsesWith(AI);
      // we can try to do the diff merge here

      // we need all lcs blocks - including for inc
      // and cond ones
      DenseSet<BasicBlock*> bb_lcs;
      DenseSet<BasicBlock*> extras;
      std::vector<BasicBlock*> newBBp_todel;

      // DEBUG(errs() << "print out bb_lcs2lcs:\n");
      // for(auto& m: lc->bb_lcs2lcs_rev){
      //   DEBUG(errs() << *m.first << " -> \n");
      //   for(auto& n: m.second){                
      //     DEBUG(errs() << "\t");
      //     DEBUG(errs() << *n.first << " -> " << *n.second << "\n");
      //   }
      // } 
      std::vector<BasicBlock*> del_extra;
      std::vector<BasicBlock*> extra_ls;
      std::vector<BasicBlock*> extra_ps;
      prev = nullptr;
      std::set<PHINode*> phis_created;
      std::vector<PHINode*> phis_created_2way;
      for(unsigned i = 0, ie = lc->bb_lcs.size(); i != ie; ++i){
        BasicBlock* blcs = lc->bb_lcs.at(i);
        std::vector<BasicBlock*> preds;
        for (pred_iterator SI = pred_begin(blcs), SE = pred_end(blcs); SI != SE; ++SI){
          // only consider predecessors that are not kind 0
          // and are in the cfg_map (ie not headr/merge blocks)
          auto kind_it = lc->cfg_kind_map.find(*SI);
          if (kind_it == lc->cfg_kind_map.end() || kind_it->second == 0)
            continue;
          preds.push_back(*SI);
        }
        assert(preds.size() <= 2 && "can't have more than 2 predecessors\n");
        BasicBlock* extral = nullptr;
        BasicBlock* extrap = nullptr;
        switch(preds.size()){
        case 0:
          break;
        case 1:{
          // get the right kind
          unsigned kind = lc->cfg_kind_map[preds.at(0)];
          if (kind == 1){
            extrap = preds.at(0);
          }
          else if (kind == 2){
            extral = preds.at(0);
          } 
          break;
        }
        case 2:
          // need to get the right order based on kind of first pred
          unsigned kind = lc->cfg_kind_map[preds.at(0)];
          if (kind == 1){
            extral = preds.at(1);
            extrap = preds.at(0);
          }
          else if (kind == 2){
            extral = preds.at(0);
            extrap = preds.at(1);                  
          }
          break;
        }

        BasicBlock* orig = blcs;
        if (extrap){
          for(BasicBlock::iterator ii = extrap->begin(), ie = extrap->end(); ii != ie; ++ii){
            DEBUG(errs() << extrap->getName().data() << "\n");
            if (std::strcmp(extrap->getName().data(), "for.body151.4.1") == 0)
              int x = 42;
            for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              Instruction* li = dyn_cast<Instruction>(ii->getOperand(no));
              if (!li)
                continue;
              auto l_inst = lc->bb_lcs2lcs_rev[*pred_begin(extrap)].find(li); // TODO: maybe use current block? or if we need prev, use pred_begin??
              // logic is:
              // if operand uses a def defined in an extra block - that's
              // fine, we'l access it - nope, had to add code
              // if it uses a lcs def - that's fine as well - nope had to add code
              // if it uses a def value that was in the original p loop
              // we use the reverse map to update the operand

              // replace all usese with ??
                  
              if(l_inst != lc->bb_lcs2lcs_rev[*pred_begin(extrap)].end()){
                assert(lc->isEquivalentType(li->getType(), l_inst->second->getType()) && "equiv types\n");
                if (li->getType()->isPointerTy() || l_inst->second->getType()->isPointerTy()){
                  // insert cast, if needed
                  Type *IntPtrTy = TD ? TD->getIntPtrType(l_inst->second->getType()) : NULL;
                  // static Value *createCastIfNeeded(Value *V, Type *DstType,
                  //            Instruction *InsertBefore, Type *IntPtrType) {
                  Value* l_inst_v = const_cast<Instruction*>(l_inst->second);
                  Value* cast = createCastIfNeeded(l_inst_v, li->getType(), ii, IntPtrTy);
                  //BitCastInst::Create(Instruction::BitCast, li, l_inst->second->getType(), "bitcast", ii);
                  li->replaceAllUsesWith(cast);
                }
                else
                  li->replaceAllUsesWith(const_cast<Instruction*>(l_inst->second));
                // Value* val = const_cast<Instruction*>(l_inst->second);
                // ii->setOperand(no, val);
              }
            }
          }
        }
        // if one of the blocks is empty skip phi node creation
        if(!extral || extral->getInstList().size() <= 1){
          prev = blcs;
          continue;
        }              
        if(extral)
          extra_ls.push_back(extral);
        if(!extrap || extrap->getInstList().size() <= 1){
          prev = blcs;
          continue;
        }
        if(extrap)
          extra_ps.push_back(extrap);
        // get all uses in blcs
        std::set<Instruction*> uses_blcs;
        for(BasicBlock::iterator ii = blcs->begin(), ie = blcs->end(); ii != ie; ++ii){
          for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
            // check if operand is not constant
            if(Instruction* inst = dyn_cast<Instruction>(ii->getOperand(no)))
              uses_blcs.insert(inst);
          }
        }
        // get all defs in extraL and extraP
        std::set<const Instruction*> defL;
        for(BasicBlock::const_iterator ii = extral->begin(), ie = extral->end(); ii != ie; ++ii){
          defL.insert(ii);
        }
        std::set<const Instruction*> defP;
        for(BasicBlock::const_iterator ii = extrap->begin(), ie = extrap->end(); ii != ie; ++ii){
          defP.insert(ii);
        }
        // check if any use in blcs is defined in extraL/extraP, or both
        // do the check by using bb_lcs2lcs map
        // if yes, create PhiNode
        for(auto it = uses_blcs.begin(), ite = uses_blcs.end(); it != ite; ++it){
          // go over instructions in blcs
          Instruction* use_in_p = nullptr;
          for(BasicBlock::iterator ii = blcs->begin(), ie = blcs->end(); ii != ie; ++ii){
            // if we find a match in the map
            auto p_it = lc->bb_lcs2lcs[orig].find(ii);
            // DEBUG(errs() << "found : " << (p_it != lc->bb_lcs2lcs[orig].end()) << "\n");
            // DEBUG(errs() << "is orig in lcs2lcs: " << (lc->bb_lcs2lcs.find(orig) != lc->bb_lcs2lcs.end()) << "\n");
            if (p_it != lc->bb_lcs2lcs[orig].end()){
              // we found a matching p istruction - go over
              // their arguments and see if they contain the use value
              int no = 0;
              int ne = ii->getNumOperands();
                    
              for (no = 0; no != ne; ++no){
                if (ii->getOperand(no) == *it)
                  break;
              }
              if (no != ne){
                use_in_p = dyn_cast<Instruction>((*p_it).second->getOperand(no));
                break;
              }
            }
            if (use_in_p)
              break;
          }
          // find whether l defed instruction exists
          Instruction* itL = nullptr;
          
          for(auto exi = lc->extras.begin(), exe = lc->extras.end(); exi != exe; ++exi){
            for(auto exii = (*exi)->begin(), exie = (*exi)->end(); exii != exie; ++exii){
              if (*it != exii)
                continue;
              itL = exii;
              break;
            }
          }
          //   use_in_p = use_container->
          // finally check if the defs contain it or not
          if(itL && use_in_p){
            // create Phi Node with both its 
            DEBUG(errs() << *(itL)->getType() << " -> " << *use_in_p->getType() << "\n");
            DEBUG(errs() << *itL << " -> " << *use_in_p << "\n");
            //assert((*itL)->getType() == use_in_p->getType() && "types must be equal!\n"); 
            // investigate if name has effect - it doesnt
                  
            PHINode* phi = PHINode::Create(itL->getType(),2,"phi",itL->getParent()->getTerminator()->getSuccessor(0)->begin());
            phis_created.insert(phi);
            // create phi node
            //cast l
            Instruction* l_cast = const_cast<Instruction*>(itL); 
            Instruction* l_cast_cp = l_cast;
            Instruction* use_in_p_cp = use_in_p;
            
            BasicBlock* EI = l_cast->getParent();

            phi->addIncoming(l_cast,EI);
            phi->addIncoming(use_in_p, use_in_p->getParent());

            // replace all uses outside of def block EI
            auto UI = l_cast->use_begin(), E = l_cast->use_end();
            for (; UI != E;) {
              Use &U = *UI;
              ++UI;
              auto *Usr = dyn_cast<Instruction>(U.getUser());
              if (Usr && Usr->getParent() == EI)
                continue;
              U.set(phi);
            }
            // replace all uses outside of def block EI
            auto UI2 = use_in_p->use_begin(), E2 = use_in_p->use_end();
            for (; UI2 != E2;) {
              Use &U = *UI2;
              ++UI2;
              auto *Usr = dyn_cast<Instruction>(U.getUser());
              if (Usr && Usr->getParent() == use_in_p->getParent())
                continue;
              U.set(phi);
            }
            // need to re-update phi node's l incoming
            phi->setIncomingValue(0,l_cast_cp);
            phi->setIncomingValue(1,use_in_p_cp);
          }
          else if(itL){
            // no need for phi?
          }
          else if(use_in_p){
            // no need for phi?
          }
          else{
            // no need for phi?
          }
        }

        // DEBUG(errs() << "vals vec: \n");
        // for(auto& v: vals_vec){
        //   DEBUG(errs() << *v << "\n");
        // }
        // DEBUG(errs() << "vals :\n");
        // for(auto& v: vals){
        //   DEBUG(errs() << *v << "\n");
        // } 
        // finally, update  extrap 
        // i.e. the instructions inside of them
        // which reference values found in loop p
        // use map
        // move up - if extraL is null this won't get executed !!! that's why
        // we are seein this case!
              
      }
      
      // do update on bb_lcs
      std::vector<Instruction*> select_vec;
      std::map<Value*, Value*> cast2val_map;
      DenseMap<std::pair<Value*, Type*>, Value* > bitcasts;
      for(auto blcsi = lc->bb_lcs.begin(), blcse = lc->bb_lcs.end(); blcsi != blcse; ++blcsi){
        auto blcs = *blcsi;
        //DEBUG(errs() << "BB : " <<*blcs << "\n");
        // finally, update the lcs blocks
        // when they are accessing conflicting arg values

        // for(BasicBlock::iterator ii = blcs->begin(), ie = blcs->end(); ii != ie; ++ii){
        //   // get corresponding instruction in p loop
        //   auto iip = lc->bb_lcs2lcs[blcs].find(ii);
        //   if(iip != lc->bb_lcs2lcs[blcs].end()){
        //     for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
        //       auto l_inst = ii->getOperand(no);
        //       auto p_inst = iip->second->getOperand(no);
        //       // if corresponding operands are different                    
        //       if(l_inst != p_inst && vals.find(l_inst) != vals.end() && vals.find(p_inst) != vals.end()){
        //         // need to create select instruction
        //         // if types are pointers, need to bitcast
        //         //assert(lc->isEquivalentType(l_inst->getType(), p_inst->getType()) && "values must have eq types!");
        //         SelectInst* select;
        //         //CastInst* bitcast = BitCastInst::Create(Instruction::BitCast,p_inst,l_inst->getType(),"bcast.n.from.int");
        //         std::vector<Instruction*>::iterator is;
        //         std::vector<Instruction*>::iterator ise = select_vec.end();
        //         for(auto& p: cast2val_map){
        //           DEBUG(errs() << *p.first << " -> " << *p.second <<"\n");
        //         }
        //         for(auto& s: select_vec){
        //           DEBUG(errs() << *s << "\n");
        //         }
        //         for(is = select_vec.begin(); is != ise; ++is){
        //           auto op_it = cast2val_map.find(const_cast<Value*>((*is)->getOperand(2)));
        //           if (op_it != cast2val_map.end())
        //             int x = 10;
        //           DEBUG(errs() << (op_it != cast2val_map.end())  << " " << (op_it->second == p_inst) << " " <<  ((*is)->getOperand(1) == l_inst) << "\n");
        //           if((op_it != cast2val_map.end() && op_it->second == p_inst && (*is)->getOperand(1) == l_inst)
        //              || ((*is)->getOperand(1) == l_inst && (*is)->getOperand(2) == p_inst)){
        //             select = cast<SelectInst>(*is);
        //             break;
        //           }
        //         }
        //         if (is == select_vec.end()){
        //           Type *IntPtrTy = TD ? TD->getIntPtrType(p_inst->getType()) : NULL;
        //           // static Value *createCastIfNeeded(Value *V, Type *DstType,
        //           //            Instruction *InsertBefore, Type *IntPtrType) {
        //           Value* p_inst_v = cast<Value>(p_inst);
        //           Value* cast = nullptr;
        //           auto bitcast_it = bitcasts.find(std::make_pair(p_inst_v, l_inst->getType()));
        //           if (bitcast_it != bitcasts.end()){
        //             cast = bitcast_it->second;
        //           }
        //           else{
        //             cast = createCastIfNeeded(p_inst_v, l_inst->getType(), newF->begin()->getTerminator(), IntPtrTy);
        //             if (cast != p_inst_v){
        //               cast2val_map[cast] = p_inst_v;
        //               bitcasts[std::make_pair(p_inst_v, l_inst->getType())] = cast;
        //             }
        //           }
        //           select = SelectInst::Create(&(newF->getArgumentList().back()),l_inst,cast,"select",newF->begin()->getTerminator());
        //           //p_inst->replaceAllUsesWith(cast);
        //           select_vec.push_back(select);
        //         }
        //         // make ii's operand use select now
        //         // if it's a load or store update type
        //         // replace all uses with?
        //         ii->setOperand(no,select);
        //         //ii->getOperand(no)->replaceAllUsesWith(select);
        //       }
        //     }
        //   }
        // }
        
        // lastly update very obscure case of phi nodes
        for(auto II = (*blcsi)->begin(), IE = (*blcsi)->end(); II != IE; ++II){
          PHINode* phi_inst = dyn_cast<PHINode>(II);
          if (phis_created.find(phi_inst) != phis_created.end())
            continue;
          for(unsigned no = 0, ne = II->getNumOperands(); no != ne; ++no){
            Instruction* use = dyn_cast<Instruction>(II->getOperand(no));
            if (!use)
              continue;
            for(auto EI = lc->extras.begin(), EE = lc->extras.end(); EI != EE; ++EI){
              for(BasicBlock::iterator EII = (*EI)->begin(), EIE = (*EI)->end(); EII != EIE; ++EII){
                DEBUG(errs() << use->getName().data() << "\n");
                if (std::strcmp(use->getName().data(),"idxprom66") == 0)
                  int x = 10;
                if (std::strcmp(EII->getName().data(), "idxprom66") == 0)
                  int y = 10;
                if (cast<Value>(EII) != cast<Value>(use))
                  continue;
                if (lc->bb_lcs2lcs[*blcsi][II]->getNumOperands() != II->getNumOperands())
                  continue;
                if (use == lc->bb_lcs2lcs[*blcsi][II]->getOperand(no))
                  continue;
                Instruction* cast_inst = dyn_cast<Instruction>(lc->bb_lcs2lcs[*blcsi][II]->getOperand(no)); // idx53
                auto it_cast_inst = lc->bb_lcs2lcs_rev[*blcsi].find(cast_inst); // idx64
                if (!cast_inst || it_cast_inst == lc->bb_lcs2lcs_rev[*blcsi].end())
                  continue;
                // use is defined in an extra block and is diff from corresponding
                // p lcs operand
                // put at top of basic block of II
                PHINode* phi = PHINode::Create(use->getType(),2,"phi_obscure",II->getParent()->begin());
                phis_created.insert(phi);
                // in the even more obscure case when the use is not immediately after the def
                PHINode* phi_pred = nullptr;
                BasicBlock* after_def_block = nullptr;
                if ((*EI)->getTerminator()->getSuccessor(0) != *blcsi){
                  after_def_block = (*EI)->getTerminator()->getSuccessor(0);
                  phi_pred = PHINode::Create(use->getType(),2,"phi_obscure_pred",(*EI)->getTerminator()->getSuccessor(0)->begin());
                  phis_created.insert(phi_pred);
                  BasicBlock* pred1 = *pred_begin((*EI)->getTerminator()->getSuccessor(0));
                  BasicBlock* pred2 = *(++pred_begin((*EI)->getTerminator()->getSuccessor(0)));
                  
                  if (pred1 == *EI && lc->cfg_kind_map[*EI]==2){
                    phi_pred->addIncoming(use,*EI);
                    phi_pred->addIncoming(Constant::getNullValue(use->getType()), pred2);
                  }
                  else if (pred2 == *EI && lc->cfg_kind_map[*EI]==2){
                    phi_pred->addIncoming(use,*EI);
                    phi_pred->addIncoming(Constant::getNullValue(use->getType()), pred1);
                  }
                  else if (pred1 == *EI && lc->cfg_kind_map[*EI]==1){
                    phi_pred->addIncoming(Constant::getNullValue(use->getType()), pred2);                    
                    phi_pred->addIncoming(use,*EI);
                  }
                  else if (pred2 == *EI && lc->cfg_kind_map[*EI]==1){
                    phi_pred->addIncoming(Constant::getNullValue(use->getType()), pred1);                    
                    phi_pred->addIncoming(use,*EI);
                  }
                  else
                    llvm_unreachable("impossiiible\n");
                  // replace all uses outside of def block EI
                  auto UI = use->use_begin(), E = use->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && Usr->getParent() == *EI)
                      continue;
                    U.set(phi_pred);
                  }
                  // re-update incoming value
                  
                  switch(lc->cfg_kind_map[*EI]){
                  case 1:
                    phi_pred->setIncomingValue(1,use);
                    break;
                  case 2:
                    phi_pred->setIncomingValue(0,use);
                    break;
                  default:
                    llvm_unreachable("impossible\n");
                  } 
                }
                // if we have the predecessor phi
                // figure out kind of extra
                if (!phi_pred){
                  // if we have the next block defined
                  switch(lc->cfg_kind_map[*EI]){
                  case 1:
                    phi->addIncoming(use,*EI);
                    phi->addIncoming(const_cast<Instruction*>((*it_cast_inst).second), const_cast<BasicBlock*>((*it_cast_inst).second->getParent()));
                  case 2:
                    phi->addIncoming(const_cast<Instruction*>((*it_cast_inst).second), const_cast<BasicBlock*>((*it_cast_inst).second->getParent()));
                    phi->addIncoming(use,*EI);
                    break;
                  default:
                    llvm_unreachable("impossible\n");
                  }
                  // replace all uses with outside of definition block?
                  // yes - this will make sure that def block uses the right values
                  // and all subsequent references access the phi node
                  //use->replaceUsesOutsideBlock(phi,*EI);
                  auto UI = use->use_begin(), E = use->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && (Usr->getParent() == *EI || Usr->getParent() == after_def_block))
                      continue;
                    U.set(phi);
                  }
                  // re-update incoming value

                  switch(lc->cfg_kind_map[*EI]){
                  case 1:
                    phi->setIncomingValue(0,use);
                    break;
                  case 2:
                    phi->setIncomingValue(1,use);
                    break;
                  default:
                    llvm_unreachable("impossible\n");
                  }
                }
                // phi_pred has been touched
                else{
                  BasicBlock* pred1 = *pred_begin(*blcsi);
                  BasicBlock* pred2 = *(++pred_begin(*blcsi));
                  
                  assert(++pred_begin(*blcsi) != pred_end(*blcsi) && "0 0 block case??\n");
                  
                  if (lc->cfg_kind_map[pred1] == lc->cfg_kind_map[*EI]){
                    phi->addIncoming(phi_pred,pred1);
                    phi->addIncoming(const_cast<Instruction*>((*it_cast_inst).second), pred2);                  
                  }
                  else if (lc->cfg_kind_map[pred2] == lc->cfg_kind_map[*EI]){
                    phi->addIncoming(phi_pred,pred2);
                    phi->addIncoming(const_cast<Instruction*>((*it_cast_inst).second), pred1);
                  }
                  else{
                    if (lc->cfg_kind_map[pred1] == 0){
                      phi->addIncoming(phi_pred,pred1);
                      phi->addIncoming(const_cast<Instruction*>((*it_cast_inst).second), pred2);                                        
                    }
                    else if (lc->cfg_kind_map[pred2] == 0){
                      phi->addIncoming(phi_pred,pred2);
                      phi->addIncoming(const_cast<Instruction*>((*it_cast_inst).second), pred1);
                    }
                    else
                      llvm_unreachable("very obscure cfg_kind case\n");
                  }
                  // do update of all uses

                  // replace all uses with outside of definition block?
                  // yes - this will make sure that def block uses the right values
                  // and all subsequent references access the phi node
                  //use->replaceUsesOutsideBlock(phi,*EI);
                  auto UI = use->use_begin(), E = use->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && (Usr->getParent() == *EI || Usr->getParent() == after_def_block))
                      continue;
                    U.set(phi);
                  }
                  // re-update incoming value
                  // TODO: not sure why this works
                  switch(lc->cfg_kind_map[*EI]){
                  case 1:
                    phi->setIncomingValue(0,const_cast<Instruction*>((*it_cast_inst).second));
                    break;
                  case 2:
                    phi->setIncomingValue(1,const_cast<Instruction*>((*it_cast_inst).second));
                    break;
                  default:
                    llvm_unreachable("impossible\n");
                  }

                }
              }
            }
          }
        }
        prev = blcs;
      }
      // asserts start
      for(unsigned ii = 0, ie = lc->extra_ls.size(); ii != ie; ++ii){
        //DEBUG(errs() << *lc->extra_ls.at(ii) << "--\n" << *extra_ls.at(ii) << "\n");
        //assert(lc->extra_ls.at(ii) == extra_ls.at(ii) && "must agree l\n");
      }
      for(unsigned ii = 0, ie = lc->extra_ps.size(); ii != ie; ++ii){
        //DEBUG(errs() << *lc->extra_ps.at(ii) << "--\n" << *extra_ps.at(ii) << "\n");
        //assert(lc->extra_ps.at(ii) == extra_ps.at(ii) && "must agree p\n");
      }
      // asserts ends
      // update the extra blocks
      for(unsigned lbbi = 0, lbbe = lc->extra_ls.size(); lbbi != lbbe; ++lbbi){
        BasicBlock* lbb = lc->extra_ls.at(lbbi);
        std::set<Instruction*> defL;
        // get defs in l
        for(auto ii = lbb->begin(), iie = lbb->end(); ii != iie; ++ii){
          defL.insert(ii);
        }
        for(unsigned lbbi_n = lbbi+1; lbbi_n != lbbe; ++lbbi_n){
          BasicBlock* lbb_next = lc->extra_ls.at(lbbi_n);
          for(auto ii = lbb_next->begin(), iie = lbb_next->end(); ii != iie; ++ii){
            for(unsigned no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              if (auto cast_inst = dyn_cast<Instruction>(ii->getOperand(no))){
                if(defL.find(cast_inst) != defL.end()){
                  DEBUG(errs() << "FOUND :" << *cast_inst << "\n");
                  DEBUG(errs() << *lbb << "\n" << *lbb_next << "\n");
                  // put phi at the start of the successor of lbb
                  PHINode* phi = PHINode::Create(cast_inst->getType(),2,"phi_nlcs",(lbb)->getTerminator()->getSuccessor(0)->begin());
                  phis_created.insert(phi);
                  BasicBlock* pred1 = *pred_begin(lbb->getTerminator()->getSuccessor(0));
                  BasicBlock* pred2 = *(++pred_begin(lbb->getTerminator()->getSuccessor(0)));
                        
                  if (pred2 == lbb){
                    phi->addIncoming(cast_inst,pred2);
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred1);
                  }
                  else if (pred1 == lbb){
                    phi->addIncoming(cast_inst,pred1);
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred2);
                  }
                  else
                    llvm_unreachable("unknown predecessor L ...\n");
                  // replace all uses?
                  //cast_inst->replaceUsesOutsideBlock(phi,*lbb);
                  auto UI = cast_inst->use_begin(), E = cast_inst->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && Usr->getParent() == lbb)
                      continue;
                    U.set(phi);
                  }
                  // re-update incoming value
                  phi->setIncomingValue(0,cast_inst);
                }
              }
            }
          }
        }
      }
      // same for p
      for(unsigned pbbi = 0, pbbe = lc->extra_ps.size(); pbbi != pbbe; ++pbbi){
        BasicBlock* pbb = lc->extra_ps.at(pbbi);
        std::set<Instruction*> defP;
        // get defs in P
        for(auto ii = pbb->begin(), iie = pbb->end(); ii != iie; ++ii){
          defP.insert(ii);
        }
        for(unsigned pbbi_n = pbbi+1; pbbi_n != pbbe; ++pbbi_n){
          BasicBlock* pbb_next = lc->extra_ps.at(pbbi_n);
          for(auto ii = pbb_next->begin(), iie = pbb_next->end(); ii != iie; ++ii){
            for(unsigned no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              if (auto cast_inst = dyn_cast<Instruction>(ii->getOperand(no))){
                if(defP.find(cast_inst) != defP.end()){
                  DEBUG(errs() << "FOUND :" << *cast_inst << "\n");
                  DEBUG(errs() << *pbb << "\n" << *pbb_next << "\n");
                  // put phi at the start of the successor of lbb
                  PHINode* phi = PHINode::Create(cast_inst->getType(),2,"phi_nlcs",(pbb)->getTerminator()->getSuccessor(0)->begin());
                  phis_created.insert(phi);
                  BasicBlock* pred1 = *pred_begin(pbb->getTerminator()->getSuccessor(0));
                  BasicBlock* pred2 = *(++pred_begin(pbb->getTerminator()->getSuccessor(0)));
                        
                  if (pred1 == pbb){
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred2);
                    phi->addIncoming(cast_inst,pred1);
                  }
                  else if (pred2 == pbb){
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred1);
                    phi->addIncoming(cast_inst,pred2);
                  }
                  else
                    llvm_unreachable("unknown pred P...\n");
                  // replace all uses?
                  //cast_inst->replaceUsesOutsideBlock(phi,*pbb);
                  auto UI = cast_inst->use_begin(), E = cast_inst->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && Usr->getParent() == pbb)
                      continue;
                    U.set(phi);
                  }
                  // re-update incoming value
                  phi->setIncomingValue(1,cast_inst);
                }
              }
            }
          }
        }
      }
      // special 
      // what if def in extraL is used in a blcs?
      for(unsigned lbbi = 0, lbbe = lc->extra_ls.size(); lbbi != lbbe; ++lbbi){
        BasicBlock* lbb = lc->extra_ls.at(lbbi);
        std::set<Instruction*> defL;
        // get defs in l
        for(auto ii = lbb->begin(), iie = lbb->end(); ii != iie; ++ii){
          defL.insert(ii);
        }
        for(auto blcsi = lc->bb_lcs.begin(), blcse = lc->bb_lcs.end(); blcsi != blcse; ++blcsi){
          auto blcs = *blcsi;
          for(auto ii = blcs->begin(), iie = blcs->end(); ii != iie; ++ii){
            auto phi_inst = dyn_cast<PHINode>(ii);
            if (phis_created.find(phi_inst) != phis_created.end())
              continue;
            for(unsigned no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              if (auto cast_inst = dyn_cast<Instruction>(ii->getOperand(no))){
                if(defL.find(cast_inst) != defL.end()){
                  bool flag = false;
                  for(auto phi_cr = phis_created.begin(), phi_cr_e = phis_created.end(); phi_cr != phi_cr_e; ++phi_cr){
                    if((*phi_cr)->getIncomingValue(0) == cast_inst || (*phi_cr)->getIncomingValue(1) == cast_inst){
                      flag = true;
                      break;
                    }
                  }
                  if (flag)
                    continue;
                  DEBUG(errs() << "FOUND :" << *cast_inst << "\n");
                  // put phi at the start of the successor of lbb
                  PHINode* phi = PHINode::Create(cast_inst->getType(),2,"phi_nlcs_blcs_l",(lbb)->getTerminator()->getSuccessor(0)->begin());
                  phis_created.insert(phi);

                  BasicBlock* pred1 = *pred_begin(lbb->getTerminator()->getSuccessor(0));
                  BasicBlock* pred2 = *(++pred_begin(lbb->getTerminator()->getSuccessor(0)));
                        
                  if (pred2 == lbb){
                    phi->addIncoming(cast_inst,pred2);
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred1);
                  }
                  else if (pred1 == lbb){
                    phi->addIncoming(cast_inst,pred1);
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred2);
                  }
                  else
                    llvm_unreachable("unknown predecessor L ...\n");
                  // replace all uses?
                  //cast_inst->replaceUsesOutsideBlock(phi,*lbb);
                  auto UI = cast_inst->use_begin(), E = cast_inst->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && Usr->getParent() == lbb)
                      continue;
                    U.set(phi);
                  }
                  // re-update incoming value
                  phi->setIncomingValue(0,cast_inst);
                }
              }
            }
          }
        }
      }
      // special for p
      // what if ...
      // same for p
      for(unsigned pbbi = 0, pbbe = lc->extra_ps.size(); pbbi != pbbe; ++pbbi){
        BasicBlock* pbb = lc->extra_ps.at(pbbi);
        std::set<Instruction*> defP;
        // get defs in P
        for(auto ii = pbb->begin(), iie = pbb->end(); ii != iie; ++ii){
          defP.insert(ii);
        }
        for(auto blcsi = lc->bb_lcs.begin(), blcse = lc->bb_lcs.end(); blcsi != blcse; ++blcsi){
          auto blcs = *blcsi;
          for(auto ii = blcs->begin(), iie = blcs->end(); ii != iie; ++ii){
            auto phi_inst = dyn_cast<PHINode>(ii);
            if (phis_created.find(phi_inst) != phis_created.end())
              continue;
            for(unsigned no = 0, ne = ii->getNumOperands(); no != ne; ++no){
              if (auto cast_inst = dyn_cast<Instruction>(ii->getOperand(no))){
                if(defP.find(cast_inst) != defP.end()){
                  bool flag = false;
                  for(auto phi_cr = phis_created.begin(), phi_cr_e = phis_created.end(); phi_cr != phi_cr_e; ++phi_cr){
                    if((*phi_cr)->getIncomingValue(0) == cast_inst || (*phi_cr)->getIncomingValue(1) == cast_inst){
                      flag = true;
                      break;
                    }
                  }
                  if (flag)
                    continue;
                  DEBUG(errs() << "FOUND :" << *cast_inst << "\n");
                  // put phi at the start of the successor of lbb
                  PHINode* phi = PHINode::Create(cast_inst->getType(),2,"phi_nlcs_blcs_p",(pbb)->getTerminator()->getSuccessor(0)->begin());
                  phis_created.insert(phi);

                  BasicBlock* pred1 = *pred_begin(pbb->getTerminator()->getSuccessor(0));
                  BasicBlock* pred2 = *(++pred_begin(pbb->getTerminator()->getSuccessor(0)));
                        
                  if (pred1 == pbb){
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred2);
                    phi->addIncoming(cast_inst,pred1);
                  }
                  else if (pred2 == pbb){
                    phi->addIncoming(Constant::getNullValue(cast_inst->getType()), pred1);
                    phi->addIncoming(cast_inst,pred2);
                  }
                  else
                    llvm_unreachable("unknown pred P...\n");
                  // replace all uses?
                  //cast_inst->replaceUsesOutsideBlock(phi,*pbb);
                  auto UI = cast_inst->use_begin(), E = cast_inst->use_end();
                  for (; UI != E;) {
                    Use &U = *UI;
                    ++UI;
                    auto *Usr = dyn_cast<Instruction>(U.getUser());
                    if (Usr && Usr->getParent() == pbb)
                      continue;
                    U.set(phi);
                  }
                  // re-update incoming value
                  phi->setIncomingValue(1,cast_inst);
                }
              }
            }
          }
        }
      }

      // fix extras
      // for(auto exi = lc->extras.begin(), exe = lc->extras.end(); exi != exe; ++exi){              
      //   auto ex = *exi;
      //   DEBUG(errs() << *ex << "\n");
      //   // finally, update the lcs blocks
      //   // when they are accessing conflicting arg values
      //   for(BasicBlock::iterator ii = ex->begin(), ie = ex->end(); ii != ie; ++ii){
      //     for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
      //       auto inst = ii->getOperand(no);
      //       // if operand is in values
      //       if(vals.find(inst) != vals.end()){
                    
      //         // need to create select instruction
      //         // if types are pointers, need to bitcast
      //         //assert(lc->isEquivalentType(l_inst->getType(), p_inst->getType()) && "values must have eq types!");
      //         SelectInst* select = nullptr;
      //         //CastInst* bitcast = BitCastInst::Create(Instruction::BitCast,p_inst,l_inst->getType(),"bcast.n.from.int");
      //         std::vector<Instruction*>::iterator is;
      //         std::vector<Instruction*>::iterator ise = select_vec.end();
      //         for(is = select_vec.begin(); is != ise; ++is){
      //           // we only select one of two values every time so this is fine
      //           if((*is)->getOperand(1) == inst || (*is)->getOperand(2) == inst){
      //             select = cast<SelectInst>(*is);
      //             break;
      //           }
      //         }
      //         // make ii's operand use select now
      //         // if it's a load or store update type
      //         DEBUG(errs() << *ii << "\n");
      //         if(select)
      //           ii->setOperand(no,select);
      //       }
      //     }
      //   }
      // }              
      // go over defs in extras and pull them one BB behind
            
      // replace arguments again
      for(Function::iterator bi = newF->begin(), be = newF->end(); bi != be; ++bi){
        for(BasicBlock::iterator ii = bi->begin(), ie = bi->end(); ii != ie; ++ii){
          for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
            // iterate over the arguments/vals_vec and find the value that we care about
            Idx = 0;
            for (Function::arg_iterator AI = newF->arg_begin(); Idx != vals_vec.size(); ++AI, ++Idx) {
              // if the two value are refering to the same thing
              if(vals_vec[Idx] == ii->getOperand(no)){
                //DEBUG(errs() << "IS THIS EXECUTED??\n");
                ii->setOperand(no,AI);
                break;
              }
              //vals_vec[Idx]->replaceAllUsesWith(AI);
            }
          }
        }
      }                                    
            
      // print newF
      DEBUG(errs() << "bb_phi: \n");
      DEBUG(errs() << "module: \n");
      // check if it was profitable to merge. 
      // if not, revert back from clonedL, clonedP
      unsigned newFinsts = 0;
      for(auto bi = newF->begin(), be = newF->end(); bi != be; ++bi){
        for(auto ii = bi->begin(), ie = bi->end(); ii != ie; ++ii)
          ++newFinsts;
      }
      if ( true){//isLikelyProfitableToMerge(lc->comb_insts, args.size(), newFinsts, 2)){
        // delete cloned loops - no need 
        // to do anything more with them
        // deleteClonedLoop(clonedL);
        // deleteClonedLoop(clonedP);
        // do the old deletions
        // drop references and unlink
        for(auto& bb: lBBs){
          bb->removeFromParent();             
          bb->dropAllReferences();
        }
        for(auto& bb: pBBs){
          if (bb->getParent() != newF){
            bb->removeFromParent();
            bb->dropAllReferences();
          }
          //delete bb;
        }
        for(auto& bb: newBBp_todel){
          bb->removeFromParent();
          bb->dropAllReferences();
        }
        for(auto& de: del_extra){
          de->removeFromParent();
          de->dropAllReferences();
        }
        // delete
        for(auto& de: del_extra){
          delete de;
        }
        for(auto& bb: newBBp_todel){
          delete bb;
        }
        
        errs() << "newF:\n ";
        if (std::strcmp(newF->getName().data(),"for.cond223_for.cond151_merged") == 0)
          int x = 42;
        errs() << *newF << "\n";

        for(auto rbb = pBBs.rbegin(); rbb != pBBs.rend(); ++rbb){
          if(*rbb){
            for(auto ii = (*rbb)->begin(), ie = (*rbb)->end(); ii != ie; ++ii){
              for(auto ui = ii->use_begin(), ue = ii->use_end(); ui != ue; ++ui){
                DEBUG(errs() <<"use: "<< **ui << "\n");
              }
              for(auto ui = ii->user_begin(), ue = ii->user_end(); ui != ue; ++ui){
                DEBUG(errs() <<"user: "<< **ui << "\n");
                if(auto b = dyn_cast<Instruction>(*ui))
                  DEBUG(errs() << *b->getParent()->getParent() << "\n");
              }
            }
          }
          if (*rbb){
            for(auto ui = (*rbb)->use_begin(), ue = (*rbb)->use_end(); ui != ue; ++ui){
              DEBUG(errs() << "use: " << **ui << "\n");
            }
            for(auto ui = (*rbb)->user_begin(), ue = (*rbb)->user_end(); ui != ue; ++ui){
              DEBUG(errs() << "user: " << **ui << "\n");
            }
          }
          if ((*rbb)->getParent() != newF)
            delete *rbb;
        }
        for(auto& bb: lBBs){
          delete bb;
        }
        DEBUG(errs() << "lc sim: " << lc->getSim() << "\n");
        ++num_merged_pairs;
      }
      else{
        // remove call insts
        lci->eraseFromParent();
        pci->eraseFromParent();
        // insert cloned blocks into original functions
        for(auto bi = clonedL.begin(), be = clonedL.end(); bi != be; ++bi){
          lF->getBasicBlockList().insert(le,*bi);
        }
        for(auto bi = clonedP.begin(), be = clonedP.end(); bi != be; ++bi){
          pF->getBasicBlockList().insert(pe,*bi);
        }
        // replace preds branch
        lpt->setSuccessor(0,clonedL.at(0));
        ppt->setSuccessor(0,clonedP.at(0));
        // replace all values defed in L
        // get all values defed in L
        // need to replace their uses after the 
        // body of the for loop
        
        for(Instruction* ii: defsL){
          for(User* u: ii->users()){
            if(Instruction* inst = dyn_cast<Instruction>(u)){
              if (inst->getParent()->getParent() == newF)
                continue;
              //errs() << *ii << " is used in: " << *inst << "\n";
              for (Use &use : inst->operands()){
                Value* v = use.get();
                if (v == ii){
                  Value* rhs = vmapL[ii];
                  use = rhs;
                }
              }             
            }
          }
        }

        for(Instruction* ii: defsP){
          for(User* u: ii->users()){
            if(Instruction* inst = dyn_cast<Instruction>(u)){
              if (inst->getParent()->getParent() == newF)
                continue;
              //errs() << *ii << " is used in: " << *inst << "\n";
              for (Use &use : inst->operands()){
                Value* v = use.get();
                if (v == ii){
                  Value* rhs = vmapP[ii];
                  use = rhs;
                }
              }              
            }
          }
        }
      
        // use clonedL and clonedP to restore 
        // finally we need to replace arguments
        for (Function::arg_iterator AI = newF->arg_begin(); Idx != vals_in_l.size(); ++AI, ++Idx) {
          // replace values used in l defed outside of l with arguments
          for (auto bi = clonedL.begin(), be = clonedL.end(); bi != be; ++bi) {
            for(BasicBlock::iterator ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
              for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
                if (ii->getOperand(no) == vals_in_l.at(Idx)){
                  ii->setOperand(no,argsL[AI]);
                }
              }
            }
          }
          // replace values used in p defed outside of p with arguments
          for (auto bi = clonedP.begin(), be = clonedP.end(); bi != be; ++bi) {
            for(BasicBlock::iterator ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
              for(int no = 0, ne = ii->getNumOperands(); no != ne; ++no){
                if (ii->getOperand(no) == vals_in_p.at(Idx)){
                  ii->setOperand(no,argsP[AI]);
                }
              }
            }
          }
        }
        // kill l bloks
        // kill p blocks
        for(auto& bb: lBBs){
          bb->removeFromParent();             
          bb->dropAllReferences();
        }
        for(auto& bb: lBBs){
          delete bb;
        }
        for(auto& bb: pBBs){
          bb->removeFromParent();
          bb->dropAllReferences();
        }
        for(auto& bb: pBBs){
          delete bb;
        }
        // just delete the thing...
        //newF->removeFromParent();
        //newF->dropAllReferences();
        // debug start
        // errs() << *newF << "\n";
        // errs() << "l:\n";
        // for(auto& b: clonedL)
        //   errs() << *b;
        // errs() << "p:\n";
        // for(auto bi = clonedP.begin(), be = clonedP.end(); bi != be; ++bi)
        //   errs() << **bi;
        
        // debug end
        //delete newF;
        errs() << "l func:\n" << *clonedL.at(0)->getParent();
        errs() << "p func:\n" << *clonedP.at(0)->getParent();

      }
      str += "(" + std::to_string(lc->comb_insts) + ", " + std::to_string(newFinsts + 2 + newF->getArgumentList().size()*2) + "),";      
      str_cfg += "(" + std::to_string(bucket.first*2) + ", " + std::to_string(newF->getBasicBlockList().size()) + "),";
      total_improv += lc->comb_insts - newFinsts - 2 - newF->getArgumentList().size()*2; // number of extra call instructions
      total_cfg_diff += bucket.first*2 - newF->getBasicBlockList().size();
      total_phis += phis_created.size();
    }
    for(auto& loop: compared_loops){
      delete loop;
    }
  }
  errs() << "# num analys pairs: " << num_merged_analysis_pairs << "\n"; 
  errs() << "# inst aftermath:\n";
  errs() << "ins_l = [" + str + "]\n";
  errs() << "# cfg aftermath:\n";
  errs() << "cfg_l = [" + str_cfg + "]\n";
  errs() << "# phis: " << total_phis << "\n";
  errs() << "# total improvement: " << total_improv << "\n";
  errs() << "# total cfg improvement: " << total_cfg_diff << "\n";
  errs() << "# mod insts: " << mod_insts << "\n";
  errs() << "# loop insts: " << total_loop_insts << "\n";
  errs() << "# mod cfg: " << mod_cfg << "\n";
  errs() << "# rel inst improvement: " << (1.0 *total_improv) / (1.0*mod_insts) << "\n";
  errs() << "# rel cfg improvement: " << (1.0 *total_cfg_diff) / (1.0*mod_cfg) << "\n";
  errs() << "# total possible improvement: " << (0.5*total_loop_insts) / (1.0* mod_insts) << "\n";
  // try to find this stupid value...
  // Function* debugF = M.getFunction("Perl_do_vop");
  // for(auto bi = debugF->begin(), be = debugF->end(); bi != be; ++bi){
  //   for(auto ii = bi->begin(), ie = bi->end(); ii != ie; ++ii){
  //     if (std::strcmp(ii->getName().data(),"sv.addr") == 0){
  //       for(User* user: ii->users()){
  //         errs() << "user : " << *user << " ::";
  //         if (Instruction* inst = dyn_cast<Instruction>(user)){
  //           for(auto ui = inst->use_begin(), ue = inst->use_end(); ui != ue; ++ui){
  //             errs() << **ui << " ";
  //           }
  //           errs() << inst->getParent()->getParent()->getName() << "\n";
  //         }
  //         else
  //           errs() << "non-inst " << *user << "\n";
  //       }
  //     }
  //   }
  // }
  // // Perl_sv_2pv_flags
  // debugF = M.getFunction("Perl_sv_2pv_flags");
  // for(auto bi = debugF->begin(), be = debugF->end(); bi != be; ++bi){
  //   for(auto ii = bi->begin(), ie = bi->end(); ii != ie; ++ii){
  //     if (std::strcmp(ii->getName().data(),"sv.addr") == 0){
  //       for(User* user: ii->users()){
  //         errs() << "user : " << *user << " ::";
  //         if (Instruction* inst = dyn_cast<Instruction>(user)){
  //           for(auto ui = inst->use_begin(), ue = inst->use_end(); ui != ue; ++ui){
  //             errs() << **ui << " ";
  //           }            
  //           errs() << inst->getParent()->getParent()->getName() << "\n";
  //         }
  //         else
  //           errs() << "non-inst " << *user << "\n";
  //       }
  //     }
  //   }
  // }
  
  errs() <<"# " << M.size() << " " << nf <<  "\n";
  DEBUG(errs() << "end of program...\n");
  errs () << "# " <<  "Num Loops: "<< loops.size() << "\n";
  errs () << "# " << "num merged pairs: " << num_merged_pairs << "\n";
  // do {
  //   unsigned InsertCount = Registry.enqueue();
  //   DEBUG(errs() << "size of module: " << M.size() << '\n');
  //   DEBUG(errs() << "size of worklist: " << InsertCount << '\n');
  //   Changed |= doExhaustiveCompareMerge();
  // } while (Registry.haveDeferred());
  // Changed |= doDiffMerge();
  return true;
}
bool MergeFunctions::compareLoopComparators(LoopComparator* l1, LoopComparator* l2){
  if (l1->getSim() < l2->getSim())
    return true;
  else
    return false;
}
bool MergeFunctions::prepareValuesForCallInst(const std::vector<Value*> &vals_vec, const std::vector<Value*> &vals_in_x, std::vector<Value*> &vals_x){
  for(unsigned i = 0, e = vals_vec.size(); i != e; ++i){
    Value* val = vals_in_x.at(i);
    if(val)
      vals_x.push_back(val);
    else{
      Type* t = vals_vec.at(i)->getType();
      DEBUG(errs() << "null type of l: \n");
      std::string type_str;
      llvm::raw_string_ostream rso(type_str);
      t->print(rso);
      DEBUG(errs() << rso.str() << "\n");
       switch (t->getTypeID()) {
       case Type::MetadataTyID:
         return false;         
       default:
         vals_x.push_back(Constant::getNullValue(t));
         break;
       }
    //   switch (t->getTypeID()) {
    //     // switch on the type of the value - since the loop doesn't need it
    //     // create constants
    //   default:
    //     DEBUG(errs() << "unsupported type: " << *(val->getType()) << "\n");
    //     break;
    //   case Type::IntegerTyID:
    //     // http://llvm.org/docs/doxygen/html/ValueTracking_8cpp_source.html#l00044
    //     // how to get bit size for scalar + using TD for vecs I guess
    //     switch(t->getScalarSizeInBits()){
    //     case 1:
    //       vals_x.push_back(ConstantInt::get(Type::getInt1Ty(getGlobalContext()),0));
    //       break;
    //     case 8:
    //       vals_x.push_back(ConstantInt::get(Type::getInt8Ty(getGlobalContext()),0));
    //       break;
    //     case 16:
    //       vals_x.push_back(ConstantInt::get(Type::getInt16Ty(getGlobalContext()),0));
    //       break;
    //     case 32:
    //       vals_x.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()),0));
    //       break;
    //     case 64:
    //       vals_x.push_back(ConstantInt::get(Type::getInt64Ty(getGlobalContext()),0));
    //       break;
    //     default:
    //       DEBUG(errs() << "unsupported integer bit length\n");
    //       break;
    //     }
    //     break;
    //   case Type::VectorTyID:
    //     // Ty1 == Ty2 would have returned true earlier.
    //     break;
                  
    //   case Type::VoidTyID:
    //     break;
    //   case Type::FloatTyID:
    //     break;
    //   case Type::DoubleTyID:
    //     break;
    //   case Type::X86_FP80TyID:
    //     break;
    //   case Type::FP128TyID:
    //     break;
    //   case Type::PPC_FP128TyID:
    //     break;
    //   case Type::LabelTyID:
    //     break;
    //   case Type::MetadataTyID:
    //     break;
                  
    //   case Type::PointerTyID: 
    //     // return constant pointer null
    //     vals_x.push_back(ConstantPointerNull::get(cast<PointerType>(t)));
    //     break;
    //   case Type::StructTyID: 
    //     // StructType *STy1 = cast<StructType>(Ty1);
    //     // StructType *STy2 = cast<StructType>(Ty2);
    //     // if (STy1->getNumElements() != STy2->getNumElements())
    //     //   return false;
                  
    //     // if (STy1->isPacked() != STy2->isPacked())
    //     //   return false;
                  
    //     // for (unsigned i = 0, e = STy1->getNumElements(); i != e; ++i) {
    //     //   if (!isEquivalentType(STy1->getElementType(i), STy2->getElementType(i)))
    //     //     return false;
    //     //}
    //     break;                  
    //   }
    // }
    }
  }
  return true;
}
bool MergeFunctions::replaceLoopWithEmptyBB(Loop* p){
  
  // trying to delete p
  // for (Loop::block_iterator block = (*p)->block_begin(), end = (*p)->block_end(); block != end; block++) {
  //   BasicBlock * bb = *block;
  //   for (BasicBlock::iterator II = bb->begin(); II != bb->end(); ++II) {
  //     Instruction * insII = &(*II);
  //     insII->dropAllReferences();
  //   }
  // }
  DEBUG(errs() << p->empty() << "\n");
  // get first BB of p loop
  BasicBlock* pFst = *p->block_begin();
  // get unique exit block of p loop, if null continue - cant deal with it
  BasicBlock* pExit = p->getUniqueExitBlock();
  if(!pExit) 
    return false;
  // remove all instructions from first block
  std::vector<Instruction*> insts;
  for(auto II = pFst->begin(), IE = pFst->end(); II != IE; ++II){
    insts.push_back(II);
  }
  for(auto inst: insts){
    inst->removeFromParent();
    // also remove references
    inst->dropAllReferences();
  }
  // insert uncond branch to successor at the end of the emptied fst block
  Instruction* br = BranchInst::Create(pExit,pFst);
            
  // get P's enclosing function
  Function* pF = pFst->getParent();

  std::vector<BasicBlock*> bbs;
  for (Loop::block_iterator block = p->block_begin()+1, end = p->block_end(); block != end; ++block) {
    bbs.push_back(*block);
  }
  for(auto block: bbs){
    // remove the BB from the loop
    p->removeBlockFromLoop(block);
    // remove it from the function as well
    block->removeFromParent();
    // make all instructions drop references
    for (BasicBlock::iterator II = block->begin(); II != block->end(); ++II) {
      Instruction * insII = &(*II);
      insII->dropAllReferences();
    }
  }
  // for(auto block: bbs){
              
  // }
  DEBUG(errs() << p->empty() << "\n");
  DEBUG(errs() << *p << "\n");
  DEBUG(errs() << "p's enclosing function:\n");
  DEBUG(errs() << *pF << "\n");
  DEBUG(errs() << "uniq exit:" << *pExit << "\n");          
  return true;
}

// Replace direct callers of Old with New.
// TODO: ask Tobias
// maybe only for 
void MergeFunctions::replaceDirectCallers(Function *Old, Function *New) {
  Constant *BitcastNew = ConstantExpr::getBitCast(New, Old->getType());
  for (Value::use_iterator UI = Old->use_begin(), UE = Old->use_end();
       UI != UE;) {
    Use *U = &*UI;
    ++UI;
    CallSite CS(U->getUser());
    if (CS && CS.isCallee(U)) {
      Registry.remove(CS.getInstruction()->getParent()->getParent());
      U->set(BitcastNew);
    }
  }
}

// Replace G with an alias to F if possible, or else a thunk to F. Deletes G.
void MergeFunctions::writeThunkOrAlias(Function *F, Function *G) {
  if (HasGlobalAliases && G->hasUnnamedAddr()) {
    if (G->hasExternalLinkage() || G->hasLocalLinkage() ||
        G->hasWeakLinkage()) {
      writeAlias(F, G);
      return;
    }
  }

  writeThunk(F, G);
}

// Helper for writeThunk,
// Selects proper bitcast operation,
// but a bit simplier then CastInst::getCastOpcode.
static Value* createCast(IRBuilder<false> &Builder, Value *V, Type *DestTy) {
  Type *SrcTy = V->getType();
  if (SrcTy->isIntegerTy() && DestTy->isPointerTy())
    return Builder.CreateIntToPtr(V, DestTy);
  else if (SrcTy->isPointerTy() && DestTy->isIntegerTy())
    return Builder.CreatePtrToInt(V, DestTy);
  else
    return Builder.CreateBitCast(V, DestTy);
}

// Replace G with a simple tail call to bitcast(F). Also replace direct uses
// of G with bitcast(F). Deletes G.
void MergeFunctions::writeThunk(Function *F, Function *G) {
  if (!G->mayBeOverridden()) {
    // Redirect direct callers of G to F.
    replaceDirectCallers(G, F);
  }

  // If G was internal then we may have replaced all uses of G with F. If so,
  // stop here and delete G. There's no need for a thunk.
  if (G->hasLocalLinkage() && G->use_empty()) {
    DEBUG(dbgs() << "All uses of " << G->getName() << " replaced by "
          << F->getName() << ". Removing it.\n");
    G->eraseFromParent();
    return;
  }

  Function *NewG = Function::Create(G->getFunctionType(), G->getLinkage(), "",
                                    G->getParent());
  BasicBlock *BB = BasicBlock::Create(F->getContext(), "", NewG);
  IRBuilder<false> Builder(BB);

  SmallVector<Value *, 16> Args;
  unsigned i = 0;
  FunctionType *FFTy = F->getFunctionType();
  Type *IntPtrTy = TD ? TD->getIntPtrType(FFTy->getContext()) : NULL;
  for (Function::arg_iterator AI = NewG->arg_begin(), AE = NewG->arg_end();
       AI != AE; ++AI) {
    Value *Orig = AI;
    Value *Cast = createCastIfNeeded(AI, FFTy->getParamType(i), NULL, IntPtrTy);
    if (Cast != Orig)
      Builder.Insert(cast<Instruction>(Cast));
    Args.push_back(Cast);
    ++i;
  }

  CallInst *CI = Builder.CreateCall(F, Args);
  CI->setTailCall();
  CI->setCallingConv(F->getCallingConv());
  if (NewG->getReturnType()->isVoidTy()) {
    Builder.CreateRetVoid();
  } else {
    Builder.CreateRet(createCast(Builder, CI, NewG->getReturnType()));
  }

  NewG->copyAttributesFrom(G);
  NewG->takeName(G);
  removeUsers(G);
  G->replaceAllUsesWith(NewG);
  G->eraseFromParent();

  DEBUG(dbgs() << "writeThunk: " << NewG->getName() << " calling "
        << F->getName() << '\n');
  ++NumThunksWritten;
}

void MergeFunctions::writeThunkWithChoice(Function *NewF, Function *OldF,
                                          int Choice) {
  // Deleting the body of a function sets its linkage to External. Save the old
  // one here and restore it at the end.
  GlobalValue::LinkageTypes OldFLinkage = OldF->getLinkage();

  // Delete OldF's body
  OldF->deleteBody();

  // Insert single BB with tail call
  BasicBlock *BB = BasicBlock::Create(OldF->getContext(), "", OldF);
  IRBuilder<false> Builder(BB);

  SmallVector<Value *, 16> Args;
  unsigned i = 0;
  FunctionType *FFTy = NewF->getFunctionType();
  Type *IntPtrTy = TD ? TD->getIntPtrType(FFTy->getContext()) : NULL;
  for (Function::arg_iterator AI = OldF->arg_begin(), AE = OldF->arg_end();
       AI != AE; ++AI) {
    Value *Orig = AI;
    Value *Cast = createCastIfNeeded(AI, FFTy->getParamType(i), NULL, IntPtrTy);
    if (Cast != Orig)
      Builder.Insert(cast<Instruction>(Cast));
    Args.push_back(Cast);
    ++i;
  }
  Args.push_back(Builder.getInt32(Choice));

  CallInst *CI = Builder.CreateCall(NewF, Args);
  CI->setTailCall();
  CI->setCallingConv(NewF->getCallingConv());
  if (OldF->getReturnType()->isVoidTy()) {
    Builder.CreateRetVoid();
  } else {
    Type *RetTy = OldF->getReturnType();
    if (CI->getType()->isIntegerTy() && RetTy->isPointerTy())
      Builder.CreateRet(Builder.CreateIntToPtr(CI, RetTy));
    else if (CI->getType()->isPointerTy() && RetTy->isIntegerTy())
      Builder.CreateRet(Builder.CreatePtrToInt(CI, RetTy));
    else
      Builder.CreateRet(Builder.CreateBitCast(CI, RetTy));
  }

  OldF->setLinkage(OldFLinkage);
}

// Replace G with an alias to F and delete G.
void MergeFunctions::writeAlias(Function *F, Function *G) {
  PointerType *PTy = G->getType();
  auto *GA = GlobalAlias::create(PTy->getElementType(), PTy->getAddressSpace(),
                                 G->getLinkage(), "", F);
  F->setAlignment(std::max(F->getAlignment(), G->getAlignment()));
  GA->takeName(G);
  GA->setVisibility(G->getVisibility());
  removeUsers(G);
  G->replaceAllUsesWith(GA);
  G->eraseFromParent();

  DEBUG(dbgs() << "writeAlias: " << GA->getName() << '\n');
  ++NumAliasesWritten;
}

// Merge two equivalent functions. Upon completion, Function G is deleted.
void MergeFunctions::mergeTwoFunctions(Function *F, Function *G) {
  if (F->mayBeOverridden()) {
    assert(G->mayBeOverridden());

    if (HasGlobalAliases) {
      // Make them both thunks to the same internal function.
      Function *H = Function::Create(F->getFunctionType(), F->getLinkage(), "",
                                     F->getParent());
      H->copyAttributesFrom(F);
      H->takeName(F);
      removeUsers(F);
      F->replaceAllUsesWith(H);

      unsigned MaxAlignment = std::max(G->getAlignment(), H->getAlignment());

      writeAlias(F, G);
      writeAlias(F, H);

      F->setAlignment(MaxAlignment);
      F->setLinkage(GlobalValue::PrivateLinkage);
    } else {
      // We can't merge them. Instead, pick one and update all direct callers
      // to call it and hope that we improve the instruction cache hit rate.
      replaceDirectCallers(G, F);
    }

    ++NumDoubleWeak;
  } else {
    writeThunkOrAlias(F, G);
  }

  ++NumFunctionsMerged;
}

static void insertCondAndRemapInstructions(
                                           Instruction *F1InstInNewF, const std::vector<const Instruction *> &F2Insts,
                                           Function *NewF, ValueToValueMapTy &F1toNewF,
                                           const SmallVectorImpl<FunctionComparator *> &Comps,
                                           Type *IntPtrTy) {
  assert(F2Insts.size() == Comps.size() &&
         "Mis-match between F2Insts & Comps!");

  SmallVector<Instruction *, 8> F2InstsInNewF;
  for (unsigned FnI = 0, FnE = F2Insts.size(); FnI != FnE; ++FnI) {
    const Instruction *F2Inst = F2Insts[FnI];
    if (!F2Inst) {
      F2InstsInNewF.push_back(NULL);
      continue;
    }

    Instruction *F2InstInNewF = F2Inst->clone();

    // Remap F2Inst: F2 values -> F1 values
    RemapInstruction(F2InstInNewF, Comps[FnI]->getF2toF1Map(),
                     RF_NoModuleLevelChanges);
    // Remap F2Inst: F1 values -> NewF values
    RemapInstruction(F2InstInNewF, F1toNewF, RF_NoModuleLevelChanges);

    F2InstsInNewF.push_back(F2InstInNewF);
  }

  SmallVector<TerminatorInst *, 8> Terminators;
  SplitBlockAndInsertSwitch(&NewF->getArgumentList().back(), F1InstInNewF,
                            F2InstsInNewF, Terminators, IntPtrTy);

  assert(Terminators.size() == F2InstsInNewF.size() + 1 &&
         "Not enough terminators returned");

  // F2InstsInNewF are now hooked up to the correct values in NewF. However,
  // some of their operands may be pointers/integers so they could potentially
  // have the wrong type in NewF (since we treat all pointers and integers of
  // same size as equal). Insert casts if needed.
  for (unsigned FnI = 0, FnE = F2InstsInNewF.size(); FnI != FnE; ++FnI) {
    Instruction *F2InstInNewF = F2InstsInNewF[FnI];
    if (!F2InstInNewF)
      continue;
    const Instruction *F2Inst = F2Insts[FnI];

    for (unsigned OpId=0; OpId < F2InstInNewF->getNumOperands(); ++OpId) {
      Value *F2NewFOperand = F2InstInNewF->getOperand(OpId);
      Value *F2OrigOperand = F2Inst->getOperand(OpId);

      if (F2NewFOperand->getType() != F2OrigOperand->getType()) {
        Value *Cast = createCastIfNeeded(F2NewFOperand,
                                         F2OrigOperand->getType(),
                                         F2InstInNewF,
                                         IntPtrTy);
        F2InstInNewF->setOperand(OpId, Cast);
      }
    }
  }
}

static void mergePHINode(
                         const SmallVectorImpl<FunctionComparator *> &Fns,
                         Function *NewF,
                         ValueToValueMapTy &VMap, /* F1->FNew */
                         const PHINode *F1PhiInst,
                         std::vector<const Instruction *> F2PhiInsts) {
  PHINode *F1PhiInNewF = dyn_cast<PHINode>(VMap[F1PhiInst]);
  assert(F1PhiInNewF && "Cannot find F1Inst in NewF!");

  // The incoming blocks in any of the F2PhiInsts may be in a different order.
  // If this is the case, we have to reorder them. F2PhiInsts is intentionally a
  // copy, so we can modify it
  SmallVector<PHINode *, 4> GCInsts; // so we can delete them later.
  for (unsigned FnI = 0, FnE = F2PhiInsts.size(); FnI != FnE; ++FnI) {
    const PHINode *F2PhiInst = dyn_cast_or_null<const PHINode>(F2PhiInsts[FnI]);
    if (!F2PhiInst)
      continue;
    
    for (unsigned I = 0, E = F1PhiInNewF->getNumIncomingValues(); I < E; ++I) {
      if (!Fns[FnI]->enumerate(F1PhiInst->getIncomingBlock(I),
                               F2PhiInst->getIncomingBlock(I))) {
        // Non-equivalent blocks in the same position - need to reorder PhiInst
        PHINode *ReorderedF2PhiInst = PHINode::Create(F2PhiInst->getType(), E);
        
        for (unsigned II = 0; II < E; ++II) {
          Value *BBVal =
            Fns[FnI]->getF1toF2Map()[F1PhiInst->getIncomingBlock(II)];
          BasicBlock *BB = cast<BasicBlock>(BBVal);
          Value *Val = F2PhiInst->getIncomingValueForBlock(BB);
          ReorderedF2PhiInst->addIncoming(Val, BB);
        }
        
        F2PhiInsts[FnI] = ReorderedF2PhiInst;
        GCInsts.push_back(ReorderedF2PhiInst);
        break;
      }
    }
  }
  
  // Now merge the PHI nodes.
  for (unsigned i = 0; i < F1PhiInNewF->getNumIncomingValues(); ++i) {
    Value *F1InValNewF = F1PhiInNewF->getIncomingValue(i),
      *F1InVal = F1PhiInst->getIncomingValue(i);
    BasicBlock *F1NewFInBlock = F1PhiInNewF->getIncomingBlock(i);
    // If this is a repeat occurrence of the same incoming BasicBlock, we
    // will have already dealt with it in a previous iteration.
    if (F1PhiInNewF->getBasicBlockIndex(F1PhiInNewF->getIncomingBlock(i)) !=
        (int)i)
      continue;

    Value *NewIncoming = F1InValNewF;

    Instruction *InsertPt = F1NewFInBlock->getTerminator();

    // Build up a chain of cmps and selects that pick the correct incoming
    // value.
    for (unsigned FnI = 0, FnE = F2PhiInsts.size(); FnI != FnE; ++FnI) {
      if (!F2PhiInsts[FnI])
        continue;
      const PHINode *F2PhiInst = cast<const PHINode>(F2PhiInsts[FnI]);
      Value *F2InVal = F2PhiInst->getIncomingValue(i);

      // If we know these are equivalent, there's no further work to do
      if (Fns[FnI]->enumerate(F1InVal, F2InVal,/*NoSelfRef=*/true) &&
          Fns[FnI]->enumerate(F1PhiInst->getIncomingBlock(i),
                              F2PhiInst->getIncomingBlock(i)))
        continue;

      assert(Fns[FnI]->enumerate(F1PhiInst->getIncomingBlock(i),
                                 F2PhiInst->getIncomingBlock(i)) &&
             "Non-equivalent incoming BBs in PHI.");

      // We have different incoming values from the same block
      // Translate F2's incoming value to NewF if needed
      Value *F2InValNewF = F2InVal;
      if (!isa<Constant>(F2InVal)) {
        Value *V = Fns[FnI]->getF2toF1Map()[F2InVal]; // F2->F1
        F2InValNewF = VMap[V]; // F1->NewF
        assert(V && F2InValNewF && "Cannot map F2InVal to NewF");
      }

      // Cast F2InValNewF to the correct type if needed
      LLVMContext &Ctx = F1InValNewF->getType()->getContext();
      F2InValNewF = createCastIfNeeded(F2InValNewF, F1InValNewF->getType(),
                                       InsertPt, Fns[FnI]->getDataLayout()->getIntPtrType(Ctx));

      // Create compare & select
      Value *ChoiceArg = &NewF->getArgumentList().back();
      Value *SelectBit = new ICmpInst(InsertPt,
                                      ICmpInst::ICMP_EQ,
                                      &NewF->getArgumentList().back(),
                                      ConstantInt::get(ChoiceArg->getType(),
                                                       FnI+1));

      // SelectBit true -> F2InValNewF, SelectBit false -> existing NewIncoming.
      NewIncoming = SelectInst::Create(SelectBit, F2InValNewF, NewIncoming, "",
                                       InsertPt);
    }

    if (NewIncoming == F1InValNewF)
      continue; // no change for this incoming value

    // Replace all occurrences of this incoming value/block by the new
    // ones (phi nodes can have repeated arguments)
    for (unsigned j=i; j < F1PhiInNewF->getNumIncomingValues(); ++j) {
      if (F1PhiInNewF->getIncomingBlock(j) == F1NewFInBlock) {
        F1PhiInNewF->setIncomingValue(j, NewIncoming);
      }
    }
  }

  // Garbage-collect the reordered PHI nodes we temporarily created.
  for (SmallVectorImpl<PHINode *>::iterator I = GCInsts.begin(),
         E = GCInsts.end(); I != E; ++I)
    delete *I;
}

static bool rewriteRecursiveCall(
                                 const CallInst *F1I, const CallInst *F2I, CallInst *NewFI,
                                 const Function *F1, const Function *F2, Function *NewF) {
  if (!(F1I->getCalledFunction() == F1 && F2I->getCalledFunction() == F2) &&
      !(F1I->getCalledFunction() == F2 && F2I->getCalledFunction() == F1))
    return false; // not a recursive/mutually recursive call

  // Replace NewFI by recursive call to NewF with additional choice argument
  SmallVector<Value *, 16> Args;
  for (unsigned AI = 0, End = NewFI->getNumArgOperands(); AI < End; ++AI) {
    Value *Arg = NewFI->getArgOperand(AI);

    // Check if F1 or F2 is one of the arguments (veeery unusual case, don't
    // handle it for now).
    if (Arg == F1 || Arg == F2)
      return false;

    Args.push_back(Arg);
  }

  if (F1I->getCalledFunction() == F1 && F2I->getCalledFunction() == F2) {
    Args.push_back(&NewF->getArgumentList().back());
  } else {
    // Need to invert the choice argument
    Value *ChoiceArg = &NewF->getArgumentList().back();
    Constant *One = ConstantInt::get(ChoiceArg->getType(), 1);
    Args.push_back(BinaryOperator::Create(Instruction::Xor, ChoiceArg, One, "",
                                          NewFI));
  }

  CallInst *CI = CallInst::Create(NewF, Args);
  CI->setCallingConv(NewF->getCallingConv());

  ReplaceInstWithInst(NewFI, CI);

  return true;
}

/// Clone F1 into a new function with F1's name + MERGE_SUFFIX. Adds an
/// additional i32 argument to the function.
static Function *cloneAndAddArgument(Function *F1, ValueToValueMapTy &VMap) {
  LLVMContext &Context = F1->getContext();

  std::vector<Type*> ArgTypes;
  for (Function::const_arg_iterator I = F1->arg_begin(), E = F1->arg_end();
       I != E; ++I)
    ArgTypes.push_back(I->getType());
  ArgTypes.push_back(Type::getInt32Ty(Context));

  FunctionType *FTy = FunctionType::get(F1->getFunctionType()->getReturnType(),
                                        ArgTypes,
                                        F1->getFunctionType()->isVarArg());
  Function *NewF = Function::Create(FTy, GlobalValue::InternalLinkage,
                                    F1->getName()+MERGED_SUFFIX);

  insertFunctionAfter(NewF, F1);

  if (F1->hasSection())
    NewF->setSection(F1->getSection());

  NewF->setCallingConv(CallingConv::Fast);

  Function::arg_iterator DestI = NewF->arg_begin();
  for (Function::const_arg_iterator I = F1->arg_begin(), E = F1->arg_end();
       I != E; ++I) {
    DestI->setName(I->getName()); // Copy the name over...
    VMap[I] = DestI++;            // Add mapping to VMap
  }

  // Name the selector argument
  DestI->setName("__merge_arg");

  SmallVector<ReturnInst*, 8> Returns;
  CloneFunctionInto(NewF, F1, VMap, false, Returns);

  return NewF;
}

typedef std::map<const Instruction *, std::vector<const Instruction *> >
CombinedDiffMap;
// TODO : this will be the function for the merge
void MergeFunctions::outlineAndMergeFunctions(
                                              SmallVectorImpl<FunctionComparator *> &Fns) {
  assert(!Fns.empty() && "Cannot merge empty set of functions");

  // All comparator instances in Fns share the same F1
  Function *F1 = Fns.front()->getF1();

  // Clone F1 into new function with an additional i32 argument
  ValueToValueMapTy VMap; // maps F1 values -> NewF values
  Function *NewF = cloneAndAddArgument(F1, VMap);

  // Combine all the DifferingInstructions maps in Fns into one single map of
  // lists to aid the merging process.
  //
  // Map F1 instruction -> list of F2 instructions indexed by position in Fns.
  CombinedDiffMap AllDifferingInstructions;
  for (unsigned I = 0, E = Fns.size(); I != E; ++I) {
    FunctionComparator *Comp = Fns[I];
    for (DenseMap<const Instruction *, const Instruction *>::iterator
           DiffI = Comp->DifferingInstructions.begin(),
           DiffE = Comp->DifferingInstructions.end();
         DiffI != DiffE; ++DiffI) {
      AllDifferingInstructions[DiffI->first].resize(Fns.size());
      AllDifferingInstructions[DiffI->first][I] = DiffI->second;
    }
  }

  // Merge differing PHI nodes. We need to handle these first because they could
  // be affected later on when we split basic blocks, thus making them
  // impossible to merge.
  for (CombinedDiffMap::const_iterator I = AllDifferingInstructions.begin(),
         E = AllDifferingInstructions.end();
       I != E; ++I) {
    const PHINode *F1PhiInst = dyn_cast<PHINode>(I->first);
    if (!F1PhiInst)
      continue;

    const std::vector<const Instruction *> &F2PhiInsts = I->second;

    mergePHINode(Fns, NewF, VMap, F1PhiInst, F2PhiInsts);
  }

  // Merge recursive calls
  //
  // TODO: We currently only support this optimization for pairs of functions.
  // If more than two functions are merged, we mark the recursive calls as
  // DifferingInstructions which causes switch statements to be inserted and
  // recursive calls going through thunks. It wouldn't be too hard to implement
  // self-recursive calls for multi-merges. *Mutually* recursive calls with
  // multi-merges are a little trickier - that's why we leave it for now.
  if (Fns.size() == 1) {
    FunctionComparator *Comp = Fns.front();
    for (DenseMap<const Instruction *, const Instruction *>::const_iterator
           I = Comp->SelfRefInstructions.begin(),
           E = Comp->SelfRefInstructions.end();
         I != E; ++I) {
      const Instruction *F1I = I->first;
      if (Comp->DifferingInstructions.find(F1I) !=
          Comp->DifferingInstructions.end())
        continue; // Differing in other ways too, so deal with it later.

      // Attempt recursive call rewriting
      if (isa<CallInst>(F1I)) {
        const CallInst *F1Call = cast<const CallInst>(F1I);
        const CallInst *F2Call = dyn_cast<const CallInst>(I->second);
        CallInst *NewFCall = dyn_cast<CallInst>(VMap[F1I]);

        if (F1Call && F2Call && NewFCall &&
            rewriteRecursiveCall(F1Call, F2Call, NewFCall,
                                 Comp->getF1(), Comp->getF2(), NewF))
          continue;
      }

      // Can't rewrite it. Mark as differing and insert conditional later
      Comp->DifferingInstructions[F1I] = I->second;
    }
  } else {
    for (unsigned I = 0, E = Fns.size(); I != E; ++I) {
      FunctionComparator *Comp = Fns[I];
      for (DenseMap<const Instruction *, const Instruction *>::const_iterator
             II = Comp->SelfRefInstructions.begin(),
             EE = Comp->SelfRefInstructions.end();
           II != EE; ++II) {
        const Instruction *F1I = II->first;
        if (Comp->DifferingInstructions.find(F1I) !=
            Comp->DifferingInstructions.end())
          continue; // Differing in other ways too, so deal with it later.

        AllDifferingInstructions[F1I].resize(Fns.size());
        AllDifferingInstructions[F1I][I] = II->second;
      }
    }
  }

  // For each differing instruction, splice basic block, and insert conditional
  LLVMContext &Context = NewF->getContext();
  Type *IntPtrType = TD->getIntPtrType(Context);
  for (CombinedDiffMap::const_iterator I = AllDifferingInstructions.begin(),
         E = AllDifferingInstructions.end();
       I != E; ++I) {
    const Instruction *F1Inst = I->first;
    const std::vector<const Instruction *> &F2Insts = I->second;

    assert(VMap.find(F1Inst) != VMap.end() &&
           "Cannot find differing inst!");
    Instruction *F1InstInNewF = cast<Instruction>(VMap[F1Inst]);

    if (isa<PHINode>(F1InstInNewF))
      continue; // we already handled these above

    insertCondAndRemapInstructions(F1InstInNewF, F2Insts,
                                   NewF, VMap, Fns, IntPtrType);
  }

  // Replace functions with thunks
  writeThunkWithChoice(NewF, F1, 0);
  for (unsigned FnI = 0, FnE = Fns.size(); FnI != FnE; ++FnI) {
    Function *F2 = Fns[FnI]->getF2();
    writeThunkWithChoice(NewF, F2, FnI + 1);
  }
}

// Remove a function from FnTree. If it was already in FnTree, add
// it to Deferred so that we'll look at it in the next round.
void MergeFunctions::remove(Function *F) {
  // We need to make sure we remove F, not a function "equal" to F per the
  // function equality comparator.
  FnTreeType::iterator found = FnTree.find(FunctionPtr(F, TD));
  size_t Erased = 0;
  if (found != FnTree.end() && found->getFunc() == F) {
    Erased = 1;
    FnTree.erase(found);
  }

  if (Erased) {
    DEBUG(dbgs() << "Removed " << F->getName()
          << " from set and deferred it.\n");
    Deferred.push_back(F);
  }
}

// For each instruction used by the value, remove() the function that contains
// the instruction. This should happen right before a call to RAUW.
// TODO: ask Tobias
void MergeFunctions::removeUsers(Value *V) {
  std::vector<Value *> Worklist;
  Worklist.push_back(V);
  while (!Worklist.empty()) {
    Value *V = Worklist.back();
    Worklist.pop_back();

    for (User *U : V->users()) {
      if (Instruction *I = dyn_cast<Instruction>(U)) {
        remove(I->getParent()->getParent());
      } else if (isa<GlobalValue>(U)) {
        // do nothing
      } else if (Constant *C = dyn_cast<Constant>(U)) {
        for (User *UU : C->users())
          Worklist.push_back(UU);
      }
    }
  }
}

bool MergeFunctions::mergeBucket(std::list<ComparableFunction> &Fns,
                                 bool &Changed) {
  for (std::list<ComparableFunction>::iterator FnI = Fns.begin(),
         FnE = Fns.end(); FnI != FnE; ++FnI) {
    if (!FnI->isNew())
      continue;

    if (!FnI->getFunc())
      continue;

    SmallVector<FunctionComparator *, 8> DiffMergeCandidates;

    std::list<ComparableFunction>::iterator Fn2I = FnI;
    // skip the first element
    for (++Fn2I; Fn2I != FnE; ++Fn2I) {
      if (!Fn2I->getFunc())
        continue;

      assert(FnI->getFunc() != Fn2I->getFunc() &&
             "Duplicate function in list!");

      FunctionComparator *Comp = new FunctionComparator(TD, FnI->getFunc(),
                                                        Fn2I->getFunc());

      if (!Comp->compare() || !Comp->isMergeCandidate()) {
        delete Comp;
        continue;
      }

      // Never thunk a strong function to a weak function.
      assert(!FnI->getFunc()->mayBeOverridden() ||
             Fn2I->getFunc()->mayBeOverridden());

      if (Comp->isExactMatch()) {
        // Equiv merge the two functions. Throw away any diff merge
        // candidate we might have found so far.
        delete Comp;

        DEBUG(dbgs() << "- Equiv merge " << FnI->getFunc()->getName()
              << " == " << Fn2I->getFunc()->getName() << '\n');

        Function *DeleteF = Fn2I->getFunc();
        Registry.remove(DeleteF, /*reanalyze=*/false);

        mergeTwoFunctions(FnI->getFunc(), DeleteF);

        Changed = true;

        // mergeTwoFunctions may have removed functions from this bucket and
        // invalidated the iterators. Rescan the whole bucket, continuing
        // from the current function (previous ones will have been
        // markCompared())
        for (SmallVector<FunctionComparator *, 8>::iterator
               I = DiffMergeCandidates.begin(), E = DiffMergeCandidates.end();
             I != E; ++I)
          delete *I;

        return false;
      } else {
        DiffMergeCandidates.push_back(Comp);
      }
    }

    if (!DiffMergeCandidates.empty()) {
      // Add to our list of candidates for diff merging
      for (SmallVector<FunctionComparator *, 8>::iterator
             I = DiffMergeCandidates.begin(), E = DiffMergeCandidates.end();
           I != E; ++I) {
        Registry.insertCandidate(*I);
      }
    }

    FnI->markCompared();
  }

  return true;
}

bool MergeFunctions::doExhaustiveCompareMerge() {
  bool Changed = false;

  // Process buckets with strong functions first.
  for (MergeRegistry::FnCompareMap::iterator
         BucketsI = Registry.FunctionsToCompare.begin(),
         BucketsE = Registry.FunctionsToCompare.end();
       BucketsI != BucketsE; ++BucketsI) {
    std::list<ComparableFunction> &Fns = BucketsI->second;
    if (Fns.size() < 2 || Fns.front().getFunc()->mayBeOverridden())
      continue;

    DEBUG(dbgs() << "Processing strong bucket " << BucketsI->first << " with "
          << Fns.size() << " functions\n");
    // Repeatedly scan this bucket, until we find no more functions to equiv
    // merge.
    while (!mergeBucket(Fns, Changed) && Fns.size() > 1) {
      DEBUG(dbgs() << "Rescanning bucket.\n");
    }
  }

  // Process buckets with weak functions.
  for (MergeRegistry::FnCompareMap::iterator
         BucketsI = Registry.FunctionsToCompare.begin(),
         BucketsE = Registry.FunctionsToCompare.end();
       BucketsI != BucketsE; ++BucketsI) {
    std::list<ComparableFunction> &Fns = BucketsI->second;
    if (Fns.size() < 2 || !Fns.front().getFunc()->mayBeOverridden())
      continue;

    DEBUG(dbgs() << "Processing weak bucket " << BucketsI->first << " with "
          << Fns.size() << " functions\n");
    // Repeatedly scan this bucket, until we find no more functions to equiv
    // merge.
    while (!mergeBucket(Fns, Changed) && Fns.size() > 1) {
      DEBUG(dbgs() << "Rescanning bucket.\n");
    }
  }

  return Changed;
}

static bool compareTypes(Type* t1, Type* t2){
  return t1 < t2;
}

static bool orderComparatorsByMetric(FunctionComparator *Cmp1,
                                     FunctionComparator *Cmp2) {
  return (Cmp1->getSimilarityMetric() > Cmp2->getSimilarityMetric());
}

// clone loop and remap all internal branches
// put it into a vector of bbs
static void cloneLoop(Loop* L, std::vector<BasicBlock*>& result, ValueToValueMapTy& vmap){
  DenseMap<BasicBlock*,BasicBlock*> bb2clone;
  for(auto bi = L->block_begin(), be = L->block_end(); bi != be; ++bi){
    BasicBlock* newBB = CloneBasicBlock(*bi,vmap,"cl");
    bb2clone[*bi] = newBB;
    result.push_back(newBB);
  }
  
  // update refs
  // for(auto p: vmap){
  //   errs() << *p.first << "->" << *p.second << "\n";
  // }
  for(auto ri = result.begin(), re = result.end(); ri != re; ++ri){
    TerminatorInst* term = (*ri)->getTerminator();
    for(unsigned si = 0, se = term->getNumSuccessors(); si != se; ++si){
      if (bb2clone[term->getSuccessor(si)])
        term->setSuccessor(si,bb2clone[term->getSuccessor(si)]);
    }
    // replace uses of instructions with vmap
    for(auto ii = (*ri)->begin(), ie = (*ri)->end(); ii != ie; ++ii){
      for(unsigned no = 0, ne = ii->getNumOperands(); no != ne; ++no){
        if (auto cast_inst = dyn_cast<Instruction>(ii->getOperand(no))){
          if (vmap[cast_inst])
            ii->setOperand(no,vmap[cast_inst]);
        }
      }
    }
  }
}
// delete all basic blocks
static void deleteClonedLoop(std::vector<BasicBlock*> cloned){
  // delete cloned loops
  for(auto bi = cloned.begin(), be = cloned.end(); bi != be; ++bi){
    (*bi)->getTerminator()->eraseFromParent();
    (*bi)->dropAllReferences();
  }
  for(auto ri = cloned.rbegin(), re = cloned.rend(); ri != re; ++ri){
    delete *ri;
  }
}
static bool isLikelyProfitableToMerge(unsigned comb_insts, unsigned num_fun_args, unsigned fun_insts, unsigned slack){
  // cost model: number of new function instructions + 2 for calls + twice the number
  // of function arguments + slack should be less than original size
  // of two loops
  errs() << "cost to merge: " << fun_insts + 2 + 2*num_fun_args + slack << " size of 2 loops on comb: " << comb_insts << "\n";
  errs() << fun_insts << " " << 2*num_fun_args << " " << slack << "\n";
  return fun_insts + 2 + 2*num_fun_args + slack < comb_insts;
}
static void getDefs(Loop* L, std::vector<Instruction*>& defs){
  for(auto bi = L->block_begin(), be = L->block_end(); bi != be; ++bi){
    for(auto ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
      defs.push_back(ii);
    }
  }
}
bool MergeFunctions::doDiffMerge() {
  if (Registry.FunctionsToMerge.empty())
    return false;

  bool Changed = false;
  DenseSet<Function *> MergedFns; // Functions that have already been merged
  Registry.FunctionsToMerge.sort(orderComparatorsByMetric);

  DEBUG(dumpSimilar(Registry.SimilarFunctions, dbgs()));

  for (std::list<FunctionComparator *>::iterator
         I = Registry.FunctionsToMerge.begin(),
         E = Registry.FunctionsToMerge.end();
       I != E; ++I) {
    FunctionComparator *Comp = *I;
    Function *F1 = Comp->getF1();
    // Ignore it if we've already merged this fn
    if (MergedFns.count(F1) || MergedFns.count(Comp->getF2()))
      continue;

    assert(Registry.SimilarFunctions.count(F1) &&
           "Comparator doesn't exist in SimilarFunctions map");

    // Look at all functions F that F1 could be merged with. Merge with each F,
    // unless there is another function F' that is more similar to F than F1.
    MergeRegistry::FnComparatorSet &SimilarFns = Registry.SimilarFunctions[F1];
    SmallVector<FunctionComparator *, 4> CurrentMerge;

    for (MergeRegistry::FnComparatorSet::iterator
           CandidateI = SimilarFns.begin(), CandidateE = SimilarFns.end();
         CandidateI != CandidateE; ++CandidateI) {
      FunctionComparator *Comp2 = *CandidateI;
      assert(Comp2->getF1() == F1 && "Inconsistency in SimilarFunctions");
      Function *F2 = Comp2->getF2();

      // Ignore it if we've already merged this fn
      if (MergedFns.count(F2))
        continue;

      // Check whether there is a better merge candidate for F2
      if (Registry.getMaxSimilarity(F2, MergedFns) >
          Comp2->getSimilarityMetric())
        continue;

      // Ok, we actually want to merge with F2
      CurrentMerge.push_back(Comp2);
      MergedFns.insert(F2);
    }

    if (CurrentMerge.empty())
      continue;

    MergedFns.insert(F1);

    DEBUG(dbgs() << "- Multi merge of " << F1->getName() << " with "
          << CurrentMerge.size() << " functions.\n");

    /*     DEBUG(dbgs() << "- Diff merging " << Comp->getF1()->getName() << " and "
     *                  << Comp->getF2()->getName() << " : "
     *                  << " bbs=" << Comp->BasicBlockCount
     *                  << " insts=" << Comp->InstructionCount
     *                  << " failed=" << Comp->DifferingInstructionsCount
     *                  << " metric=" << Comp->getSimilarityMetric()
     *                  << '\n');
     *
     */
    Changed = true;
    outlineAndMergeFunctions(CurrentMerge);
  }

  return Changed;
}
