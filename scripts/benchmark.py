import subprocess

cpu_int = ['400.perlbench','401.bzip2']

# source 
subprocess.call("source /home/bilyan/workspace/code-similarity-analyser/trunk/shrc")

for bench in cpu_int:
    name = bench.split('.')[1]
    subprocess.call("runspec --config=linux64-amd64-clang3.5.cfg --action=build --tune=base " + name)

