import matplotlib.pyplot as plt
import numpy as np
import sys
from  scatter_stats import *

xs_ins_l, ys_ins_l = zip(*ins_l)
xs_cfg_l, ys_cfg_l = zip(*cfg_l)

def show_scatter(xlabel, ylabel,xs,ys,c):
    xs = xs + np.random.normal(0,0.5,size=len(xs))
    ys = ys + np.random.normal(0,0.5,size=len(ys))
    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(0,max(np.max(xs),np.max(ys)))
    ax.set_ylim(0,max(np.max(xs),np.max(ys)))
    ax.scatter(xs,ys,c=c)
    ax.plot([0,max(np.max(xs),np.max(ys))],[0,max(np.max(xs),np.max(ys))],'k-',alpha=0.75, zorder=0)
    plt.show()

def unzip(l):
    return (zip(*l))
