import numpy as np
from scipy.stats import ttest_rel

#
#HAVE YOU SOURCED?
#config_resultsall_loops_os.py
#lol libquantum 462.libquantum --min-sim=0.8 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=1 --use-heap=1  -Os
#running...
#10
#462.libquantum run time results for 10 runs:
#mean: 590.444986129 std: 9.37275447555
sim_462_libquantum = [562.50937986373901, 591.57825899124146, 594.77505779266357, 594.40386986732483, 594.26097011566162, 593.49767780303955, 591.67983388900757, 593.16369700431824, 594.34901714324951, 594.23209881782532]
#mean: 592.755651593 std: 2.04235363026
org_462_libquantum = [588.62323307991028, 592.84806895256042, 594.24431800842285, 596.45199298858643, 591.80883312225342, 590.25238299369812, 593.57038688659668, 592.90605592727661, 593.44347906112671, 593.40776491165161]
#2 tailed t-test p value: 0.414383918668
#done
#lol h264ref 464.h264ref --min-sim=0.9 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=0 --use-heap=0  -Os
#running...
#20
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#loop_merge_results12/1866c7ae027df1460567d3a210368fe0/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg
#464.h264ref run time results for 10 runs:
#mean: 77.4702427745 std: 2.6205196673
sim_464_h264ref = [88.794236898422241, 76.19047212600708, 77.541172027587891, 77.084625005722046, 77.006066799163818, 77.453593969345093, 77.067455053329468, 76.774651050567627, 76.940095901489258, 77.241842985153198, 77.069685935974121, 76.972733974456787, 76.601593971252441, 76.530711889266968, 76.526509046554565, 76.643447875976562, 77.016397953033447, 77.062103986740112, 76.552288055419922, 76.335170984268188]
#mean: 76.5191065669 std: 2.28043693343
org_464_h264ref = [86.412004947662354, 75.595752000808716, 75.971160888671875, 75.887319087982178, 75.969925880432129, 75.915165901184082, 76.298819065093994, 76.314283847808838, 76.006804943084717, 75.77096700668335, 75.853730916976929, 75.956400871276855, 76.231756925582886, 76.092022895812988, 76.517520904541016, 75.60156512260437, 75.900568008422852, 75.94189715385437, 76.186404943466187, 75.958060026168823]
#2 tailed t-test p value: 3.00175193686e-07
#done
#lol hmmer 456.hmmer --min-sim=0.9 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=1 --use-heap=1  -Os
#running...
#20
#456.hmmer run time results for 10 runs:
#mean: 145.132145894 std: 1.77742665836
sim_456_hmmer = [143.01932287216187, 143.35972094535828, 143.3215651512146, 143.23575496673584, 143.42237520217896, 143.48780298233032, 143.61875987052917, 143.22409415245056, 143.31110191345215, 143.70767092704773, 146.47377490997314, 146.62294220924377, 147.05028200149536, 146.66590285301208, 147.35066318511963, 146.76407098770142, 146.77593493461609, 147.21501588821411, 147.2125780582428, 146.80358386039734]
#mean: 145.508322585 std: 1.82391496564
org_456_hmmer = [143.37659001350403, 143.48849487304688, 143.69513297080994, 143.99917984008789, 143.74874687194824, 143.42875790596008, 143.68019509315491, 143.79967093467712, 143.52886295318604, 144.34431910514832, 146.39254808425903, 147.52509212493896, 147.32130098342896, 147.49899196624756, 147.29618406295776, 147.58143591880798, 147.37680983543396, 147.40349698066711, 147.35210919380188, 147.32853198051453]
#2 tailed t-test p value: 3.73092890686e-05
#done
#lol milc 433.milc --min-sim=0.9 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=0 --use-heap=1  -Os
#running...
#15
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out0 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err0 
#553.26134491
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out1 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err1 
#533.459538937
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out2 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err2 
#532.2301929
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out3 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err3 
#532.603283882
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out4 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err4 
#536.70300293
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out5 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err5 
#536.368109941
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out6 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err6 
#534.69408989
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out7 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err7 
#531.898085117
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out8 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err8 
#530.106809855
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out9 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err9 
#535.048277855
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out10 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err10 
#530.755511045
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out11 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err11 
#520.503778934
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out12 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err12 
#524.317796946
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out13 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err13 
#525.525771141
#start
#taskset 0x00000002 loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.out14 2>loop_merge_results12/d83792367c52dc06710286cac025e7a4/milc.ref.err14 
#525.539831877
#433.milc run time results for 10 runs:
#mean: 532.201028411 std: 7.26501478216
sim_433_milc= [553.26134490966797, 533.45953893661499, 532.23019289970398, 532.60328388214111, 536.7030029296875, 536.36810994148254, 534.69408988952637, 531.89808511734009, 530.10680985450745, 535.04827785491943, 530.75551104545593, 520.50377893447876, 524.3177969455719, 525.52577114105225, 525.53983187675476]
#mean: 508.762550386 std: 5.45501443935
org_433_milc = [515.2694399356842, 507.67034387588501, 513.92398691177368, 511.03943109512329, 516.20723295211792, 513.41822695732117, 513.48883104324341, 510.07542419433594, 511.10198092460632, 507.73026084899902, 508.72721004486084, 502.18313217163086, 499.96213483810425, 500.21566796302795, 500.42495203018188]
#2 tailed t-test p value: 2.88411003717e-11
#done
#lol mcf 429.mcf --min-sim=0.7 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=1 --use-heap=0  -Os
#running...
#20
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out0 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err0 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out1 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err1 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out2 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err2 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out3 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err3 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out4 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err4 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out5 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err5 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out6 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err6 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out7 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err7 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out8 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err8 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out9 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err9 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out10 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err10 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out11 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err11 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out12 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err12 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out13 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err13 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out14 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err14 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out15 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err15 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out16 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err16 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out17 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err17 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out18 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err18 
#taskset 0x00000002 loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.out19 2>loop_merge_results12/f4d6ca91e880f7ef4bbacca07c719250/mcf.ref.err19 
#429.mcf run time results for  20  runs:
#mean: 438.561652505 std: 34.0311991147
sim_429_mcf = [448.23852896690369, 447.53623294830322, 462.14721393585205, 452.3981831073761, 465.34493017196655, 461.31606197357178, 461.83919405937195, 461.4253351688385, 447.12086009979248, 462.72043895721436, 447.03700089454651, 449.5523579120636, 451.36233115196228, 458.79881691932678, 449.52250409126282, 458.42458391189575, 375.44242405891418, 366.97228193283081, 376.36085200309753, 367.67291784286499]
#mean: 431.757155442 std: 35.4156823266
org_429_mcf = [447.05783605575562, 445.13677191734314, 447.56715512275696, 463.37663197517395, 450.29736399650574, 456.51716017723083, 445.681401014328, 446.71229600906372, 447.4914608001709, 457.26326107978821, 447.88524794578552, 448.92967391014099, 458.07758283615112, 457.30265116691589, 459.71685314178467, 379.02384400367737, 374.31837296485901, 372.62511086463928, 365.04865193367004, 365.11378192901611]
#2 tailed t-test p value: 0.124095090673
#done
#lol sjeng 458.sjeng --min-sim=0.7 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=1 --use-heap=1  -Os
#running...
#done
#
# HAVE YOU SOURCED?
# config_resultsall_loops_os.py
# lol sjeng 458.sjeng --min-sim=0.7 --min-insts=0 --max-insts=4000000 --min-num-common-values=0 --max-num-common-values=2000000 --min-num-bbs=0 --max-num-bbs=3000000 --sim-metric=1 --use-heap=1  -Os
# running...
# 458.sjeng run time results for 10 runs:
# mean: 0.0166834712029 std: 0.015999131563
sim_458_sjeng = [0.02110600471496582, 0.0076742172241210938, 0.0070688724517822266, 0.0078060626983642578, 0.0074791908264160156, 0.0071868896484375, 0.0077040195465087891, 0.012599945068359375, 0.028491020202636719, 0.024987936019897461, 0.054899930953979492, 0.048481941223144531, 0.051284074783325195, 0.0071430206298828125, 0.0066571235656738281, 0.0067369937896728516, 0.0067639350891113281, 0.0066661834716796875, 0.0063660144805908203, 0.0065660476684570312]
# mean: 541.513576198 std: 1.44718706159
org_458_sjeng = [544.87451601028442, 540.45434498786926, 541.6682391166687, 544.3997790813446, 541.54759383201599, 540.93199992179871, 542.5940260887146, 543.07280993461609, 540.23960089683533, 542.73193192481995, 540.75439095497131, 541.12840795516968, 540.55704593658447, 542.19624614715576, 541.03022789955139, 541.54196405410767, 539.80971503257751, 540.69008111953735, 541.38671803474426, 538.66188502311707]
# 2 tailed t-test p value: 2.38090684761e-50
# done


benches = ["429.mcf","433.milc","456.hmmer","458.sjeng","462.libquantum","464.h264ref"]
orgs = [org_429_mcf         ,org_433_milc        ,org_456_hmmer       ,org_458_sjeng       ,org_462_libquantum  ,org_464_h264ref]
sims = [sim_429_mcf,         sim_433_milc,        sim_456_hmmer,       sim_458_sjeng,       sim_462_libquantum,  sim_464_h264ref]
     

for i in range(len(benches)):
    med_sim = np.median(sims[i])
    med_org = np.median(orgs[i])
    p = ttest_rel(sims[i],orgs[i])
    print benches[i], "med_sim/med_org speedup in %:",(1 - med_sim/med_org)*100, "med sim:",med_sim, "med org: ",med_org, "p value:",p[1]
