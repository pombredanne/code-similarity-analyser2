import matplotlib.pyplot as plt
import numpy as np
from loop_stats import*
from plplot import *
from plfit import*
# num loops per cfg size
xs_all1 = set(xs_astar_1 + xs_bzip2_1 + xs_gcc_1 + xs_gobmk_1 + xs_h264ref_1 + xs_hmmer_1 + xs_lbm_1 + xs_libquantum_1 + xs_mcf_1 + xs_mcf_1 + xs_namd_1 + xs_omnetpp_1 + xs_perlbench_1 + xs_povray_1 + xs_sjeng_1 + xs_soplex_1 + xs_sphinx3_1)
ys_all1 = [dict(zip(xs_astar_1, ys_astar_1)), dict(zip(xs_bzip2_1, ys_bzip2_1)), dict(zip(xs_gcc_1, ys_gcc_1)), dict(zip(xs_gobmk_1, ys_gobmk_1)), dict(zip(xs_h264ref_1, ys_h264ref_1)), dict(zip(xs_hmmer_1, ys_hmmer_1)), dict(zip(xs_lbm_1, ys_lbm_1)), dict(zip(xs_libquantum_1, ys_libquantum_1)), dict(zip(xs_mcf_1, ys_mcf_1)), dict(zip(xs_mcf_1, ys_mcf_1)), dict(zip(xs_namd_1, ys_namd_1)), dict(zip(xs_omnetpp_1, ys_omnetpp_1)), dict(zip(xs_perlbench_1, ys_perlbench_1)), dict(zip(xs_povray_1, ys_povray_1)), dict(zip(xs_sjeng_1, ys_sjeng_1)), dict(zip(xs_soplex_1, ys_soplex_1)), dict(zip(xs_sphinx3_1, ys_sphinx3_1))]

d_all = dict.fromkeys(xs_all1,0)

for d in ys_all1:    
    for v,c in d.items():
        d_all[v] = d_all[v] + c

x_all1 , y_all1 = zip(*sorted(d_all.items()))

y_all1_cs = np.cumsum(y_all1)
p80 = np.sum(y_all1)*0.8
ind = 0
for i in range(len(y_all1_cs)):
    if y_all1_cs[i] > p80:
        ind = i
        break
p90 = np.sum(y_all1)*0.9
ind_2 = 0
for i in range(len(y_all1_cs)):
    if y_all1_cs[i] > p90:
        ind_2 = i
        break

#print "cfg size 90th percentile:", ind, 1.0*np.sum(y_all1[:ind+1])/np.sum(y_all1), 1.0*y_all1_cs[ind]/np.sum(y_all1), p90

plt.bar(x_all1,y_all1,1,color='g',log=True)
plt.axvline(x=ind,c="r",label='80th percentile')
plt.axvline(x=ind_2,c="b",label='90th percentile')
plt.xscale('log')
plt.xlabel('Number of LLVM basic blocks',size=15)
plt.ylabel('Number of loops',size=15)
plt.legend().set_zorder(3)
plt.tick_params(axis='both',labelsize=15)
plt.savefig('size_cfg_all_bench_log_log.pdf', bbox_inches='tight')
plt.show()

# num loops per num instructions
xs_all2 = set(xs_astar_2 + xs_bzip2_2 + xs_gcc_2 + xs_gobmk_2 + xs_h264ref_2 + xs_hmmer_2 + xs_lbm_2 + xs_libquantum_2 + xs_mcf_2 + xs_mcf_2 + xs_namd_2 + xs_omnetpp_2 + xs_perlbench_2 + xs_povray_2 + xs_sjeng_2 + xs_soplex_2 + xs_sphinx3_2)
ys_all2 = [dict(zip(xs_astar_2, ys_astar_2)), dict(zip(xs_bzip2_2, ys_bzip2_2)), dict(zip(xs_gcc_2, ys_gcc_2)), dict(zip(xs_gobmk_2, ys_gobmk_2)), dict(zip(xs_h264ref_2, ys_h264ref_2)), dict(zip(xs_hmmer_2, ys_hmmer_2)), dict(zip(xs_lbm_2, ys_lbm_2)), dict(zip(xs_libquantum_2, ys_libquantum_2)), dict(zip(xs_mcf_2, ys_mcf_2)), dict(zip(xs_mcf_2, ys_mcf_2)), dict(zip(xs_namd_2, ys_namd_2)), dict(zip(xs_omnetpp_2, ys_omnetpp_2)), dict(zip(xs_perlbench_2, ys_perlbench_2)), dict(zip(xs_povray_2, ys_povray_2)), dict(zip(xs_sjeng_2, ys_sjeng_2)), dict(zip(xs_soplex_2, ys_soplex_2)), dict(zip(xs_sphinx3_2, ys_sphinx3_2))]

d_all = dict.fromkeys(xs_all2,0)

for d in ys_all2:    
    for v,c in d.items():
        d_all[v] = d_all[v] + c

x_all2 , y_all2 = zip(*sorted(d_all.items()))
y_all2_cs = np.cumsum(y_all2)
p80 = np.sum(y_all2)*0.8
ind = 0
for i in range(len(y_all2_cs)):
    if y_all2_cs[i] > p80:
        ind = i
        break
p90 = np.sum(y_all2)*0.9
ind_2 = 0
for i in range(len(y_all2_cs)):
    if y_all2_cs[i] > p90:
        ind_2 = i
        break

plt.bar(x_all2,y_all2,1,color='purple',log=True)
plt.axvline(x=ind,c="r",label='80th percentile')
plt.axvline(x=ind_2,c="b",label='90th percentile')
plt.xscale('log')
plt.xlabel('Number of LLVM instructions',size=15)
plt.ylabel('Number of loops',size=15)
plt.tick_params(axis='both',labelsize=15)
plt.legend().set_zorder(3)
plt.savefig('insts_all_bench_log_log.pdf', bbox_inches='tight')
plt.show()

# plots about number of external def values
plt.plot(xs_astar_3,ys_astar_3,color='blue')
plt.plot(xs_bzip2_3,ys_bzip2_3,color='brown')
plt.plot(xs_gcc_3,ys_gcc_3,color='chartreuse')
plt.plot(xs_gobmk_3,ys_gobmk_3,color='crimson')
plt.plot(xs_h264ref_3,ys_h264ref_3,color='cyan')
plt.plot(xs_hmmer_3,ys_hmmer_3,color='darkorange')
plt.plot(xs_lbm_3,ys_lbm_3,color='darkviolet')
plt.plot(xs_libquantum_3,ys_libquantum_3,color='gold')
plt.plot(xs_mcf_3,ys_mcf_3,color='greenyellow')
plt.plot(xs_milc_3,ys_milc_3,color='indigo')
plt.plot(xs_namd_3,ys_namd_3,color='lightblue')
plt.plot(xs_omnetpp_3,ys_omnetpp_3,color='lightcoral')
plt.plot(xs_perlbench_3,ys_perlbench_3,color='lightseagreen')
plt.plot(xs_povray_3,ys_povray_3,color='magenta')
plt.plot(xs_sjeng_3,ys_sjeng_3,color='navy')
plt.plot(xs_soplex_3,ys_soplex_3,color='orangered')
plt.plot(xs_sphinx3_3,ys_sphinx3_3,color='purple')

plt.xscale('log')
plt.yscale('log')

plt.show()
plt.close()


# plot ext defs stuff
xs_all3 = set(xs_astar_3 + xs_bzip2_3 + xs_gcc_3 + xs_gobmk_3 + xs_h264ref_3 + xs_hmmer_3 + xs_lbm_3 + xs_libquantum_3 + xs_mcf_3 + xs_mcf_3 + xs_namd_3 + xs_omnetpp_3 + xs_perlbench_3 + xs_povray_3 + xs_sjeng_3 + xs_soplex_3 + xs_sphinx3_3)
ys_all3 = [dict(zip(xs_astar_3, ys_astar_3)), dict(zip(xs_bzip2_3, ys_bzip2_3)), dict(zip(xs_gcc_3, ys_gcc_3)), dict(zip(xs_gobmk_3, ys_gobmk_3)), dict(zip(xs_h264ref_3, ys_h264ref_3)), dict(zip(xs_hmmer_3, ys_hmmer_3)), dict(zip(xs_lbm_3, ys_lbm_3)), dict(zip(xs_libquantum_3, ys_libquantum_3)), dict(zip(xs_mcf_3, ys_mcf_3)), dict(zip(xs_mcf_3, ys_mcf_3)), dict(zip(xs_namd_3, ys_namd_3)), dict(zip(xs_omnetpp_3, ys_omnetpp_3)), dict(zip(xs_perlbench_3, ys_perlbench_3)), dict(zip(xs_povray_3, ys_povray_3)), dict(zip(xs_sjeng_3, ys_sjeng_3)), dict(zip(xs_soplex_3, ys_soplex_3)), dict(zip(xs_sphinx3_3, ys_sphinx3_3))]

d_all = dict.fromkeys(xs_all3,0)

for d in ys_all3:    
    for v,c in d.items():
        d_all[v] = d_all[v] + c

x_all3 , y_all3 = zip(*sorted(d_all.items()))
y_all3_cs = np.cumsum(y_all3)
p90 = np.sum(y_all3)*0.9
ind = 0
for i in range(len(y_all3_cs)):
    if y_all3_cs[i] > p90:
        ind = i
        break

plt.bar(x_all3,y_all3,1,color='y',log=True,label='Number of loops')
plt.tick_params(axis='both',labelsize=15)
plt.xlabel('Number of used values inside a loop that are defined outside of its body')
plt.axvline(x=ind,c="purple",label='90th percentile')

ys_all4 = [dict(zip(xs_astar_4, ys_astar_4)), dict(zip(xs_bzip2_4, ys_bzip2_4)), dict(zip(xs_gcc_4, ys_gcc_4)), dict(zip(xs_gobmk_4, ys_gobmk_4)), dict(zip(xs_h264ref_4, ys_h264ref_4)), dict(zip(xs_hmmer_4, ys_hmmer_4)), dict(zip(xs_lbm_4, ys_lbm_4)), dict(zip(xs_libquantum_4, ys_libquantum_4)), dict(zip(xs_mcf_4, ys_mcf_4)), dict(zip(xs_mcf_4, ys_mcf_4)), dict(zip(xs_namd_4, ys_namd_4)), dict(zip(xs_omnetpp_4, ys_omnetpp_4)), dict(zip(xs_perlbench_4, ys_perlbench_4)), dict(zip(xs_povray_4, ys_povray_4)), dict(zip(xs_sjeng_4, ys_sjeng_4)), dict(zip(xs_soplex_4, ys_soplex_4)), dict(zip(xs_sphinx3_4, ys_sphinx3_4))]

d_all4 = dict.fromkeys(xs_all3,0)

for d in ys_all4:    
    for v,c in d.items():
        d_all4[v] = d_all4[v] + c
# divide by num of benchmakrs
x_all4 , y_all4 = zip(*sorted(d_all4.items()))

#y_all4 = np.array(y_all4)/(1.0*len(ys_all4))

plt.plot(x_all4,np.array(y_all4)/(1.0*len(ys_all4)),'xb-',label='Average number of instructions per loop') # Was total
#plt.xscale('log')
#plt.yscale('log')

ys_all5 = [dict(zip(xs_astar_5, ys_astar_5)), dict(zip(xs_bzip2_5, ys_bzip2_5)), dict(zip(xs_gcc_5, ys_gcc_5)), dict(zip(xs_gobmk_5, ys_gobmk_5)), dict(zip(xs_h264ref_5, ys_h264ref_5)), dict(zip(xs_hmmer_5, ys_hmmer_5)), dict(zip(xs_lbm_5, ys_lbm_5)), dict(zip(xs_libquantum_5, ys_libquantum_5)), dict(zip(xs_mcf_5, ys_mcf_5)), dict(zip(xs_mcf_5, ys_mcf_5)), dict(zip(xs_namd_5, ys_namd_5)), dict(zip(xs_omnetpp_5, ys_omnetpp_5)), dict(zip(xs_perlbench_5, ys_perlbench_5)), dict(zip(xs_povray_5, ys_povray_5)), dict(zip(xs_sjeng_5, ys_sjeng_5)), dict(zip(xs_soplex_5, ys_soplex_5)), dict(zip(xs_sphinx3_5, ys_sphinx3_5))]

d_all5 = dict.fromkeys(xs_all3,0)

for d in ys_all5:    
    for v,c in d.items():
        d_all5[v] = d_all5[v] + c
# divide by num of benchmakrs
x_all5 , y_all5 = zip(*sorted(d_all5.items()))

#y_all5 = np.array(y_all5)/(1.0*len(ys_all5))

plt.plot(x_all5,np.array(y_all5)/(1.0*len(ys_all5)),'.r-',label='Average number of basic blocks per loop')
plt.legend(bbox_to_anchor=(0.2,-0.1), loc=2,prop={'size':15}).set_zorder(20)
plt.savefig('ex_defs_comb_log_lin_scatter.pdf', bbox_inches='tight')
plt.show()
plt.close()
# cumsum
y_all4_cs = np.cumsum(y_all4)
p90 = np.sum(y_all4)*0.9
ind = 0
for i in range(len(y_all4_cs)):
    if y_all4_cs[i] > p90:
        ind = i
        break
plt.axvline(x=ind,c="b",label='90th percentile instructions')
y_all5_cs = np.cumsum(y_all5)
p90 = np.sum(y_all5)*0.9
ind = 0
for i in range(len(y_all5_cs)):
    if y_all5_cs[i] > p90:
        ind = i
        break
plt.axvline(x=ind,c="r",label='90th percentile basic blocks')
print y_all4_cs[10]/y_all4_cs[-1]
print y_all5_cs[10]/y_all5_cs[-1]
print y_all5_cs[67]/y_all5_cs[-1]

plt.yscale('log')
plt.ylim((10,1000000))
# axes = plt.gca()
# axes.set_ylim((0,1000000))
plt.tick_params(axis='both',labelsize=15)
plt.xlabel('Number of used values inside a loop that are defined outside of its body',size=15)
plt.plot(x_all5,np.cumsum(y_all5),'.r-',label='Cumulative number of basic blocks')
plt.plot(x_all4,np.cumsum(y_all4),'xb-',label='Cumulative number of instructions')
plt.savefig('ex_defs_comb_log_lin_cumulative.pdf', bbox_inches='tight')
plt.show()
plt.close()
# total number of loops
total_num_d1_loops = ys_astar_0[0] + ys_bzip2_0[0] + ys_gcc_0[0] + ys_gobmk_0[0] + ys_h264ref_0[0] + ys_hmmer_0[0] + ys_lbm_0[0] + ys_libquantum_0[0] + ys_mcf_0[0] + ys_mcf_0[0] + ys_namd_0[0] + ys_omnetpp_0[0] + ys_perlbench_0[0] + ys_povray_0[0] + ys_sjeng_0[0] + ys_soplex_0[0] + ys_sphinx3_0[0]




