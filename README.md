# Loop Similarity Merger for the Reduction of Code Size #

Honours 4th year project by Bilyan Borisov, all rights reserved, not to be redistributed. You are encouraged to browse the code through the website or clone it locally and look at it but you need my explicit consent to modify, adapt, or otherwise use it for your own intentions. The same applies to the text of the undegraduate dissertation inside the report subfolder - I reserve all copyrights on the text, you are free to read it but no modifications, reproductions, or uses of the text in any way are allowed without my explicit permission.

The LLVM opt pass I am producing is in llvm-3.5/lib/Transforms/CodeSimilarityAnalyser .
To run the pass, compile a file to bitcode with

    clang -emit-llvm -o file.bc file.c

and then run

    opt -analyze -indvars   -load llvm-3.5/Debug+Asserts/lib/CodeSimilarityAnalyser.so  -codesimilarityanalyser  file.bc > /dev/null

You would need llvm3.5 and clang binaries built on your path as well as a locally built llvm3.5 with Debug + Asserts.

There is a presentation folder with some slides if you wish to see a very general overview of the project - go to presentations/ and look for .pdf files.

The dissertation that describes the work and achievements for my Honours project is the report/skeleton.pdf file.