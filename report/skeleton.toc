\contentsline {chapter}{\numberline {1}Introduction}{7}
\contentsline {section}{\numberline {1.1}Overview}{7}
\contentsline {section}{\numberline {1.2}Motivation}{7}
\contentsline {section}{\numberline {1.3}Main Contributions}{8}
\contentsline {section}{\numberline {1.4}Thesis Organisation}{9}
\contentsline {chapter}{\numberline {2}Background}{11}
\contentsline {section}{\numberline {2.1}Importance of Code Size for Embedded Systems}{11}
\contentsline {section}{\numberline {2.2}Compiler Terminology}{11}
\contentsline {subsection}{\numberline {2.2.1}Different Phases of a Compiler}{11}
\contentsline {subsection}{\numberline {2.2.2}Intermediate Representation}{12}
\contentsline {subsection}{\numberline {2.2.3}Control Flow Graphs and Basic Blocks}{12}
\contentsline {subsection}{\numberline {2.2.4}Dominance and Loops}{13}
\contentsline {subsection}{\numberline {2.2.5}SSA Form and Phi Instructions}{13}
\contentsline {section}{\numberline {2.3}LLVM}{14}
\contentsline {subsection}{\numberline {2.3.1}LLVM Internal Representation}{14}
\contentsline {subsection}{\numberline {2.3.2}The {\tt opt} Tool}{14}
\contentsline {subsection}{\numberline {2.3.3}Important Classes}{14}
\contentsline {subsection}{\numberline {2.3.4}Passes in LLVM}{15}
\contentsline {section}{\numberline {2.4}Summary}{16}
\contentsline {chapter}{\numberline {3}Related Work}{17}
\contentsline {section}{\numberline {3.1}Code Size Reduction}{17}
\contentsline {subsection}{\numberline {3.1.1}Software Based}{17}
\contentsline {subsection}{\numberline {3.1.2}Hardware Assisted}{18}
\contentsline {section}{\numberline {3.2}Summary}{19}
\contentsline {chapter}{\numberline {4}Loop Similarity}{21}
\contentsline {section}{\numberline {4.1}Control Flow Graph Isomorphism Algorithm}{22}
\contentsline {subsection}{\numberline {4.1.1}The Graph Isomorphism Problem}{22}
\contentsline {subsection}{\numberline {4.1.2}Algorithm for Finding Isomorphisms Between Control Flow Graphs}{23}
\contentsline {section}{\numberline {4.2}Basic Block Level Similarity}{24}
\contentsline {subsection}{\numberline {4.2.1}Instruction Level Equivalence}{24}
\contentsline {subsection}{\numberline {4.2.2}Longest Common Subsequence Based Similarity Function}{25}
\contentsline {subsection}{\numberline {4.2.3}Longest Common Subsequence Problem}{25}
\contentsline {subsection}{\numberline {4.2.4}Obtaining the Longest Common Subsequence and the ``diff'' from the $C$ Matrix}{26}
\contentsline {section}{\numberline {4.3}Combining Similarities}{28}
\contentsline {section}{\numberline {4.4}Summary}{29}
\contentsline {chapter}{\numberline {5}Loop Merging}{31}
\contentsline {section}{\numberline {5.1}Overview}{31}
\contentsline {section}{\numberline {5.2}Comparing All Loops}{32}
\contentsline {section}{\numberline {5.3}Profitability of Merging}{32}
\contentsline {section}{\numberline {5.4}Merging Strategy}{33}
\contentsline {subsection}{\numberline {5.4.1}Example}{33}
\contentsline {subsubsection}{\numberline {5.4.1.1}Two Loops' Header Blocks}{34}
\contentsline {subsubsection}{\numberline {5.4.1.2}Result of Merging Two Header Blocks}{34}
\contentsline {subsection}{\numberline {5.4.2}Merging Algorithm}{35}
\contentsline {subsubsection}{\numberline {5.4.2.1}Inserting Extra Control Flow}{35}
\contentsline {subsubsection}{\numberline {5.4.2.2}Phi Nodes}{36}
\contentsline {subsubsection}{\numberline {5.4.2.3}Extract Loops Into a Separate Function}{37}
\contentsline {section}{\numberline {5.5}Pass Parameters}{37}
\contentsline {section}{\numberline {5.6}Summary}{39}
\contentsline {chapter}{\numberline {6}Experimental Methodology and Implementation Details}{41}
\contentsline {section}{\numberline {6.1}Experimentation Methodology}{41}
\contentsline {subsection}{\numberline {6.1.1}Benchmarks}{41}
\contentsline {subsection}{\numberline {6.1.2}Platform and Verification}{41}
\contentsline {subsection}{\numberline {6.1.3}Code Size Methodology}{42}
\contentsline {subsection}{\numberline {6.1.4}Run Time Methodology}{42}
\contentsline {subsection}{\numberline {6.1.5}Difference Between {\tt -Os} and {\tt -O2}}{43}
\contentsline {section}{\numberline {6.2}Implementation Details}{43}
\contentsline {subsection}{\numberline {6.2.1}Technologies and Software Used}{43}
\contentsline {subsection}{\numberline {6.2.2}LLVM Implementation Details}{44}
\contentsline {subsection}{\numberline {6.2.3}Code Reuse}{44}
\contentsline {subsection}{\numberline {6.2.4}Limitations}{44}
\contentsline {section}{\numberline {6.3}Summary}{45}
\contentsline {chapter}{\numberline {7}Analysis}{47}
\contentsline {section}{\numberline {7.1}Limit study}{47}
\contentsline {subsection}{\numberline {7.1.1}Theoretical Upper Bound}{48}
\contentsline {subsection}{\numberline {7.1.2}Obtaining a Tighter Bound}{48}
\contentsline {section}{\numberline {7.2}Loop Statistics and Their Implications}{49}
\contentsline {subsection}{\numberline {7.2.1}Loop Control Flow Graph Distribution}{49}
\contentsline {subsection}{\numberline {7.2.2}Loop Instruction Distribution}{50}
\contentsline {subsection}{\numberline {7.2.3}Loop Independence of Context}{50}
\contentsline {section}{\numberline {7.3}Summary}{52}
\contentsline {chapter}{\numberline {8}Evaluation}{53}
\contentsline {section}{\numberline {8.1}Parameter Space Exploration}{53}
\contentsline {section}{\numberline {8.2}Code Size Results Evaluation}{54}
\contentsline {subsection}{\numberline {8.2.1}Interpretation of Results}{54}
\contentsline {subsection}{\numberline {8.2.2}Pass Parameter Configurations That Achieved Best Code Size Reduction}{57}
\contentsline {section}{\numberline {8.3}Run Time Evaluation}{58}
\contentsline {subsection}{\numberline {8.3.1}Statistical Significance of Results}{59}
\contentsline {subsection}{\numberline {8.3.2}Interpretations of Results}{60}
\contentsline {section}{\numberline {8.4}Summary}{60}
\contentsline {chapter}{\numberline {9}Conclusion}{61}
\contentsline {section}{\numberline {9.1}Summary}{61}
\contentsline {section}{\numberline {9.2}Critical Analysis}{61}
\contentsline {section}{\numberline {9.3}Future Work}{62}
\contentsline {chapter}{Bibliography}{63}
